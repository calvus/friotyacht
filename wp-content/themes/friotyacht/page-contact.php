<?php
/*
 * Template name: Page Contact
 */

get_header();
?>

<!-- NOTE: Page content wrapper STARTS here -->
<main class="page-content-wrapper" role="main">
    <!-- NOTE: Page content -->
    <?php
    if (have_posts()) : while (have_posts()) : the_post();
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
            ?>
            <div class="container-fluid container--home">
                <section class="section section--header section--page-header" style="background-image: url('<?php echo $featured_img_url; ?>');">
                    <div class="container">
                        <div class="row page-breadcrumb-row ml-0 mr-0">
                            <nav style="--bs-breadcrumb-divider: '/';" class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <?php
                                    if (function_exists('yoast_breadcrumb')) {
                                        yoast_breadcrumb('<li class="breadcrumb-item">', '</li>');
                                    }
                                    ?>                  
                                </ol>
                            </nav>
                            <div class="button-wrapper col-12 col-md-4 p-0">

                            </div>
                        </div>
                        <div class="row m-0">
                            <div class="text-block text-center w-100">
                                <h1 class="banner-title"><?php the_title(); ?></h1>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- section counter -->
                <section class="section section--contact-form bg-parallax">
                    <div class="container">
                        <p class="text-center mb-4"><strong><?php _e('Please contact us for further information', 'friot'); ?></strong></p>
                        <?php echo do_shortcode('[contact-form-7 id="87" title="Contact form"]'); ?>
                        <div class="row">
                            <div class="col-12 mb-3 col-md-6 mb-md-0">
                                <div class="d-flex justify-content-center align-items-md-center h-100">
                                    <div class="grey-info-box">
                                        <?php dynamic_sidebar('contact-1'); ?>                                
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="d-flex justify-content-center align-items-md-center h-100">
                                    <div class="grey-info-box">
                                        <?php dynamic_sidebar('contact-2'); ?>                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /counter -->

            </div>
            <?php
        endwhile;
    endif;
    ?>
</main><!-- NOTE: Page content wrapper ENDS here -->
<?php get_footer(); ?>