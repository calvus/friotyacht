<?php get_header(); ?>
<!-- NOTE: Page content wrapper STARTS here -->
<main class="page-content-wrapper" role="main">
    <?php
    if (have_posts()) : while (have_posts()) : the_post();
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
            ?>
            <!-- NOTE: Page content -->
            <div class="container-fluid container--home">
                <section class="section section--header section--destination-header"
                         style="background-image: url('<?php echo esc_url($featured_img_url); ?>');">
                    <div class="container">
                        <div class="row page-breadcrumb-row ml-0 mr-0">
                            <nav class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <?php
                                    if (function_exists('yoast_breadcrumb')) {
                                        yoast_breadcrumb('<li class="breadcrumb-item">', '</li>');
                                    }
                                    ?>
                                </ol>
                            </nav>
                            <div class="button-wrapper col-12 col-md-4 p-0">

                            </div>
                        </div>
                        <div class="row m-0">
                            <div class="text-block text-center w-100">
                                <h1 class="banner-title --sm-title"><?php the_title(); ?></h1>
                                <div class="divider"></div>
                                <h1 class="banner-sub-title"><?php the_excerpt(); ?></h1>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- accordion/tab -->
                <section class="section section--card-block bg-lightblue">
                    <div class="container">
                        <!-- accordion display on small screen -->
                        <div class="d-block d-lg-none">
                            <div class="accordion accordion-flush" id="mainAccordion">
                                <?php
                                $tour_report_args = array(
                                    'post_type' => 'tour-report-days',
                                    'posts_per_page' => -1,
                                    'meta_query' => array(
                                        array(
                                            'key' => '_tour_report_id',
                                            'value' => array(get_the_ID()),
                                            'compare' => 'IN',
                                        ),
                                    )
                                );

                                $tour_report_loop = new WP_Query($tour_report_args);
                                if ($tour_report_loop->have_posts()) :
                                    ?>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="accordion-item-heading_1">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                                    data-bs-target="#accordion-item-collapse_1">
                                                        <?php _e('Itiner', 'friot'); ?>
                                            </button>
                                        </h2>
                                        <div id="accordion-item-collapse_1" class="accordion-collapse collapse"
                                             data-bs-parent="#mainAccordion">
                                            <div class="accordion-body">
                                                <?php
                                                $c = 0;
                                                while ($tour_report_loop->have_posts()) : $tour_report_loop->the_post();
                                                    ?>
                                                    <div class="day-row">
                                                        <div class="day-wrapper w-100">
                                                            <p><?php the_title(); ?></p>
                                                        </div>
                                                        <div class="day-info w-100">
                                                            <div class="image-holder">
                                                                <?php
                                                                $napi_kepek = get_post_meta(get_the_ID(), '_mbox_tour_reports_day_gallery', true);

                                                                if ($napi_kepek) {
                                                                    ?>
                                                                    <div class="swiper-wrapper">
                                                                        <?php
                                                                        foreach ($napi_kepek as $key => $napi_kep) {
                                                                            $srcset = wp_get_attachment_image_srcset($key);
                                                                            $img_src = wp_get_attachment_image_src($key, 'large');
                                                                            $img_src_full = wp_get_attachment_image_src($key, 'full');
                                                                            ?>
                                                                            <div class="swiper-slide">
                                                                                <a href="<?php echo $img_src_full[0]; ?>" class="gallery-item" data-fancybox="group<?php echo $c; ?>"> <img src="<?php echo $img_src[0]; ?>"></a>
                                                                            </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <?php
                                                                } else {
                                                                    the_post_thumbnail('large');
                                                                }
                                                                ?>
                                                            </div>
                                                            <?php if (has_excerpt()) { ?>
                                                                <p><strong><?php the_excerpt(); ?></strong></p>
                                                            <?php } ?>
                                                            <?php the_content(); ?>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $c++;
                                                endwhile;
                                                ?>

                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                endif;
                                wp_reset_query();
                                ?>
                            </div>
                        </div>
                        <!-- tab view on large -->
                        <div class="d-none d-lg-block">
                            <div class="tab-content" id="nav-tabContent">
                                <?php if ($tour_report_loop->have_posts()) : ?>
                                    <div class="tab-pane fade show active" id="tab-1-toggle" role="tabpanel">
                                        <?php
                                        $postCounter = 1;
                                        while ($tour_report_loop->have_posts()) : $tour_report_loop->the_post();
                                            ?>
                                            <div class="row tab-row day-row flex-wrap">
                                                <div class="col-12 p-0">
                                                    <div class="row tab-row day-row mb-0">
                                                        <div class="day-wrapper">

                                                            <?php if (get_locale() === 'hu_HU') { ?>
                                                                <p class="cbc-title"><span style="color:#072B3D"><?php echo $postCounter; ?>.</span> nap</p>
                                                            <?php } else { ?>
                                                                <p class="cbc-title">Day <span style="color:#072B3D"><?php echo $postCounter; ?></span></p>
                                                            <?php } ?>

                                                        </div>
                                                        <div class="day-info">
                                                            <div class="image-holder mb-0" style="position: relative;">
                                                                <?php
                                                                $napi_kepek = get_post_meta(get_the_ID(), '_mbox_tour_reports_day_gallery', true);

                                                                if ($napi_kepek) {
                                                                    ?>
                                                                    <div class="icon" style="position:absolute; right:0; top:0; z-index:99; background: rgba(0, 0, 0, 0.5); padding: 4px 10px;">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" height="48" viewBox="0 96 960 960" width="48"><path fill="#ffffff" d="M100 856q-24.75 0-42.375-17.625T40 796V356q0-24.75 17.625-42.375T100 296h440q24.75 0 42.375 17.625T600 356v440q0 24.75-17.625 42.375T540 856H100Zm620-320q-17 0-28.5-11.5T680 496V336q0-17 11.5-28.5T720 296h160q17 0 28.5 11.5T920 336v160q0 17-11.5 28.5T880 536H720Zm20-60h120V356H740v120ZM100 796h440V356H100v440Zm60-100h320L375 556l-75 100-55-73-85 113Zm560 160q-17 0-28.5-11.5T680 816V656q0-17 11.5-28.5T720 616h160q17 0 28.5 11.5T920 656v160q0 17-11.5 28.5T880 856H720Zm20-60h120V676H740v120Zm-640 0V356v440Zm640-320V356v120Zm0 320V676v120Z"/></svg>
                                                                        <small style="color:white; display:block; text-align:center"> <?= count($napi_kepek); ?> db</small>
                                                                    </div>
                                                                    <div class="swiper-wrapper">
                                                                        <?php
                                                                        foreach ($napi_kepek as $key => $napi_kep) {
                                                                            $srcset = wp_get_attachment_image_srcset($key);
                                                                            $img_src = wp_get_attachment_image_src($key, 'large');
                                                                            $img_src_full = wp_get_attachment_image_src($key, 'full');
                                                                            ?>
                                                                            <div class="swiper-slide">
                                                                                <a href="<?php echo $img_src_full[0]; ?>" class="gallery-item" data-fancybox="group<?php echo $c; ?>"> <img src="<?php echo $img_src[0]; ?>"></a>
                                                                            </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <?php
                                                                } else {
                                                                    the_post_thumbnail('large');
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 p-0">
                                                    <div class="row tab-row day-row cbc-content mb-0">
                                                        <div class="day-wrapper">
                                                            <?php
                                                                if ($postCounter === 1) {
                                                            ?>
                                                                    <img class="cbc-arrow-img2" src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/start-arrow3.svg" style="width:20px;object-fit:cover;object-position:top;" />
                                                            <?php
                                                                } else if (count($tour_report_loop->posts) === $postCounter) {
                                                            ?>
                                                                    <img class="cbc-arrow-img2" src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/end-arrow3.svg" style="width:20px;object-fit:cover;object-position:bottom;" />
                                                            <?php
                                                                } else {
                                                            ?>
                                                                <img class="cbc-arrow-img2" src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/large-arrow3.svg" style="width:20px;object-fit:cover;object-position:bottom;" />
                                                            <?php
                                                                }
                                                            ?>
                                                        </div>
                                                        <div class="day-info cbc-info" style="padding-top:30px;padding-bottom: 30px;">
                                                            <p><strong><?php the_title(); ?></strong></p>
                                                            <?php if (has_excerpt()) { ?>
                                                                <p><strong><?php the_excerpt(); ?></strong></p>
                                                            <?php } ?>
                                                            <?php the_content(); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            $c++;
                                            $postCounter++;
                                        endwhile;
                                        ?>
                                    </div>
                                    <?php
                                endif;
                                wp_reset_query();
                                ?>
                            </div>
                        </div>
                    </div>
                </section>
                <!--accordion/tab-->
            </div>
            <?php
        endwhile;
    endif;
    rewind_posts();
    ?>
</main><!-- NOTE: Page content wrapper ENDS here -->

<?php get_footer(); ?>