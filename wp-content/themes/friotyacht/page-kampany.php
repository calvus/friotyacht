<?php
/*
 * Template name: Kampany oldal
 */
get_header();
?>

<!--
    TODO:
    1. Kampány aloldal létrehozása (template name)
    2. Contact from létrehozás majd hozzáadás az oldalhoz (magyar, angol)
    3. oldal hozzáadása a menühöz (angol magyar)
    4. kampany post type neyelvesites

 -->

<!-- NOTE: Page content wrapper STARTS here -->
<main class="page-content-wrapper" role="main">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <!-- NOTE: Page content -->
            <div class="container-fluid container--home">

                <!-- section counter -->
                <section class="section">
                    <div class="container bg-white">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <?php the_content(); ?>
                            </div>
                            <div class="col-12 col-lg-6">
                                <img src="<?= the_post_thumbnail_url(); ?>" class="cover-image" />
                            </div>
                        </div>


                    <!-- </div>

                    <div class="container"> -->
                        <?php
                        $loop = new WP_Query(
                            array(
                                'post_type' => 'campaigns',
                                'posts_per_page' => -1,
                                // 'order' => 'DESC',
                            )
                        );
                        $i = 1;
                        ?>
                        <?php if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post(); ?>
                                <div class="row mb-5 mt-5">
                                    <div class="col-12 col-lg-6 <?php echo $i % 2 ? "custom-order-1" : "custom-order-2"; ?>">
                                        <img src="<?= the_post_thumbnail_url(); ?>" class="cover-image mb-3 mb-lg-0" />
                                    </div>
                                    <div class="col-12 col-lg-6 <?php echo $i % 2 ? "custom-order-2" : "custom-order-1"; ?>">
                                        <h3 class="mb-3"><?php the_title(); ?></h3>
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                        <?php
                                $i++;
                            endwhile;
                        endif;
                        wp_reset_query();
                        ?>
                    </div>
                </section>
                <!-- /counter -->

            </div>
    <?php
        endwhile;
    endif;
    ?>
</main><!-- NOTE: Page content wrapper ENDS here -->

<?php get_footer() ?>