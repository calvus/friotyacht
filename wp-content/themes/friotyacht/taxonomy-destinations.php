<?php get_header(); ?>
<!-- NOTE: Page content wrapper STARTS here -->
<main class="page-content-wrapper" role="main">
    <!-- NOTE: Page content -->
    <div class="container-fluid container--home">
        <section class="section section--header section--destination-header" style="background-image: url('<?php
        if (function_exists('z_taxonomy_image_url')) {
            echo z_taxonomy_image_url(NULL, 'headerimg');
        }
        ?>');">
            <div class="container">
                <div class="row page-breadcrumb-row ml-0 mr-0">
                    <nav  class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <?php
                            $taxonomy_list_page_id = apply_filters('wpml_object_id', 89);
                            $term_list = wp_get_post_terms(get_the_ID(), 'destinations', array('fields' => 'all'));
                            ?>
                            <li class="breadcrumb-item">
                                <span><span><a href="<?php echo get_bloginfo('wpurl'); ?>"><?php _e('Home', 'friot'); ?></a> » 
                                        <span><a href="<?php echo get_permalink($taxonomy_list_page_id); ?>"><?php _e('Destinations', 'friot'); ?></a> » 
                                            <span class="breadcrumb_last" aria-current="page"><?php single_term_title(); ?></span>
                                        </span>
                                    </span>
                                </span>                               
                            </li>
                        </ol>
                    </nav>
                    <div class="button-wrapper col-12 col-md-4 p-0">

                    </div>
                </div>
                <div class="row m-0">
                    <div class="text-block">
                        <h1 class="banner-title"><?php single_term_title(); ?></h1>
                        <div class="floating-excerpt">
                            <p><?php echo term_description(); ?></p>
                        </div>
                    </div>
                    <div class="image-block">
                        <div class="image-wrapper">
                            <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/white-wave.png"/>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- cabin charter -->
        <section class="section section--card-block extra-padding bg-lightblue" id="cabin_charter">
            <div class="container">

                <div class="row section-title-row">
                    <?php /* <div class="col-12 col-md-6">
                      <h2 class="section-title"><?php single_term_title(); ?></h2>
                      </div> */ ?>
                    <div class="col-12">
                        <span class="section-subtitle"> <?php echo apply_filters('the_content', get_term_meta(get_queried_object_id(), 'term_long_description', true)); ?></span>
                    </div>
                </div>
                <!-- carousel display on small screen -->
                <div class="d-block d-lg-none mt-4">
                    <div class="carousel-wrapper">
                        <div class="owl-carousel card-layout-3">
                            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                    <div class="item">
                                        <div class="display-card">
                                            <a href="<?php the_permalink(); ?>">
                                                <div class="badge-container">
                                                    <div class="badge">
                                                        <span><?php the_title(); ?></span>
                                                    </div>
                                                </div>
                                                <?php the_post_thumbnail('areaimg'); ?>
                                                <div class="card-layer">
                                                    <p class="view"><?php _e('view', 'friot'); ?></p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <?php
                                endwhile;
                            endif;
                            rewind_posts();
                            ?>
                        </div>
                    </div>
                </div>
                <!--list view on large-->
                <div class = "d-none d-lg-block mt-4">
                    <div class = "row three-card-layout">
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                <div class="display-card">
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="badge-container">
                                            <div class="badge">
                                                <span><?php the_title(); ?></span>
                                            </div>
                                        </div>
                                        <?php the_post_thumbnail('areaimg'); ?>
                                        <div class="card-layer">
                                            <p class="view"><?php _e('view', 'friot'); ?></p>
                                        </div>
                                    </a>
                                </div>
                                <?php
                            endwhile;
                        endif;
                        rewind_posts();
                        ?>
                    </div>

                </div>
            </div>
        </section>
        <!--cabin charter-->
    </div>

</main><!-- NOTE: Page content wrapper ENDS here -->

<?php get_footer(); ?>