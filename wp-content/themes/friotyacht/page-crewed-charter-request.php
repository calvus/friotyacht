<?php
/*
 * Template name: Page Crewed Charter Request
 */
$bookingSiteLink = get_option('booking_site_link');
if (ICL_LANGUAGE_CODE == 'en') {
    $bookingSiteLink = get_option('booking_site_link_' . ICL_LANGUAGE_CODE);
}

$boat_types = json_decode(file_get_contents(get_option('booking_site_link') . 'yacht/ajax-yacht-categories?lang=' . ICL_LANGUAGE_CODE));

get_header();
?>
<!-- NOTE: Page content wrapper STARTS here -->
<main class="page-content-wrapper" role="main">
    <!-- NOTE: Page content -->
    <?php
    if (have_posts()) : while (have_posts()) : the_post();
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
            $charter_type = sanitize_text_field($_GET['type']);
            ?>
            <div class="container-fluid container--home">
                <section class="section section--header section--page-header" style="background-image: url('<?php echo $featured_img_url; ?>');">
                    <div class="container">
                        <div class="row page-breadcrumb-row ml-0 mr-0">
                            <nav style="--bs-breadcrumb-divider: '/';" class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <?php
                                    if (function_exists('yoast_breadcrumb')) {
                                        yoast_breadcrumb('<li class="breadcrumb-item">', '</li>');
                                    }
                                    ?>                  
                                </ol>
                            </nav>
                            <div class="button-wrapper col-12 col-md-4 p-0">

                            </div>
                        </div>
                        <div class="row m-0">
                            <div class="text-block w-100">
                                <h1 class="banner-title"><?php _e('Send Enquiry', 'friot'); ?></h1>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- section counter -->
                <section class="section section--contact-form bg-parallax">
                    <div class="container">
                        <p class="text-center mb-4"><strong><?php echo $charter_type; ?> <?php _e('Enquiry', 'friot'); ?></strong></p>
                        <?php
                        $postedToken = filter_input(INPUT_POST, 'token');
                        if (isset($_POST['action']) && ($_POST['action'] == "crewed_charter_request")) {
                            $recaptcha_response = recaptcha_validate($_POST['googletoken']);
                            if (!$recaptcha_response['success'] || $recaptcha_response['score'] < 0.5 || $recaptcha_response['action'] != 'request_send_form') {
                                $url = get_bloginfo('wpurl');
                                wp_safe_redirect($url);
                                die();
                            } else {
                                if (!empty($postedToken)) {
                                    if (isTokenValid($postedToken)) {
                                        if (!isset($_POST['crewedcharterrequestcheck']) || !wp_verify_nonce($_POST['crewedcharterrequestcheck'], 'crewedcharterrequest')) {
                                            $redirect_to = get_bloginfo('wpurl');
                                            wp_safe_redirect($redirect_to);
                                            die();
                                        } else {
                                            $has_error = false;
                                            $reqired_fields = ['crewed_charter_type', 'firstname', 'lastname', 'email_address', 'phone', 'destination', 'ship_type', 'ship_age', 'number_of_persons', 'date_from', 'date_to'];
                                            foreach ($reqired_fields as $reqired_field) {
                                                if (empty($_POST[$reqired_field])) {
                                                    $error = __("Required field is empty!", 'friot');
                                                    $has_error = true;
                                                }
                                            }

                                            $crewed_charter_type = sanitize_text_field($_POST['crewed_charter_type']);
                                            $firstname = sanitize_text_field($_POST['firstname']);
                                            $lastname = sanitize_text_field($_POST['lastname']);
                                            $email_address = sanitize_email($_POST['email_address']);
                                            $phone = sanitize_text_field($_POST['phone']);
                                            $destination = sanitize_text_field($_POST['destination']);
                                            $ship_type = sanitize_text_field($_POST['ship_type']);
                                            $ship_age = sanitize_text_field($_POST['ship_age']);
                                            $number_of_persons = sanitize_text_field($_POST['number_of_persons']);
                                            $date_from = sanitize_text_field($_POST['date_from']);
                                            $date_to = sanitize_text_field($_POST['date_to']);

                                            $other_information = sanitize_textarea_field($_POST['other_information']);
                                            $sentids = true;

                                            // $ship_type_array = array(__('Catamaran', 'friot'), __('Sailing Yacht', 'friot'), __('Motor', 'friot'));
                                            if (!in_array($ship_type, $boat_types)) {
                                                $sentids = false;
                                            }


                                            $ship_age_array = array(__('any', 'friot'), __('0-1 year', 'friot'), __('1-3 years', 'friot'), __('3-5 years', 'friot'), __('5-10 years', 'friot'), __('10-15 years', 'friot'));
                                            if (!in_array($ship_age, $ship_age_array)) {
                                                $sentids = false;
                                            }


                                            if ($sentids) {

                                                if (!$has_error) {

                                                    // ide jön az checkelés és email kiküldés
                                                    $content .= "<strong>" . __("Crew Charter", "friot") . "</strong>: " . $crewed_charter_type . "<br/>";
                                                    $content .= "<strong>" . __("Destinations", "friot") . "</strong>: " . $destination . "<br/>";
                                                    $content .= "<strong>" . __("Dátum", "friot") . "</strong>: " . $date_from . ' - ' . $date_to . "<br/>";
                                                    $content .= "<strong>" . __("Selected boat type", "friot") . "</strong>: " . $ship_type . "<br/>";
                                                    $content .= "<strong>" . __("Selected boat age", "friot") . "</strong>: " . $ship_age . "<br/><br/>";
                                                    $content .= "<strong>" . __("Number of Persons", "friot") . "</strong>: " . $number_of_persons . "<br/><br/>";
                                                    $content .= "<strong>" . __("Name", "friot") . "</strong>: " . $firstname . " " . $lastname . "<br/>";
                                                    $content .= "<strong>" . __("Email", "friot") . "</strong>: " . $email_address . "<br/> ";
                                                    $content .= "<strong>" . __("Telefon", "friot") . "</strong>: " . $phone . "<br/> ";

                                                    if (!empty($other_information)) {
                                                        $content .= "<br/><strong>" . __("Other information", "friot") . "</strong>:<br/>" . $other_information . "<br/> ";
                                                    }


                                                    $bookings = array(
                                                        'post_author' => 1,
                                                        'post_status' => 'private',
                                                        'post_title' => $firstname . " " . $lastname,
                                                        'post_content' => $content,
                                                        'post_type' => 'bookings'
                                                    );
                                                    $post_id = wp_insert_post($bookings);


                                                    if ($post_id) {
                                                        $crewed_charter = get_page_by_title($crewed_charter_type, OBJECT, 'crewed-charter');

                                                        // check to make sure its a successful upload             
                                                        update_post_meta($post_id, '_cabin_charter_bookings_mbox_tura_id', $crewed_charter->ID);
                                                        update_post_meta($post_id, '_cabin_charter_bookings_mbox_phone', $phone);
                                                        update_post_meta($post_id, '_cabin_charter_bookings_mbox_email', $email_address);
                                                    }
                                                    $subject = "Friot Yacht - _" . __("Crew Charter Enquiry", "friot") . $tura_neve;
                                                    $headers = array('Content-Type: text/html; charset=UTF-8');
                                                    $headers[] = 'From: Friot Yacht  <info@friotyacht.com>';
                                                    wp_mail('info@friotyacht.com', $subject, $content, $headers);
                                                    //   wp_mail('oliver.szenyi@calv.us', $subject, $content, $headers);
                                                    ?>
                                                    <h2><?php _e('Your Enquiry has been sent', 'friot'); ?></h2>
                                                    <?php echo $content; ?>
                                                    <?php
                                                } else {
                                                    
                                                }
                                            } else {
                                                $redirect_to = get_bloginfo('wpurl');
                                                wp_safe_redirect($redirect_to);
                                                die();
                                            }
                                        }
                                    } else {
                                        $url = get_bloginfo('wpurl');
                                        wp_safe_redirect($url);
                                        die();
                                    }
                                } else {
                                    $url = get_bloginfo('wpurl');
                                    wp_safe_redirect($url);
                                    die();
                                }
                            }
                        } else {
                            ?>

                            <form action="<?php the_permalink(); ?>" method="POST"  class="crewedcharterrequest-form" id="crewedcharterrequest_form" name="crewedcharterrequest_form"> 
                                <?php $token = getToken();
                                ?>
                                <input type="hidden" name="googletoken" id="googletoken">
                                <input type="hidden" name="token" value="<?php echo $token; ?>"/>
                                <input type="hidden" name="action" value="crewed_charter_request" />
                                <input type="hidden" name="crewed_charter_type" value="<?php echo sanitize_text_field($_GET['type']); ?>" />
                                <div class="row">
                                    <div class="form-group col-12 col-md-6">
                                        <label for="firstname" class="form-label"><?php _e('Name', 'friot'); ?> *</label>
                                        <input type="text" class="form-control" id="firstname" name="firstname" placeholder="<?php _e('First name', 'friot'); ?>" required="required" value="<?php echo sanitize_text_field($_POST['firstname']); ?>">
                                    </div>
                                    <div class="form-group col-12 col-md-6">
                                        <label for="lastname" class="form-label">&nbsp;</label>
                                        <input type="text" class="form-control" id="lastname" name="lastname" placeholder="<?php _e('Last name', 'friot'); ?>" required="required"  value="<?php echo sanitize_text_field($_POST['lastname']); ?>">
                                    </div>
                                    <div class="form-group col-12 col-md-6">
                                        <label for="email_address" class="form-label"><?php _e('E-mail', 'friot'); ?> *</label>
                                        <input type="email" class="form-control" id="email_address" name="email_address" placeholder="<?php _e('E-mail', 'friot'); ?>" required="required"   value="<?php echo sanitize_email($_POST['email_address']); ?>">
                                    </div>
                                    <div class="form-group col-12 col-md-6">
                                        <label for="phone" class="form-label"><?php _e('Phone', 'friot'); ?> *</label>
                                        <input type="text" class="form-control" id="phone" name="phone" placeholder="<?php _e('Phone', 'friot'); ?>" required="required"   value="<?php echo sanitize_text_field($_POST['phone']); ?>">
                                    </div>  

                                    <div class="form-group col-12 col-md-6">
                                        <?php $ship_type = sanitize_text_field($_POST['ship_type']); ?>
                                        <label for="ship_type" class="form-label"><?php _e('Boat Type', 'friot'); ?> *</label>
                                        <select class="form-control" id="ship_type" name="ship_type" placeholder="" required="required">
                                            <option value=""><?php _e(' --- Please Select --- ', 'friot'); ?></option>                                  
                                            <?php foreach ($boat_types as $boat_type) { ?>
                                                <option value="<?php echo $boat_type; ?>"<?php
                                                if ($ship_type == $boat_type) {
                                                    echo 'selected';
                                                }
                                                ?> ><?php echo $boat_type; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div> 
                                    <div class="form-group col-12 col-md-6">
                                        <?php $ship_age = sanitize_text_field($_POST['ship_age']); ?>
                                        <label for="ship_age" class="form-label"><?php _e('Boat Age', 'friot'); ?> *</label>
                                        <select class="form-control" id="ship_age" name="ship_age" placeholder="" required="required">
                                            <option value=""><?php _e(' --- Please Select --- ', 'friot'); ?></option>
                                            <option value="<?php _e('any', 'friot'); ?>" <?php
                                            if ($ship_age == __('any', 'friot')) {
                                                echo 'selected';
                                            }
                                            ?>><?php _e('any', 'friot'); ?></option>
                                            <option value="<?php _e('0-1 year', 'friot'); ?>" <?php
                                            if ($ship_age == __('0-1 year', 'friot')) {
                                                echo 'selected';
                                            }
                                            ?>><?php _e('0-1 year', 'friot'); ?></option>
                                            <option value="<?php _e('1-3 years', 'friot'); ?>" <?php
                                            if ($ship_age == __('1-3 years', 'friot')) {
                                                echo 'selected';
                                            }
                                            ?>><?php _e('1-3 years', 'friot'); ?></option>
                                            <option value="<?php _e('3-5 years', 'friot'); ?>" <?php
                                            if ($ship_age == __('3-5 years', 'friot')) {
                                                echo 'selected';
                                            }
                                            ?>><?php _e('3-5 years', 'friot'); ?></option>
                                            <option value="<?php _e('5-10 years', 'friot'); ?>" <?php
                                            if ($ship_age == __('5-10 years', 'friot')) {
                                                echo 'selected';
                                            }
                                            ?>><?php _e('5-10 years', 'friot'); ?></option>
                                            <option value="<?php _e('10-15 years', 'friot'); ?>" <?php
                                            if ($ship_age == __('10-15 years', 'friot')) {
                                                echo 'selected';
                                            }
                                            ?>><?php _e('10-15 years', 'friot'); ?></option>
                                        </select>
                                    </div> 
                                    <div class="form-group col-12 col-md-6">
                                        <label for="destination" class="form-label"><?php _e('Destination', 'friot'); ?> *</label>
                                        <input type="text" class="form-control" id="destination" name="destination" placeholder="<?php _e('Destination', 'friot'); ?>" required="required"  value="<?php echo sanitize_text_field($_POST['destination']); ?>">
                                    </div>
                                    <div class="form-group col-12 col-md-6">
                                        <label for="number_of_persons" class="form-label"><?php _e('Number of Persons', 'friot'); ?> *</label>
                                        <input type="number" class="form-control" id="number_of_persons" name="number_of_persons" placeholder="<?php _e('Number of Persons', 'friot'); ?>" min="1" max="100" required="required" value="<?php echo sanitize_text_field($_POST['number_of_persons']); ?>">
                                    </div>  
                                    <div class="form-group col-12 col-md-6 ">
                                        <?php $today = date('Y-m-d'); ?>
                                        <label for="date_from" class="form-label"><?php _e('Date from', 'friot'); ?> *</label>
                                        <input type="date" class="form-control" id="date_from" name="date_from" placeholder="" min="<?php echo $today; ?>" defaultValue="<?php echo $today; ?>" required="required"  value="<?php echo sanitize_text_field($_POST['date_from']); ?>">
                                    </div>
                                    <div class="form-group col-12 col-md-6">
                                        <label for="date_to" class="form-label"><?php _e('Date to', 'friot'); ?> *</label>
                                        <input type="date" class="form-control" id="date_to" name="date_to" placeholder="" min="<?php echo $today; ?>" defaultValue="<?php echo $today; ?>" required="required"  value="<?php echo sanitize_text_field($_POST['date_to']); ?>">
                                    </div> 
                                    <div class="form-group col-12 ">
                                        <label for="other_information" class="form-label"><?php _e('Other information', 'friot'); ?></label>
                                        <textarea class="form-control" id="other_information" name="other_information" rows="5"> <?php echo sanitize_textarea_field($_POST['other_information']); ?></textarea>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="turahajo_aszf" name="turahajo_aszf">
                                        <label class="form-check-label" for="turahajo_aszf">
                                            <?php $terms_and_conditions_id = apply_filters('wpml_object_id', 72); ?>
                                            <?php _e("I accept the", "friot"); ?> <a href="<?php echo get_permalink($terms_and_conditions_id); ?>"  target="_blank"><?php echo get_the_title($terms_and_conditions_id); ?></a><?php _e("et", "friot"); ?>
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="privacy_chk" name="privacy_chk">
                                        <label class="form-check-label" for="privacy_chk">
                                            <?php $privacy_policy_id = apply_filters('wpml_object_id', 3); ?>
                                            <?php _e("I accept the", "friot"); ?> <a href="<?php echo get_permalink($privacy_policy_id); ?>"  target="_blank"><?php echo get_the_title($privacy_policy_id); ?></a><?php _e("t", "friot"); ?>
                                        </label>
                                    </div>                                   
                                </div>
                                <div class="button-wrapper mt-4 text-center">
                                    <a id="crewedcharterrequest_send" class="btn btn--red"><?php _e('Send Enquiry', 'friot'); ?></a>
                                </div>
                                <?php wp_nonce_field('crewedcharterrequest', 'crewedcharterrequestcheck'); ?>
                            </form>
                        <?php } ?>
                    </div>
                </section>
                <!-- /counter -->

            </div>
            <?php
        endwhile;
    endif;
    ?>
</main><!-- NOTE: Page content wrapper ENDS here -->
<?php get_footer(); ?>