<?php

/*
 * Template name: PDF letöltő
 */

$pdf_download_id = sanitize_text_field($_GET['pdf']);
if (empty($pdf_download_id)) {
    wp_safe_redirect(get_bloginfo('wpurl'));
    exit;
} else {
    $email = sanitize_email(urldecode($_GET['email']));
    $salt = "gQ5}dO9{zM2_bX3~";
    $download_code = hash('sha512', $email . $salt);
    if ($pdf_download_id === $download_code) {

        $file_path = '/opt/data/friotyacht_tippek_es_tanacsok_ebook-1.pdf';
        $file_name = 'friotyacht_tippek_es_tanacsok_ebook-1.pdf';
        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment; filename="' . $file_name . '"');
        readfile($file_path);
    } else {
        wp_safe_redirect(get_bloginfo('wpurl'));
        exit;
    }
}


