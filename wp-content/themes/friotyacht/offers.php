<!-- section Boat rental -->
<?php
?>
<link
  rel="stylesheet"
  href="https://unpkg.com/swiper@8/swiper-bundle.min.css"
/>
<link
  rel="stylesheet"
  href="https://unpkg.com/browse/swiper@8.0.7/swiper.min.css"
/>
<section class="section section--card-block from-booking" id="boat_rental_cards" style="overflow:hidden;">
    <div class="container-fluid">
        <?php
        $offers_transient = offers();
        echo $offers_transient;
        ?>

    </div>
</section>
<script src="https://unpkg.com/swiper@8/swiper-bundle.min.js"></script>
<script>
    new Swiper(".mySwiper1", {
            slidesPerView: 1,
            centeredSlides: true,
            loop: true,
            spaceBetween: 20,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            navigation: {
                nextEl: ".button-next",
                prevEl: ".button-prev",
            },
            breakpoints: {
                768: {
                    slidesPerView: 2,
                    spaceBetween: 40,
                },
                1200: {
                    slidesPerView: 4,
                },
            },
        });
</script>
<!-- /Boat rental -->