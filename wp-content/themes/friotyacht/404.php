<?php get_header(); ?>

<!-- NOTE: Page content wrapper STARTS here -->
<main class="page-content-wrapper" role="main">

    <!-- NOTE: Page content -->
    <div class="container-fluid container--home">
        <section class="section section--header nobg-header">
            <div class="container">
                <div class="row page-breadcrumb-row ml-0 mr-0">
                    <nav style="--bs-breadcrumb-divider: '/';" class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <?php
                            if (function_exists('yoast_breadcrumb')) {
                                yoast_breadcrumb('<li class="breadcrumb-item">', '</li>');
                            }
                            ?>                                    
                        </ol>
                    </nav>
                    <div class="button-wrapper col-12 col-md-4 p-0">
                        
                    </div>
                </div>
                <div class="row m-0">
                    <div class="text-block w-100">
                        <h1 class="banner-title">404</h1>
                    </div>
                </div>
            </div>
        </section>

        <!-- section counter -->
        <section class="section section--contact-form bg-parallax">
            <div class="container">

                <p class="text-center mb-4"><strong><?php _e('Page not found!', 'friot'); ?></strong></p>
                <p class="text-center mb-4"><?php _e('Sorry, the page you requested has not been found. Please return to our', 'friot'); ?> <a href="<?php echo get_bloginfo('wpurl'); ?>"><?php _e('Homepage', 'friot'); ?></a></p>
                <p class="text-center mb-4"><?php _e('Sorry for the Inconvenience.', 'friot'); ?></p>
            </div>
        </section>
        <!-- /counter -->

    </div>

</main><!-- NOTE: Page content wrapper ENDS here -->

<?php
get_footer()?>