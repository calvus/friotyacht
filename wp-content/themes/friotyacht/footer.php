
<!-- NOTE: Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/logo-white.svg" />
            </div>
            <div class="divider divider-first d-block d-md-none"></div>
            <div class="col-12 col-md-6">
                <div class="row social-row">
                    <a href="<?php echo get_option('facebook_link'); ?>" class="social-icon" target="_blank"><img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/facebook.svg" /></a>
                    <a href="<?php echo get_option('instagram_link'); ?>" class="social-icon" target="_blank"><img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/instagram.svg" /></a>
                    <a href="<?php echo get_option('youtube_link'); ?>" class="social-icon" target="_blank"><img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/youtube.svg" /></a>
                </div>
            </div>
            <div class="divider d-block d-md-none"></div>
            <div class="col-12 col-lg-6">
                <?php dynamic_sidebar('footer-contact'); ?>
                <div class="row">
                    <?php dynamic_sidebar('footer-0'); ?>
                </div>
            </div>
            <div class="divider d-block d-lg-none"></div>
            <div class="col-12 col-lg-6">
                <?php dynamic_sidebar('footer-newsletterhead'); ?>
                <div class="row">
                    <?php
                    if (function_exists('newsletter_subscribe_show_form')) {
                        newsletter_subscribe_show_form();
                    }
                    ?>
                </div>
            </div>
            <div class="divider d-block d-lg-none"></div>
            <div class="col-12 pl-0 pr-0">
                <div class="row mt-4">
                    <div class="col-12 col-md-6 col-lg-3">
                        <?php dynamic_sidebar('footer-1'); ?>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <?php dynamic_sidebar('footer-2'); ?>

                    </div>
                    <div class="col-12 col-md-6 col-lg-3">

                        <p class="footer-subtitle"><?php _e('Cabin Charter', 'friot') ?></p>
                        <div class="menu-vezetett-turak-container">
                            <ul id="menu-vezetett-turak" class="menu">
                                <li class="menu-item menu-item-type-post_type">
                                    <a href="<?php echo get_post_type_archive_link('turabeszamolok'); ?>"><?php _e('Túrabeszámolók', 'friot'); ?></a>
                                </li>

                                <?php /* if (get_locale() === 'hu_HU') { ?>
                                  <li class="menu-item menu-item-type-post_type">
                                  <a href="https://friotyacht.calvus.xyz/turabeszamolok/">Túrabeszámolók</a>
                                  </li>
                                  <?php } else { ?>
                                  <li class="menu-item menu-item-type-post_type">
                                  <a href="https://friotyacht.calvus.xyz/en/tourreports/">Tour reports</a>
                                  </li>
                                  <?php } */ ?>
                                <?php
                                $args = array('post_type' => 'cabin-charter', 'posts_per_page' => -1);
                                $loop = new WP_Query($args);
                                if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
                                        ?>
                                        <li class="menu-item menu-item-type-post_type">
                                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                        </li>
                                        <?php
                                    endwhile;
                                endif;
                                ?>
                            </ul>
                        </div>


                    </div>
                    <div class="col-12 col-md-6 col-lg-3">
                        <?php dynamic_sidebar('footer-4'); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

</div><!-- NOTE: Main container ENDS here -->
<?php wp_footer(); ?>
<!-- JavaScript -->
<!-- AddToAny -->
<script async src="https://static.addtoany.com/menu/page.js"></script>
<script src="<?php echo get_bloginfo('wpurl'); ?>/includes/js/jquery-3.5.1.min.js"></script>
<script src="<?php echo get_bloginfo('wpurl'); ?>/includes/js/popper.min.js"></script>
<script src="<?php echo get_bloginfo('wpurl'); ?>/includes/js/bootstrap.min.js"></script>
<script src="<?php echo get_bloginfo('wpurl'); ?>/includes/js/owl.carousel.min.js"></script>
<script src="<?php echo get_bloginfo('wpurl'); ?>/includes/js/swiper.min.js"></script>
<script src="<?php echo get_bloginfo('wpurl'); ?>/includes/js/litepicker.min.js"></script>
<script src="<?php echo get_bloginfo('wpurl'); ?>/includes/js/jquery.magnific-popup.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<?php if (ICL_LANGUAGE_CODE == 'hu') { ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.hu.min.js" integrity="sha512-hUj94GVUcMtQpARqIUR1qfiM9hFGW/sOKx6pZVEyuqUSYbjSw/LjQbjuXpFVfKqy8ZeYbDxylIm6D/KIfcJbTQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <?php
}
/* * local -- 2152 */
/* * calvus.xyz -- 2151 */
/* * éles -- 2164 */
$friot_options = get_option('friot_options');
$request_form_id_option = $friot_options['tura_ajanlatkero'];
$request_form_id = apply_filters('wpml_object_id', $request_form_id_option);
$privacy_policy_id = apply_filters('wpml_object_id', 3);
?>

<script src="<?php echo get_bloginfo('wpurl'); ?>/includes/js/scripts.min.js?v=0.12"></script>

<script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>
<script>
    var translations_request_forms = {
        select_cabin: '<?php _e('Select cabin', 'friot'); ?>',
        please_select_a_boat: '<?php _e('Please select a boat!', 'friot') ?>',
        please_select_a_cabin: '<?php _e('Please select a cabin!', 'friot') ?>',
        accept_terms: '<?php _e('Accepting the Terms and Conditions is mandatory!', 'friot') ?>',
        accept_policy: '<?php _e('Accepting the Privacy Policy is mandatory!', 'friot') ?>',
        accept_conditions: '<?php _e('Accepting the Booking Conditions is mandatory!', 'friot') ?>',
        enter_first_name: '<?php _e('Please enter First name!', 'friot') ?>',
        enter_last_name: '<?php _e('Please enter Last name!', 'friot') ?>',
        enter_email: '<?php _e('Please enter Email!', 'friot') ?>',
        error_email: '<?php _e('Error in Email!', 'friot') ?>',
        enter_phone: '<?php _e('Please enter Phone!', 'friot') ?>!',
        request_form_id: '<?php echo get_permalink($request_form_id); ?>',
        enter_destination: '<?php _e('Please enter Destination!', 'friot') ?>',
        please_select_a_boat_type: '<?php _e('Please select Boat type!', 'friot') ?>!',
        please_select_a_boat_age: '<?php _e('Please select Boat age!', 'friot') ?>!',
        please_select_number_of_persons: '<?php _e('Please select Number of Persons!', 'friot') ?>!',
        please_select_date_from: '<?php _e('Please select a Date From!', 'friot') ?>!',
        please_select_date_to: '<?php _e('Please select a Date To!', 'friot') ?>!',
        please_select_budget: '<?php _e('Please select Budget!', 'friot') ?>!',
        please_select_skipper: '<?php _e('Please select if Skipper is needed!', 'friot') ?>!'
    };
    var ICL_LANGUAGE_CODE = '<?php echo ICL_LANGUAGE_CODE; ?>';
    var translations_cookieconsent = {
        got_it: '<?php _e("Got it!", "friot") ?>',
        privacy_policy_link: '<?php echo get_permalink($privacy_policy_id); ?>',
        learn_more: '<?php _e("Learn more", "friot") ?>',
        cookie_text: '<?php _e("This website uses cookies to ensure you get the best experience on our website.", "friot") ?>'

    }
</script>
<script src="<?php echo get_bloginfo('wpurl'); ?>/includes/js/scripts.min.js?v=0.12"></script>
</body>
</html>
