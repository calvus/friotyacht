<?php
/*
 * Template name: Konzultáció oldal
 */

get_header(); ?>

<!-- NOTE: Page content wrapper STARTS here -->
<main class="page-content-wrapper" role="main">
    <?php
    if (have_posts()) : while (have_posts()) : the_post();
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
    ?>
            <!-- NOTE: Page content -->
            <div class="container-fluid container--home">
                <section class="section section--header section--page-header fullpage-banner" style="background-image: url('<?php echo $featured_img_url; ?>');">
                    <div class="container">
                        <div class="row page-breadcrumb-row ml-0 mr-0">
                            <nav style="--bs-breadcrumb-divider: '/';" class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <?php
                                    if (function_exists('yoast_breadcrumb')) {
                                        yoast_breadcrumb('<li class="breadcrumb-item">', '</li>');
                                    }
                                    ?>
                                </ol>
                            </nav>
                            <div class="button-wrapper col-12 col-md-4 p-0">

                            </div>
                        </div>
                        <div class="row align-items-start">
                            <div class="col-12 col-lg-6">
                                <div class="white-box">
                                    <?php if (has_excerpt()) { ?>
                                        <div class="excerpt">
                                            <h4 class="mb-4"><strong><?php the_excerpt(); ?></strong></h4>
                                        </div>
                                    <?php } ?>
                                    <?php echo the_content(); ?>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="white-box">
                                    <?php

                                    echo do_shortcode('[contact-form-7 id="3798" title="Konzultacio"]');
                                    $thankyou_page_id = apply_filters('wpml_object_id', 3815);
                                    ?>
                                    <script>
                                        document.addEventListener('wpcf7mailsent', function(event) {
                                            // if ('3798' == event.detail.contactFormId) {
                                            //     // alert("The contact form ID is 3798.");
                                            // }
                                            window.location.href = "<?php echo get_permalink($thankyou_page_id); ?>"
                                        }, false);
                                    </script>
                                    <!-- textarea 7 rows -->
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
    <?php
        endwhile;
    endif;
    ?>
</main><!-- NOTE: Page content wrapper ENDS here -->

<?php get_footer() ?>