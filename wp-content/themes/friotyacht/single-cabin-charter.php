<?php get_header(); ?>
<!-- NOTE: Page content wrapper STARTS here -->
<main class="page-content-wrapper" role="main">
    <?php
    if (have_posts()) : while (have_posts()) : the_post();
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
            ?>
            <!-- NOTE: Page content -->
            <div class="container-fluid container--home">
                <section class="section section--header section--destination-header" style="background-image: url('<?php echo esc_url($featured_img_url); ?>');">
                    <div class="container">
                        <div class="row page-breadcrumb-row ml-0 mr-0">
                            <nav class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <?php
                                    if (function_exists('yoast_breadcrumb')) {
                                        yoast_breadcrumb('<li class="breadcrumb-item">', '</li>');
                                    }
                                    ?>
                                </ol>
                            </nav>
                            <div class="button-wrapper col-12 col-md-4 p-0">

                            </div>
                        </div>
                        <div class="row m-0">
                            <div class="text-block text-center w-100">
                                <h1 class="banner-title --sm-title"><?php _e('Cabin charter', 'friot') ?></h1>
                                <div class="divider"></div>
                                <h1 class="banner-sub-title"><?php the_title(); ?></h1>

                            </div>
                        </div>
                    </div>
                </section>

                <!-- tour information -->
                <section class="section section--ship-info bg-lightblue">
                    <div class="container">
                        <div class="inner">
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <?php if (!get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_pre_registration', true) || get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_pre_registration', true) != "on") { ?>
                                        <div class="badge-container">
                                            <div class="badge">
                                                <?php
                                                $free_cabins = intval(get_avaible_cabins_on_tour(get_the_id()));
                                                if ($free_cabins == 0) {
                                                    ?>
                                                    <span> <?php _e('Minden kabin eladva', 'friot'); ?></span>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <span>  <?php echo sprintf(__('Már csak %s kabin <br /> érhető el!', 'friot'), $free_cabins); ?></span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    $images = array();
                                    $galleries = get_post_galleries($post->ID, false);

                                    $featured_img_slider = get_the_post_thumbnail_url(get_the_ID(), 'charter_slider');
                                    $featured_img_slider_full = get_the_post_thumbnail_url(get_the_ID(), 'headerimg');
                                    ?>
                                    <div thumbsslider="" class="swiper" id="swiper-bottom">

                                        <div class="swiper-wrapper">
                                            <?php if (!empty($featured_img_slider)) { ?>
                                                <div class="swiper-slide">
                                                    <a href="<?php echo esc_url($featured_img_slider_full); ?>" class="gallery-item" data-fancybox="group1">
                                                        <img src="<?php echo esc_url($featured_img_slider); ?>">
                                                    </a>
                                                </div>
                                                <?php
                                            }
                                            /*  unset($exclude_image_ids);
                                              $general_information_image = get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_general_information_image', true);
                                              $attachment_id = attachment_url_to_postid($general_information_image);
                                              $exclude_image_ids[] = $attachment_id;
                                              $take_with_me_image = get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_take_with_me_image', true);
                                              $attachment_id = attachment_url_to_postid($take_with_me_image);
                                              $exclude_image_ids[] = $attachment_id;
                                              $whats_included_image = get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_whats_included_image', true);
                                              $attachment_id = attachment_url_to_postid($whats_included_image);
                                              $exclude_image_ids[] = $attachment_id;
                                              $whats_not_included_image = get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_whats_not_included_image', true);
                                              $attachment_id = attachment_url_to_postid($whats_not_included_image);
                                              $exclude_image_ids[] = $attachment_id; */
                                            foreach ($galleries as $gallery) {
                                                if (!empty($gallery['ids'])) {

                                                    $image_ids = explode(',', $gallery['ids']);
                                                    $first = true;
                                                    $i = 0;
                                                    foreach ($image_ids as $image_id) {

                                                        /*   if (in_array($image_id, $exclude_image_ids)) {
                                                          continue;
                                                          } */
                                                        $src_full = wp_get_attachment_image_src($image_id, 'full', false);
                                                        $src_large = wp_get_attachment_image_src($image_id, 'charter_slider', false);
                                                        ?>
                                                        <div class="swiper-slide">
                                                            <a href="<?php echo $src_full[0]; ?>" class="gallery-item" data-fancybox="group1"> <img src="<?php echo $src_large[0]; ?>"></a>
                                                        </div>
                                                        <?php
                                                        $i++;
                                                    }
                                                }
                                            }
                                            ?>

                                        </div>
                                    </div>
                                    <div class="swiper" id="swiper-top">
                                        <div class="swiper-wrapper">
                                            <?php if (!empty($featured_img_slider)) { ?>
                                                <div class="swiper-slide">
                                                    <img src="<?php echo esc_url($featured_img_url); ?>">
                                                </div>
                                                <?php
                                            }
                                            foreach ($galleries as $gallery) {
                                                if (!empty($gallery['ids'])) {

                                                    $image_ids = explode(',', $gallery['ids']);
                                                    $first = true;
                                                    $i = 0;
                                                    foreach ($image_ids as $image_id) {
                                                        /*  if (in_array($imageID, $exclude_image_ids)) {
                                                          continue;
                                                          } */
                                                        $src_large = wp_get_attachment_image_src($image_id, 'charter_slider_thumb', false);
                                                        ?>
                                                        <div class="swiper-slide">
                                                            <img src="<?php echo $src_large[0]; ?>">
                                                        </div>
                                                        <?php
                                                        $i++;
                                                    }
                                                }
                                            }
                                            ?>

                                        </div>
                                        <div class="swiper-button-next"></div>
                                        <div class="swiper-button-prev"></div>
                                    </div>


                                </div>
                                <div class="col-12 col-lg-6">
                                    <h2><?php the_title(); ?></h2>
                                    <?php if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_pre_registration', true) && get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_pre_registration', true) == "on") {
                                        ?>
                                        <p class="info-text-bold"> <span class="text-blue"><?php echo mysql2date('Y. m.', get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_date_from', true), 0, '', ' '); ?></span> </p>
                                    <?php } else {
                                        ?>
                                        <p class="info-text-bold"> <span class="text-blue"><?php echo mysql2date('Y. m. d.', get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_date_from', true), 0, '', ' '); ?></span> - <span class="text-blue"><?php echo mysql2date('Y. m. d.', get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_date_to', true), 0, '', ' '); ?></span></p>
                                        <?php
                                    }
                                    if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_pre_registration', true) && get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_pre_registration', true) == "on") {
                                        ?><p class="info-text-bold"><?php _e('From', 'friot'); ?> <span class="text-blue">€<?php echo number_format(intval(get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_price_from', true)), 0, '', ' '); ?></span>  /<?php _e('people', 'friot_cabin_charter'); ?></p> <?php
                                        } else {
                                            if ($free_cabins == 1) {
                                                if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_discount', true)) {
                                                    $discount_number = intval(get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_discount', true));
                                                    $discount_price_from = (1 - ($discount_number / 100)) * intval(get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_price_from', true));
                                                    ?>
                                                <p class="info-text-bold"><small><span class="strike-trough"><span><?php _e('Price', 'friot'); ?> <span class="text-blue">€<?php echo number_format(intval(get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_price_from', true)), 0, '', ' '); ?></span> /<?php _e('people', 'friot_cabin_charter'); ?></span></span></small></p>
                                                <p class="info-text-bold"><?php _e('Price', 'friot'); ?> <span class="text-red">€<?php echo number_format($discount_price_from, 0, '', ' '); ?></span> /<?php _e('people', 'friot_cabin_charter'); ?> <span class="discount-percent">-<?php echo intval(get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_discount', true)); ?>%</span></p>
                                            <?php } else { ?>
                                                <p class="info-text-bold"><?php _e('Price', 'friot'); ?> <span class="text-blue">€<?php echo number_format(intval(get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_price_from', true)), 0, '', ' '); ?></span> /<?php _e('people', 'friot_cabin_charter'); ?> </p>
                                                <?php
                                            }
                                        } else {
                                            if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_discount', true)) {
                                                $discount_number = intval(get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_discount', true));
                                                $discount_price_from = (1 - ($discount_number / 100)) * intval(get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_price_from', true));
                                                $discount_price_to = (1 - ($discount_number / 100)) * intval(get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_price_to', true));
                                                ?>
                                                <p class="info-text-bold"><small><span class="strike-trough"><span><?php _e('From', 'friot'); ?> <span class="text-blue">€<?php echo number_format(intval(get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_price_from', true)), 0, '', ' '); ?></span> <?php _e('to', 'friot'); ?> <span class="text-blue">€<?php echo number_format(intval(get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_price_to', true)), 0, '', ' '); ?></span> /<?php _e('people', 'friot_cabin_charter'); ?></span></span></small></p>
                                                <p class="info-text-bold"><?php _e('From', 'friot'); ?> <span class="text-red">€<?php echo number_format($discount_price_from, 0, '', ' '); ?></span> <?php _e('to', 'friot'); ?> <span class="text-red">€<?php echo number_format($discount_price_to, 0, '', ' '); ?></span> /<?php _e('people', 'friot_cabin_charter'); ?>       <span class="discount-percent">-<?php echo intval(get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_discount', true)); ?>%</span></p>

                                                <?php
                                            } else {
                                                ?>
                                                <p class="info-text-bold"><?php _e('From', 'friot'); ?> <span class="text-blue">€<?php echo number_format(intval(get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_price_from', true)), 0, '', ' '); ?></span> <?php _e('to', 'friot'); ?> <span class="text-blue">€<?php echo number_format(intval(get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_price_to', true)), 0, '', ' '); ?></span> /<?php _e('people', 'friot_cabin_charter'); ?></p>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style my-3">
                                        <a class="a2a_button_facebook"></a>
                                        <a class="a2a_button_twitter"></a>
                                        <a class="a2a_button_email"></a>
                                        <a class="a2a_button_facebook_messenger"></a>
                                    </div>

                                    <p class="info-text-light">
                                        <?php
                                        if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_description_text', true)) {
                                            echo apply_filters('the_content', get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_description_text', true));
                                        }
                                        //the_content();
                                        ?>
                                    </p>
                                    <?php
                                    if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_pre_registration', true) && get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_pre_registration', true) == "on") {
                                        ?>
                                        <?php
                                        $friot_options = get_option('friot_options');
                                        $preregister_form_id_option = $friot_options['tura_eloregisztracio'];
                                        $preregister_finalize_id = apply_filters('wpml_object_id', $preregister_form_id_option);
                                        ?>
                                        <form action="<?php the_permalink($preregister_finalize_id); ?>" method="GET" class="turahajo-form" id="turahajo_eloregisztracio_turavalasztas_form" name="turahajo_eloregisztracio_turavalasztas_form">
                                            <input type="hidden" name="action" value="turahajo_eloregisztracio_turavalasztas" />
                                            <input type="hidden" id="tura_id" name="tura_id" value="<?php echo get_the_ID(); ?>">
                                            <input type="hidden" id="request_type" name="request_type" value="eloregisztracio_">
                                            <a id="turahajo_eloregisztracio" class="btn btn--outline--red"><?php _e('Pre register', 'friot'); ?></a>
                                            <?php
                                            if (get_locale() === 'hu_HU') {
                                                ?>
                                                <p class="charter-contact mt-3">Kérdés estén hívj minket a <a href="tel:+36 70 380 6576">+36 70 380 6576</a>-os telefonszámon.</p>
                                            <?php } else { ?>
                                                <p class="charter-contact mt-3">Call Us if you have any questions <a href="tel:+44 20 3290 2487">+44 20 3290 2487</a></p>
                                                <?php
                                            }
                                            ?>  
                                        </form>
                                        <?php
                                    } else {
                                        $booking_form_id = apply_filters('wpml_object_id', 289);
                                        ?>
                                        <form action="<?php the_permalink($booking_form_id); ?>" method="GET" class="turahajo-form" id="turahajo_foglalas_turavalasztas_form" name="turahajo_foglalas_turavalasztas_form">
                                            <input type="hidden" name="action" value="turahajo_foglalas_turavalasztas" />
                                            <input type="hidden" id="tura_id" name="tura_id" value="<?php echo get_the_ID(); ?>">
                                            <input type="hidden" id="request_type" name="request_type" value="ajanlatkeres_">

                                            <div class="preloadaer p-2 text-center">
                                                <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/loading_2.gif" />
                                            </div>
                                            <div class="available-ships" style="position: relative;">
                                                <div class="dropdown-toggle" data-bs-toggle="dropdown" data-bs-auto-close="true" id="available-ships-display">
                                                    <div class="item" data-value="">
                                                        <p class="date"><?php _e('Select ship', 'friot'); ?></p>
                                                    </div>
                                                </div>
                                                <div class="dropdown-menu" id="available-ships-dropdown">
                                                    <?php
                                                    $args = array(
                                                        'post_type' => 'ships',
                                                        'posts_per_page' => -1,
                                                        'meta_query' => array(
                                                            array(
                                                                'key' => '_ships_adatok_mbox_cabin_charter_id',
                                                                'value' => array(get_the_ID()),
                                                                'compare' => 'IN',
                                                            ),
                                                        )
                                                    );
                                                    $loop = new WP_Query($args);
                                                    if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
                                                            if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_bookable', true) != 'on') {
                                                                ?> 
                                                                <div class="item">
                                                                    <p class="ship full"><?php echo get_the_title(); ?> - <?php _e('Sold', 'friot'); ?></p>
                                                                </div>
                                                            <?php } else { ?>

                                                                <div class="item" data-value="<?php echo get_the_ID(); ?>">
                                                                    <p class="ship"><?php echo get_the_title(); ?></p>
                                                                </div>
                                                                <?php
                                                            }
                                                        endwhile;
                                                    endif;
                                                    wp_reset_query();
                                                    ?>
                                                </div>
                                                <input type="hidden" id="available-ships-input" name="available-ships-input" required="required">
                                            </div>
                                            <div class="available-cabins disabled" style="position: relative;">
                                                <div class="dropdown-toggle" data-bs-toggle="dropdown" data-bs-auto-close="true" id="available-cabins-display">
                                                    <div class="item" data-value="">
                                                        <p class="cabin"><?php _e('Select cabin', 'friot'); ?></p>
                                                    </div>
                                                </div>
                                                <div class="dropdown-menu" id="available-cabins-dropdown">

                                                </div>
                                                <input type="hidden" id="available-cabins-input" name="available-cabins-input" required="required">
                                            </div>

                                            <p class="info-text-bold --small">
                                                <?php
                                                if (has_excerpt()) {
                                                    the_excerpt();
                                                }
                                                ?>
                                            </p>
                                            <div class="mt-4 mb-4">
                                                <div class="d-flex flex-column flex-md-row">
                                                    <a id="turahajo_lefoglalas" class="btn btn--red mb-2 mb-md-0 me-md-2"><?php _e('Book now', 'friot'); ?></a>
                                                    <a id="turahajo_ajanlatkeres" class="btn btn--outline--red"><?php _e('Request an offer', 'friot'); ?></a>
                                                </div>

                                                <?php
                                                if (get_locale() === 'hu_HU') {
                                                    ?>
                                                    <p class="charter-contact mt-3">Kérdés estén hívj minket a <a href="tel:+36 70 380 6576">+36 70 380 6576</a>-os telefonszámon.</p>
                                                <?php } else { ?>
                                                    <p class="charter-contact mt-3">Call Us if you have any questions <a href="tel:+44 20 3290 2487">+44 20 3290 2487</a></p>
                                                    <?php
                                                }
                                                ?>  
                                            </div>
                                            <?php // wp_nonce_field('turahajofoglalasturavalasztas', 'turahajofoglalasturavalasztascheck');
                                            ?>
                                        </form>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section><!-- /tour information -->

                <!-- accordion/tab -->
                <section class="section section--card-block bg-lightblue">
                    <div class="container">
                        <!-- accordion display on small screen -->
                        <div class="d-block d-lg-none">
                            <div class="accordion accordion-flush" id="mainAccordion">
                                <?php
                                $cabin_charter_days_args = array(
                                    'post_type' => 'cabin-charter_days',
                                    'posts_per_page' => -1,
                                    'meta_query' => array(
                                        array(
                                            'key' => '_cabin_charter_id',
                                            'value' => array(get_the_ID()),
                                            'compare' => 'IN',
                                        ),
                                    )
                                );
                                $cabin_charter_days_loop = new WP_Query($cabin_charter_days_args);
                                if ($cabin_charter_days_loop->have_posts()) :
                                    ?>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="accordion-item-heading_1">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-item-collapse_1">
                                                <?php _e('Itiner', 'friot'); ?>
                                            </button>
                                        </h2>
                                        <div id="accordion-item-collapse_1" class="accordion-collapse collapse" data-bs-parent="#mainAccordion">
                                            <div class="accordion-body">
                                                <?php while ($cabin_charter_days_loop->have_posts()) : $cabin_charter_days_loop->the_post(); ?>
                                                    <div class="day-row">
                                                        <div class="day-wrapper w-100">
                                                            <p><?php the_title(); ?></p>
                                                        </div>
                                                        <div class="day-info w-100">
                                                            <div class="image-holder">
                                                                <?php the_post_thumbnail('large'); ?>
                                                            </div>
                                                            <?php if (has_excerpt()) { ?>
                                                                <p><strong><?php the_excerpt(); ?></strong></p>
                                                            <?php } ?>
                                                            <?php the_content(); ?>
                                                        </div>
                                                    </div>
                                                <?php endwhile; ?>

                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                endif;
                                wp_reset_query();
                                if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_general_information_text', true)) {
                                    ?>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="accordion-item-heading_2">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-item-collapse_2">
                                                <?php _e('General Information', 'friot'); ?>
                                            </button>
                                        </h2>
                                        <div id="accordion-item-collapse_2" class="accordion-collapse collapse" data-bs-parent="#mainAccordion">
                                            <div class="accordion-body">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">
                                                        <?php
                                                        $general_information_image = get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_general_information_image', true);
                                                        $attachment_id = attachment_url_to_postid($general_information_image);
                                                        if ($attachment_id != 0) {
                                                            $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'areaimg')
                                                            ?>
                                                            <img src="<?php echo $attachment_image_src[0]; ?>" > 
                                                        <?php } else { ?>
                                                            <img src="<?php echo $general_information_image; ?>" > 
                                                        <?php } ?>                                                        
                                                    </a>
                                                </div>
                                                <div class="accordion accordion-flush accordion-sub" id="smSubAccordion">
                                                    <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_general_information_text', true)); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_take_with_me_text', true)) {
                                    ?>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="accordion-item-heading_3">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-item-collapse_3">
                                                <?php _e('What should I take with me?', 'friot'); ?>
                                            </button>
                                        </h2>
                                        <div id="accordion-item-collapse_3" class="accordion-collapse collapse" data-bs-parent="#mainAccordion">
                                            <div class="accordion-body">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">
                                                        <?php
                                                        $take_with_me_image = get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_take_with_me_image', true);
                                                        $attachment_id = attachment_url_to_postid($take_with_me_image);
                                                        if ($attachment_id != 0) {
                                                            $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'areaimg')
                                                            ?>
                                                            <img src="<?php echo $attachment_image_src[0]; ?>" > 
                                                        <?php } else { ?>
                                                            <img src="<?php echo $take_with_me_image; ?>" > 
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                                <div class="accordion accordion-flush accordion-sub" id="smSubAccordion">
                                                    <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_take_with_me_text', true)); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_whats_included_text', true)) {
                                    ?>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="accordion-item-heading_4">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-item-collapse_4">
                                                <?php _e('What\'s included', 'friot'); ?>
                                            </button>
                                        </h2>
                                        <div id="accordion-item-collapse_4" class="accordion-collapse collapse" data-bs-parent="#mainAccordion">
                                            <div class="accordion-body">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">
                                                        <?php
                                                        $whats_included_image = get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_whats_included_image', true);
                                                        $attachment_id = attachment_url_to_postid($whats_included_image);
                                                        if ($attachment_id != 0) {
                                                            $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'areaimg')
                                                            ?>
                                                            <img src="<?php echo $attachment_image_src[0]; ?>" > 
                                                        <?php } else { ?>
                                                            <img src="<?php echo $whats_included_image; ?>" > 
                                                        <?php } ?>

                                                    </a>
                                                </div>
                                                <div class="accordion accordion-flush accordion-sub" id="smSubAccordion">
                                                    <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_whats_included_text', true)); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }

                                if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_whats_not_included_text', true)) {
                                    ?>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="accordion-item-heading_5">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-item-collapse_5">
                                                <?php _e('What\'s not included', 'friot'); ?>
                                            </button>
                                        </h2>
                                        <div id="accordion-item-collapse_5" class="accordion-collapse collapse" data-bs-parent="#mainAccordion">
                                            <div class="accordion-body">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">
                                                        <?php
                                                        $whats_not_included_image = get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_whats_not_included_image', true);
                                                        $attachment_id = attachment_url_to_postid($whats_not_included_image);
                                                        if ($attachment_id != 0) {
                                                            $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'areaimg')
                                                            ?>
                                                            <img src="<?php echo $attachment_image_src[0]; ?>" > 
                                                        <?php } else { ?>
                                                            <img src="<?php echo $whats_not_included_image; ?>" > 
                                                        <?php } ?>

                                                    </a>
                                                </div>
                                                <div class="accordion accordion-flush accordion-sub" id="smSubAccordion">
                                                    <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_whats_not_included_text', true)); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }


                                $available_ships_args = array(
                                    'post_type' => 'ships',
                                    'posts_per_page' => -1,
                                    'meta_query' => array(
                                        array(
                                            'key' => '_ships_adatok_mbox_cabin_charter_id',
                                            'value' => array(get_the_ID()),
                                            'compare' => 'IN',
                                        ),
                                    )
                                );

                                /*
                                 *  array(
                                  'key' => '_ships_adatok_mbox_bookable',
                                  'value' => 'on',
                                  'compare' => '=',
                                  ),
                                 */
                                $available_ships_loop = new WP_Query($available_ships_args);
                                if ($available_ships_loop->have_posts()) :
                                    ?>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="accordion-item-heading_6">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-item-collapse_6">
                                                <?php _e('Boats', 'friot'); ?>
                                            </button>
                                        </h2>
                                        <div id="accordion-item-collapse_6" class="accordion-collapse collapse" data-bs-parent="#mainAccordion">
                                            <!-- elerhetok hajok lista -->
                                            <?php
                                            $hajos_kepek2 = 0;
                                            while ($available_ships_loop->have_posts()) : $available_ships_loop->the_post();
                                                $hajos_kepek2++;
                                                ?>
                                                <div class="ship-card">
                                                    <div class="row">
                                                        <div class="col-12 col-lg-5 pl-0 pr-0 mb-4">
                                                            <div class="img-wrapper">
                                                                <?php
                                                                $featured_img_url_ship = get_the_post_thumbnail_url(get_the_ID(), 'full');
                                                                $args = array(
                                                                    'post_mime_type' => 'image',
                                                                    'post_parent' => get_the_ID(),
                                                                    'post_status' => null,
                                                                    'post_type' => 'attachment'
                                                                );

                                                                $images = get_children($args);
                                                                $featured_img_slider = get_the_post_thumbnail_url(get_the_ID(), 'charter_slider');
                                                                $featured_img_slider_full = get_the_post_thumbnail_url(get_the_ID(), 'full');
                                                                ?>
                                                                <div thumbsslider="" class="swiper" id="swiper-ship">

                                                                    <div class="swiper-wrapper">
                                                                        <div class="swiper-slide">
                                                                            <a href="<?php echo esc_url($featured_img_slider_full); ?>" class="gallery-item" data-fancybox="<?php echo 'group2' . $hajos_kepek2 ?>">
                                                                                <img src="<?php echo esc_url($featured_img_slider); ?>">
                                                                            </a>
                                                                        </div>
                                                                        <?php
                                                                        foreach ($images as $imageID => $imagePost) {
                                                                            if ($imageID == get_post_thumbnail_id()) {
                                                                                continue;
                                                                            }
                                                                            $src_full = wp_get_attachment_image_src($imageID, 'full', false);
                                                                            $src_large = wp_get_attachment_image_src($imageID, 'charter_slider', false);
                                                                            ?>
                                                                            <div class="swiper-slide">
                                                                                <a href="<?php echo $src_full[0]; ?>" class="gallery-item" data-fancybox="<?php echo 'group2' . $hajos_kepek2 ?>"> <img src="<?php echo $src_large[0]; ?>"></a>
                                                                            </div>
                                                                            <?php
                                                                            $i++;
                                                                        }
                                                                        ?>

                                                                    </div>
                                                                    <div class="swiper-ship-button-next"></div>
                                                                    <div class="swiper-ship-button-prev"></div>
                                                                </div>
                                                                <?php if (false) { ?>
                                                                    <div class="swiper" id="swiper-top">
                                                                        <div class="swiper-wrapper">
                                                                            <div class="swiper-slide">
                                                                                <img src="<?php echo esc_url($featured_img_url_ship); ?>">
                                                                            </div>
                                                                            <?php
                                                                            foreach ($images as $imageID => $imagePost) {
                                                                                $src_large = wp_get_attachment_image_src($imageID, 'charter_slider_thumb', false);
                                                                                ?>
                                                                                <div class="swiper-slide">
                                                                                    <img src="<?php echo $src_large[0]; ?>">
                                                                                </div>
                                                                                <?php
                                                                                $i++;
                                                                            }
                                                                            ?>

                                                                        </div>
                                                                        <div class="swiper-button-next"></div>
                                                                        <div class="swiper-button-prev"></div>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-lg-7 pl-0 pr-0">
                                                            <p class="ship-name"><?php
                                                                the_title();
                                                                if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_bookable', true) != 'on') {
                                                                    echo ' - ';
                                                                    _e('Sold', 'friot');
                                                                }
                                                                ?> </p>
                                                            <?php if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_ship_type', true)) { ?>
                                                                <p class="ship-type">
                                                                    <?php echo get_post_meta(get_the_ID(), '_ships_adatok_mbox_ship_type', true); ?>
                                                                </p>
                                                            <?php } ?>

                                                            <div class="features">
                                                                <?php if (!empty(get_the_content())) { ?>
                                                                    <h3><?php _e('Features', 'friot'); ?></h3>
                                                                    <?php the_content(); ?>
                                                                <?php } ?>
                                                                <ul class="general-info">

                                                                    <?php if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_built_year', true)) { ?>
                                                                        <li>
                                                                            <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/icon-cog.svg">
                                                                            <span><?php _e('Built year', 'friot'); ?> </span> <?php echo get_post_meta(get_the_ID(), '_ships_adatok_mbox_built_year', true); ?>
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                    if (false) {
                                                                        ?>
                                                                        <li>
                                                                            <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/icon-globe.svg">
                                                                            <span>Destination</span> British Virgin Islands
                                                                        </li>
                                                                        <li>
                                                                            <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/icon-location.svg">
                                                                            <span>Base</span> BVI, Scrub Island Marina
                                                                        </li>
                                                                        <li>
                                                                            <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/icon-yacht.svg">
                                                                            <span>Sail category </span> Sailing yacht
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                    if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_built_year', true)) {
                                                                        ?>
                                                                        <li>
                                                                            <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/icon-layout.svg">
                                                                            <span><?php _e('Length', 'friot'); ?> </span> <?php echo get_post_meta(get_the_ID(), '_ships_adatok_mbox_length', true); ?>
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                    if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_width', true)) {
                                                                        ?>
                                                                        <li>
                                                                            <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/icon-layout.svg">
                                                                            <span><?php _e('Width', 'friot'); ?> </span> <?php echo get_post_meta(get_the_ID(), '_ships_adatok_mbox_width', true); ?>
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                    if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_cabins', true)) {
                                                                        ?>
                                                                        <li>
                                                                            <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/icon-layout.svg">
                                                                            <span><?php _e('Cabins', 'friot'); ?> </span> <?php echo get_post_meta(get_the_ID(), '_ships_adatok_mbox_cabins', true); ?>
                                                                        </li>
                                                                        <?php
                                                                    }

                                                                    if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_wc', true)) {
                                                                        ?>
                                                                        <li>
                                                                            <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/icon-layout.svg">
                                                                            <span><?php _e('WC', 'friot'); ?> </span> <?php echo get_post_meta(get_the_ID(), '_ships_adatok_mbox_wc', true); ?>
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                    ?>

                                                                </ul>
                                                            </div>
                                                            <div class="equipment">
                                                                <?php
                                                                if ((
                                                                        !empty(get_post_meta(get_the_ID(), '_ships_adatok_mbox_ac', true)) ||
                                                                        !empty(get_post_meta(get_the_ID(), '_ships_adatok_mbox_generator', true)) ||
                                                                        !empty(get_post_meta(get_the_ID(), '_ships_adatok_mbox_solar_panel', true)) ||
                                                                        !empty(get_post_meta(get_the_ID(), '_ships_adatok_mbox_freshwater_maker', true)) ||
                                                                        !empty(get_post_meta(get_the_ID(), '_ships_adatok_mbox_grill', true))
                                                                        )) {
                                                                    ?>
                                                                    <h3><?php _e('Equipment', 'friot'); ?></h3>
                                                                    <ul>
                                                                        <?php
                                                                        if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_ac', true)) {
                                                                            ?>
                                                                            <li>

                                                                                <span><?php _e('AC', 'friot'); ?> </span> &check;
                                                                            </li>
                                                                            <?php
                                                                        }
                                                                        if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_generator', true)) {
                                                                            ?>
                                                                            <li>

                                                                                <span><?php _e('Generator', 'friot'); ?> </span> &check;
                                                                            </li>
                                                                            <?php
                                                                        }
                                                                        if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_solar_panel', true)) {
                                                                            ?>
                                                                            <li>

                                                                                <span><?php _e('Solar panel', 'friot'); ?> </span> &check;
                                                                            </li>
                                                                            <?php
                                                                        }
                                                                        if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_freshwater_maker', true)) {
                                                                            ?>
                                                                            <li>

                                                                                <span><?php _e('Freshwater maker', 'friot'); ?> </span> &check;
                                                                            </li>
                                                                            <?php
                                                                        }
                                                                        if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_grill', true)) {
                                                                            ?>
                                                                            <li>

                                                                                <span><?php _e('Grill', 'friot'); ?> </span> &check;
                                                                            </li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endwhile;
                                            ?>
                                            <!-- /elerhetok hajok lista -->
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="accordion-item-heading_7">
                                            <a href="<?php echo get_post_type_archive_link('turabeszamolok'); ?>"class="accordion-button collapsed"  type="button"  >
                                                <?php _e('Tour reports', 'friot'); ?>
                                            </a>
                                        </h2>
                                    </div>
                                    <?php
                                endif;
                                wp_reset_query();
                                ?>
                            </div>
                        </div>
                        <!-- tab view on large -->
                        <div class="d-none d-lg-block">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <?php
                                    if ($cabin_charter_days_loop->have_posts()) :
                                        ?>
                                        <button class="nav-link active" id="tab-1" data-bs-toggle="tab" data-bs-target="#tab-1-toggle" type="button" role="tab"><?php _e('Itiner', 'friot'); ?></button>
                                        <?php
                                    endif;
                                    if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_general_information_text', true)) {
                                        ?>
                                        <button class="nav-link" id="tab-2" data-bs-toggle="tab" data-bs-target="#tab-2-toggle" type="button" role="tab"><?php _e('General Information', 'friot'); ?></button>
                                        <?php
                                    }
                                    if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_take_with_me_text', true)) {
                                        ?>
                                        <button class="nav-link" id="tab-3" data-bs-toggle="tab" data-bs-target="#tab-3-toggle" type="button" role="tab"><?php _e('What should I take with me', 'friot'); ?></button>
                                        <?php
                                    }
                                    if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_whats_included_text', true)) {
                                        ?>
                                        <button class="nav-link" id="tab-4" data-bs-toggle="tab" data-bs-target="#tab-4-toggle" type="button" role="tab"><?php _e('What\'s included', 'friot'); ?></button>
                                        <?php
                                    }
                                    if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_whats_not_included_text', true)) {
                                        ?>
                                        <button class="nav-link" id="tab-5" data-bs-toggle="tab" data-bs-target="#tab-5-toggle" type="button" role="tab"><?php _e('What\'s not included', 'friot'); ?></button>
                                        <?php
                                    }
                                    if ($available_ships_loop->have_posts()) :
                                        ?>
                                        <button class="nav-link" id="tab-6" data-bs-toggle="tab" data-bs-target="#tab-6-toggle" type="button" role="tab"><?php _e('Boats', 'friot'); ?></button>
                                        <?php
                                    endif
                                    ?>
                                    <a href="<?php echo get_post_type_archive_link('turabeszamolok'); ?>" class="nav-link"  type="button" role="tab"><?php _e('Tour reports', 'friot'); ?></a>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <?php if ($cabin_charter_days_loop->have_posts()) : ?>
                                    <div class="tab-pane fade show active" id="tab-1-toggle" role="tabpanel">

                                        <?php
                                        $postCounter = 0;
                                        while ($cabin_charter_days_loop->have_posts()) : $cabin_charter_days_loop->the_post();
                                            ?>
                                            <div class="row tab-row day-row  flex-wrap">
                                                <?php /*
                                                  <div class="day-wrapper">
                                                  <p class="cbc-title"><?php the_title(); ?></p>
                                                  <img class="cbc-arrow-img" src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/arrw.svg" />
                                                  </div>
                                                  <div class="day-info">
                                                  <div class="image-holder">
                                                  <?php the_post_thumbnail('large'); ?>
                                                  </div>
                                                  <?php if (has_excerpt()) { ?>
                                                  <p><strong><?php the_excerpt(); ?></strong></p>
                                                  <?php } ?>
                                                  <?php the_content(); ?>
                                                  </div> */
                                                ?>
                                                <div class="col-12 p-0">
                                                    <div class="row tab-row day-row mb-0">
                                                        <div class="day-wrapper">
                                                            <?php
                                                            if (get_locale() === 'hu_HU' && $postCounter === 0) {
                                                                ?>
                                                                <p class="cbc-title">Útiterv</p>
                                                                <?php
                                                            } else if (get_locale() === 'hu_HU') {
                                                                ?>
                                                                <p class="cbc-title"><span style="color:#072B3D"><?php echo $postCounter; ?>.</span> nap</p>
                                                                <?php
                                                            } else if (get_locale() !== 'hu_HU' && $postCounter === 0) {
                                                                ?>
                                                                <p class="cbc-title">Itiner</p>
                                                            <?php } else {
                                                                ?>
                                                                <p class="cbc-title">Day <span style="color:#072B3D"><?php echo $postCounter; ?></span></p>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="day-info">
                                                            <div class="image-holder mb-0">
                                                                <?php the_post_thumbnail('large'); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 p-0">
                                                    <div class="row tab-row day-row cbc-content mb-0">
                                                        <div class="day-wrapper">
                                                            <?php
                                                            if ($postCounter === 0) {
                                                                ?>
                                                                <img class="cbc-arrow-img2" src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/start-arrow3.svg" style="width:20px;object-fit:cover;object-position:top;" />
                                                                <?php
                                                            } else if (count($cabin_charter_days_loop->posts) === $postCounter + 1) {
                                                                ?>
                                                                <img class="cbc-arrow-img2" src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/end-arrow3.svg" style="width:20px;object-fit:cover;object-position:bottom;" />
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <img class="cbc-arrow-img2" src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/large-arrow3.svg" style="width:20px;object-fit:cover;object-position:bottom;" />
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="day-info cbc-info" style="padding-top:30px;padding-bottom: 30px;">
                                                            <p><strong><?php the_title(); ?></strong></p>
                                                            <?php if (has_excerpt()) { ?>
                                                                <p><strong><?php the_excerpt(); ?></strong></p>
                                                            <?php } ?>
                                                            <?php the_content(); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            $postCounter++;
                                        endwhile;
                                        ?>

                                    </div>
                                    <?php
                                endif;
                                wp_reset_query();
                                if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_general_information_text', true)) {
                                    ?>
                                    <div class="tab-pane fade show" id="tab-2-toggle" role="tabpanel">
                                        <div class="row tab-row">
                                            <div class="display-card-wrapper">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">
                                                        <?php
                                                        $general_information_image = get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_general_information_image', true);
                                                        $attachment_id = attachment_url_to_postid($general_information_image);
                                                        if ($attachment_id != 0) {
                                                            $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'areaimg')
                                                            ?>
                                                            <img src="<?php echo $attachment_image_src[0]; ?>" > 
                                                        <?php } else { ?>
                                                            <img src="<?php echo $general_information_image; ?>" > 
                                                        <?php } ?>

                                                    </a>
                                                </div>
                                            </div>
                                            <div class="text-block">
                                                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_general_information_text', true)); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_take_with_me_text', true)) {
                                    ?>
                                    <div class="tab-pane fade show" id="tab-3-toggle" role="tabpanel">
                                        <div class="row tab-row">
                                            <div class="display-card-wrapper">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">

                                                        <?php
                                                        $take_with_me_image = get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_take_with_me_image', true);
                                                        $attachment_id = attachment_url_to_postid($take_with_me_image);
                                                        if ($attachment_id != 0) {
                                                            $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'areaimg')
                                                            ?>
                                                            <img src="<?php echo $attachment_image_src[0]; ?>" > 
                                                        <?php } else { ?>
                                                            <img src="<?php echo $take_with_me_image; ?>" > 
                                                        <?php } ?>

                                                    </a>
                                                </div>
                                            </div>
                                            <div class="text-block">
                                                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_take_with_me_text', true)); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_whats_included_text', true)) {
                                    ?>
                                    <div class="tab-pane fade show" id="tab-4-toggle" role="tabpanel">
                                        <div class="row tab-row">
                                            <div class="display-card-wrapper">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">
                                                        <?php
                                                        $whats_included_image = get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_whats_included_image', true);
                                                        $attachment_id = attachment_url_to_postid($whats_included_image);
                                                        if ($attachment_id != 0) {
                                                            $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'areaimg')
                                                            ?>
                                                            <img src="<?php echo $attachment_image_src[0]; ?>" > 
                                                        <?php } else { ?>
                                                            <img src="<?php echo $whats_included_image; ?>" > 
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="text-block">
                                                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_whats_included_text', true)); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_whats_not_included_text', true)) {
                                    ?>
                                    <div class="tab-pane fade show" id="tab-5-toggle" role="tabpanel">
                                        <div class="row tab-row">
                                            <div class="display-card-wrapper">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">
                                                        <?php
                                                        $whats_not_included_image = get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_whats_not_included_image', true);
                                                        $attachment_id = attachment_url_to_postid($whats_not_included_image);
                                                        if ($attachment_id != 0) {
                                                            $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'areaimg')
                                                            ?>
                                                            <img src="<?php echo $attachment_image_src[0]; ?>" > 
                                                        <?php } else { ?>
                                                            <img src="<?php echo $whats_not_included_image; ?>" > 
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="text-block">
                                                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_whats_not_included_text', true)); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                if ($available_ships_loop->have_posts()) :
                                    ?>
                                    <div class="tab-pane fade show" id="tab-6-toggle" role="tabpanel">

                                        <!-- elerhető hajók lista -->
                                        <?php
                                        $hajos_kepek = 0;
                                        while ($available_ships_loop->have_posts()) : $available_ships_loop->the_post();
                                            $hajos_kepek++;
                                            ?>
                                            <div class="ship-card">
                                                <div class="row">
                                                    <div class="col-12 col-lg-5 pl-0">
                                                        <div class="img-wrapper" id="slider_<?php echo get_the_ID(); ?>">
                                                            <?php
                                                            $featured_img_url_ship = get_the_post_thumbnail_url(get_the_ID(), 'full');
                                                            $args = array(
                                                                'post_mime_type' => 'image',
                                                                'post_parent' => get_the_ID(),
                                                                'post_status' => null,
                                                                'post_type' => 'attachment'
                                                            );

                                                            $images = get_children($args);
                                                            $featured_img_slider = get_the_post_thumbnail_url(get_the_ID(), 'charter_slider');
                                                            $featured_img_slider_full = get_the_post_thumbnail_url(get_the_ID(), 'full');
                                                            ?>
                                                            <div thumbsslider="" class="swiper" id="swiper-ship">

                                                                <div class="swiper-wrapper">
                                                                    <div class="swiper-slide">
                                                                        <a href="<?php echo esc_url($featured_img_slider_full); ?>" class="gallery-item" data-fancybox="<?php echo 'group3' . $hajos_kepek ?>">
                                                                            <img src="<?php echo esc_url($featured_img_slider); ?>">
                                                                        </a>
                                                                    </div>
                                                                    <?php
                                                                    foreach ($images as $imageID => $imagePost) {
                                                                        if ($imageID == get_post_thumbnail_id()) {
                                                                            continue;
                                                                        }
                                                                        $src_full = wp_get_attachment_image_src($imageID, 'full', false);
                                                                        $src_large = wp_get_attachment_image_src($imageID, 'charter_slider', false);
                                                                        ?>
                                                                        <div class="swiper-slide">
                                                                            <a href="<?php echo $src_full[0]; ?>" class="gallery-item" data-fancybox="<?php echo 'group3' . $hajos_kepek ?>"> <img src="<?php echo $src_large[0]; ?>"></a>
                                                                        </div>
                                                                        <?php
                                                                        $i++;
                                                                    }
                                                                    ?>

                                                                </div>
                                                                <div class="swiper-ship-button-next"></div>
                                                                <div class="swiper-ship-button-prev"></div>
                                                            </div>
                                                            <?php if (false) { ?>
                                                                <div class="swiper" id="swiper-top">
                                                                    <div class="swiper-wrapper">
                                                                        <div class="swiper-slide">
                                                                            <img src="<?php echo esc_url($featured_img_url_ship); ?>">
                                                                        </div>
                                                                        <?php
                                                                        foreach ($images as $imageID => $imagePost) {
                                                                            $src_large = wp_get_attachment_image_src($imageID, 'charter_slider_thumb', false);
                                                                            ?>
                                                                            <div class="swiper-slide">
                                                                                <img src="<?php echo $src_large[0]; ?>">
                                                                            </div>
                                                                            <?php
                                                                            $i++;
                                                                        }
                                                                        ?>

                                                                    </div>
                                                                    <div class="swiper-button-next"></div>
                                                                    <div class="swiper-button-prev"></div>
                                                                </div>
                                                            <?php } ?>

                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-lg-7 pr-0">

                                                        <p class="ship-name">                                                            
                                                            <?php
                                                            the_title();
                                                            if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_bookable', true) != 'on') {
                                                                echo ' - ';
                                                                _e('Sold', 'friot');
                                                            }
                                                            ?>
                                                        </p>
                                                        <?php if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_ship_type', true)) { ?>
                                                            <p class="ship-type">
                                                                <?php echo get_post_meta(get_the_ID(), '_ships_adatok_mbox_ship_type', true); ?>
                                                            </p>
                                                        <?php } ?>

                                                        <div class="features">
                                                            <div class="row">
                                                                <div class="col-lg-6 pl-0">
                                                                    <h4 class="mb-3"><?php _e('Details', 'friot'); ?></h4>
                                                                    <ul class="general-info">
                                                                        <?php if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_built_year', true)) { ?>
                                                                            <li>
                                                                                <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/icon-cog.svg">
                                                                                <span><?php _e('Built year', 'friot'); ?> </span> <?php echo get_post_meta(get_the_ID(), '_ships_adatok_mbox_built_year', true); ?>
                                                                            </li>
                                                                            <?php
                                                                        }
                                                                        if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_built_year', true)) {
                                                                            ?>
                                                                            <li>
                                                                                <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/icon-layout.svg">
                                                                                <span><?php _e('Length', 'friot'); ?> </span> <?php echo get_post_meta(get_the_ID(), '_ships_adatok_mbox_length', true); ?>
                                                                            </li>
                                                                            <?php
                                                                        }
                                                                        if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_width', true)) {
                                                                            ?>
                                                                            <li>
                                                                                <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/icon-layout.svg">
                                                                                <span><?php _e('Width', 'friot'); ?> </span> <?php echo get_post_meta(get_the_ID(), '_ships_adatok_mbox_width', true); ?>
                                                                            </li>
                                                                            <?php
                                                                        }
                                                                        if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_draft', true)) {
                                                                            ?>
                                                                            <li>
                                                                                <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/icon-layout.svg">
                                                                                <span><?php _e('Draft', 'friot'); ?> </span> <?php echo get_post_meta(get_the_ID(), '_ships_adatok_mbox_draft', true); ?>
                                                                            </li>
                                                                            <?php
                                                                        }
                                                                        if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_cabins', true)) {
                                                                            ?>
                                                                            <li>
                                                                                <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/icon-layout.svg">
                                                                                <span><?php _e('Cabins', 'friot'); ?> </span> <?php echo get_post_meta(get_the_ID(), '_ships_adatok_mbox_cabins', true); ?>
                                                                            </li>
                                                                            <?php
                                                                        }
                                                                        if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_beds', true)) {
                                                                            ?>
                                                                            <li>
                                                                                <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/icon-layout.svg">
                                                                                <span><?php _e('Beds', 'friot'); ?> </span> <?php echo get_post_meta(get_the_ID(), '_ships_adatok_mbox_beds', true); ?>
                                                                            </li>
                                                                            <?php
                                                                        }
                                                                        if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_wc', true)) {
                                                                            ?>
                                                                            <li>
                                                                                <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/icon-layout.svg">
                                                                                <span><?php _e('WC', 'friot'); ?> </span> <?php echo get_post_meta(get_the_ID(), '_ships_adatok_mbox_wc', true); ?>
                                                                            </li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                </div>
                                                                <div class="equipment col-lg-6 pr-0">
                                                                    <?php
                                                                    if ((
                                                                            !empty(get_post_meta(get_the_ID(), '_ships_adatok_mbox_ac', true)) ||
                                                                            !empty(get_post_meta(get_the_ID(), '_ships_adatok_mbox_generator', true)) ||
                                                                            !empty(get_post_meta(get_the_ID(), '_ships_adatok_mbox_solar_panel', true)) ||
                                                                            !empty(get_post_meta(get_the_ID(), '_ships_adatok_mbox_freshwater_maker', true)) ||
                                                                            !empty(get_post_meta(get_the_ID(), '_ships_adatok_mbox_grill', true))
                                                                            )) {
                                                                        ?>
                                                                        <h4 class="mb-3"><?php _e('Equipment', 'friot'); ?></h4>
                                                                        <ul>
                                                                            <?php
                                                                            if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_ac', true)) {
                                                                                ?>
                                                                                <li>

                                                                                    <span><?php _e('AC', 'friot'); ?> </span> &check;
                                                                                </li>
                                                                                <?php
                                                                            }
                                                                            if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_generator', true)) {
                                                                                ?>
                                                                                <li>

                                                                                    <span><?php _e('Generator', 'friot'); ?> </span> &check;
                                                                                </li>
                                                                                <?php
                                                                            }
                                                                            if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_solar_panel', true)) {
                                                                                ?>
                                                                                <li>

                                                                                    <span><?php _e('Solar panel', 'friot'); ?> </span> &check;
                                                                                </li>
                                                                                <?php
                                                                            }
                                                                            if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_freshwater_maker', true)) {
                                                                                ?>
                                                                                <li>

                                                                                    <span><?php _e('Freshwater maker', 'friot'); ?> </span> &check;
                                                                                </li>
                                                                                <?php
                                                                            }
                                                                            if (get_post_meta(get_the_ID(), '_ships_adatok_mbox_grill', true)) {
                                                                                ?>
                                                                                <li>

                                                                                    <span><?php _e('Grill', 'friot'); ?> </span> &check;
                                                                                </li>
                                                                            <?php } ?>
                                                                        </ul>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <?php if (!empty(get_the_content())) { ?>
                                                                <h3><?php _e('Features', 'friot'); ?></h3>
                                                                <?php the_content(); ?>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        endwhile;
                                        wp_reset_query();
                                        ?>

                                        <!-- /elerhető hajók lista -->
                                    </div>
                                    <?php
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>
                </section>
                <!--accordion/tab-->


                <!--FROM HERE GOES TO WP FOOTER-->
                <!--section Boat rental-->
                <section class="section section--card-block bg-parallax">
                    <div class="container">
                        <?php
                        $post_id = apply_filters('wpml_object_id', 80);
                        //$post_id = 80;
                        ?>
                        <div class="row section-title-row">
                            <div class="col-12 col-md-6">
                                <h2 class="section-title"><?php echo get_the_title($post_id); ?></h2>
                            </div>
                            <div class="col-12 col-md-6">
                                <p class="section-subtitle"><?php echo apply_filters('the_content', get_the_content('', '', $post_id)); ?></p>
                            </div>
                        </div>
                        <div class="carousel-wrapper mt-4">
                            <div class="owl-carousel card-layout-4">
                                <?php
                                $current_id = get_the_ID();
                                $args = array('post_type' => 'cabin-charter', 'posts_per_page' => -1, 'post__not_in' => array($current_id));
                                $loop = new WP_Query($args);
                                if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
                                        ?>
                                        <div class="item">
                                            <div class="display-card">
                                                <a href="<?php the_permalink(); ?>">
                                                    <div class="badge-container">
                                                        <div class="badge">
                                                            <span><?php the_title(); ?></span>
                                                        </div>
                                                    </div>
                                                    <?php the_post_thumbnail('areaimg'); ?>
                                                    <div class="card-layer">
                                                        <p class="view"><?php _e("view", 'friot'); ?></p>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <?php
                                    endwhile;
                                endif;
                                ?>


                            </div>
                            <div class="button-wrapper text-center mt-3">
                                <a href="<?php echo get_post_type_archive_link('cabin-charter'); ?>" class="btn btn--outline--blue"><?php _e("All", 'friot'); ?></a>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/Boat rental-->

                <?php
                $tesimonial_args = array('post_type' => 'testimonial', 'posts_per_page' => -1);
                $tesimonial_loop = new WP_Query($tesimonial_args);
                if ($tesimonial_loop->have_posts()) :
                    ?>   
                    <!-- section Testimonial -->
                    <section class="section section--carousel bg-parallax testimonial-section">
                        <div class="container">
                            <div class="row section-title-row">
                                <div class="col-12 col-md-6">
                                    <h2 class="section-title"><?php _e('Testimonials', 'friot'); ?></h2>
                                </div>
                                <div class="col-12 col-md-6">
                                    <p class="section-subtitle"><?php //_e('Our passengers said about us', 'friot');     ?></p>
                                </div>
                            </div>
                            <div class="carousel-wrapper">
                                <div id="owltesimonial" class="owl-carousel owl-3-col">
                                    <?php while ($tesimonial_loop->have_posts()) : $tesimonial_loop->the_post(); ?>
                                        <div class="item" id="konzultacio_slide">

                                            <div class="row">


                                                <div class="text-wrapper">                                            
                                                    <div class="description"><?php the_content(); ?></div>
                                                    <p class="title"><?php the_title(); ?></p>
                                                </div>

                                            </div>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- /Testimonial -->
                    <?php
                endif;
                wp_reset_query();
                if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_previous_tours_gallery', true)) {
                    ?>
                    <!-- section Gallery -->
                    <section id="tour_gallery">
                        <div class="container">
                            <div class="row section-title-row">
                                <div class="col-12 col-md-12">
                                    <h2 class="section-title"><?php _e('Néhány kép korábbi túráinkról', 'friot'); ?></h2>
                                </div>
                             
                            </div>
                            <div class="gallery-wrapper">
                                <?php
                                    $gallery_shortcode = '[gallery size="large" id="' . get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_previous_tours_gallery', true) . '';
                                    echo apply_filters( 'the_content', $gallery_shortcode);
                                ?> 
                            </div>
                        </div>
                    </section>
                <?php }
                ?>
            </div>
            <?php
        endwhile;
    endif;
    rewind_posts();
    ?>

</main><!-- NOTE: Page content wrapper ENDS here -->

<?php get_footer(); ?>