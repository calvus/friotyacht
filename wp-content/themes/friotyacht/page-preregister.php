<?php
//print_r($_GET);
//die();
/*
 * Template name: Charter Pregegistration finalaze
 */

if (isset($_GET['action']) && ($_GET['action'] == "turahajo_eloregisztracio_turavalasztas")) {
    get_header();
    $tura_id = intval($_GET['tura_id']);
    $featured_img_url = get_the_post_thumbnail_url($tura_id, 'full');
    ?>

    <!-- NOTE: Page content wrapper STARTS here -->
    <main class="page-content-wrapper" role="main">
        <!-- NOTE: Page content -->
        <div class="container-fluid container--home">
            <section class="section section--header section--page-header" style="background-image: url('<?php echo $featured_img_url; ?>');">
                <div class="container">
                    <div class="row page-breadcrumb-row ml-0 mr-0">
                        <nav style="--bs-breadcrumb-divider: '/';" class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <?php
                                if (function_exists('yoast_breadcrumb')) {
                                    yoast_breadcrumb('<li class="breadcrumb-item">', '</li>');
                                }
                                ?>
                            </ol>
                        </nav>
                        <div class="button-wrapper col-12 col-md-4 p-0">

                        </div>
                    </div>
                    <div class="row m-0">
                        <div class="text-block w-100">
                            <h1 class="banner-title"><?php the_title(); ?></h1>
                        </div>
                    </div>
                </div>
            </section>

            <!-- section counter -->
            <section class="section section--contact-form bg-parallax">
                <div class="container">
                    <p class="text-center mb-4"><strong><?php _e('Pre register form', 'friot'); ?></strong></p>
                    <?php
                    /* if (!isset($_GET['turahajofoglalasturavalasztascheck']) || !wp_verify_nonce($_GET['turahajofoglalasturavalasztascheck'], 'turahajofoglalasturavalasztas')) {
                      $redirect_to = get_bloginfo('wpurl');
                      wp_safe_redirect($redirect_to);
                      die();
                      } else { */

                        if (!empty($_SESSION['errormessage'])) {
                            echo '<label class="alertbox alert">' . $_SESSION['errormessage'] . '</label>';
                        }
                        /*                         * local -- 2156 */
                        /*                         * calvus.xyz -- 2155 */
                        /*                         * éles -- 2180 */
                        $friot_options = get_option('friot_options');
                        $preregistration_thankyou_id_option = $friot_options['tura_eloregisztracio_koszono'];
                        $preregistration_thankyou_id = apply_filters('wpml_object_id', $preregistration_thankyou_id_option);
                        ?>
                        <form action="<?php the_permalink($preregistration_thankyou_id); ?>" method="POST"  class="turahajo-form" id="turahajo_eloregisztracio_form" name="turahajo_eloregisztracio_form">                                        
                            <?php $token = getToken(); ?>
                            <input type="hidden" name="googletoken" id="googletoken">
                            <input type="hidden" name="token" value="<?php echo $token; ?>"/>
                            <input type="hidden" name="action" value="turahajo_eloregisztracio" />
                            <input type="hidden" id="tura_id" name="tura_id" value="<?php echo $tura_id; ?>">                        
                            <div class="row">
                                <div class="form-group col-12 col-md-9">
                                    <label   class="form-label"><?php _e('Selected tour', 'friot'); ?></label>                                      
                                    <div><strong><?php echo get_the_title($tura_id); ?></strong></div>
                                    <div> <span class="text-blue"><?php echo mysql2date('Y. m.', get_post_meta($tura_id, '_cabin_charter_adatok_mbox_date_from', true), 0, '', ' '); ?></span> </div>
                                    <div><?php echo get_the_title($selected_ship); ?></div>
                                    <div class="text-capitalize"><?php echo $selected_cabin; ?></div>    


                                </div>
                                <div class="form-group col-12 col-md-3">

                                    <?php echo get_the_post_thumbnail($tura_id, 'thumbnail'); ?>

                                </div>

                                <div class="form-group col-12 col-md-6">
                                    <label for="firstname" class="form-label"><?php _e('Name', 'friot'); ?> *</label>
                                    <input type="text" class="form-control" id="firstname" name="firstname" placeholder="<?php _e('First name', 'friot'); ?>" value="<?php echo $_SESSION['bookingdata']['firstname']; ?>" required="required">
                                </div>
                                <div class="form-group col-12 col-md-6">
                                    <label for="lastname" class="form-label">&nbsp;</label>
                                    <input type="text" class="form-control" id="lastname" name="lastname" placeholder="<?php _e('Last name', 'friot'); ?>" value="<?php echo $_SESSION['bookingdata']['lastname']; ?>" required="required">
                                </div>
                                <div class="form-group col-12 ">
                                    <label for="email_address" class="form-label"><?php _e('E-mail', 'friot'); ?> *</label>
                                    <input type="email" class="form-control" id="email_address" name="email_address" placeholder="<?php _e('E-mail', 'friot'); ?>" value="<?php echo $_SESSION['bookingdata']['email_address']; ?>" required="required">
                                </div>
                                <div class="form-group col-12 ">
                                    <label for="phone" class="form-label"><?php _e('Phone', 'friot'); ?> *</label>
                                    <input type="text" class="form-control" id="phone" name="phone" placeholder="<?php _e('Phone', 'friot'); ?>" value="<?php echo $_SESSION['bookingdata']['phone']; ?>" required="required">
                                </div>                                                              
                                <div class="form-group col-12 ">
                                    <label for="other_information" class="form-label"><?php _e('Other information', 'friot'); ?></label>
                                    <textarea class="form-control" id="other_information" name="other_information" rows="5"><?php echo $_SESSION['bookingdata']['other_information']; ?></textarea>
                                </div>

                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="turahajo_aszf" name="turahajo_aszf">
                                    <label class="form-check-label" for="turahajo_aszf">
                                        <?php $terms_and_conditions_id = apply_filters('wpml_object_id', 72); ?>
                                        <?php _e("I accept the", "friot"); ?> <a href="<?php echo get_permalink($terms_and_conditions_id); ?>"  target="_blank"><?php echo get_the_title($terms_and_conditions_id); ?></a><?php _e("et", "friot"); ?>
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="privacy_chk" name="privacy_chk">
                                    <label class="form-check-label" for="privacy_chk">
                                        <?php $privacy_policy_id = apply_filters('wpml_object_id', 3); ?>
                                        <?php _e("I accept the", "friot"); ?> <a href="<?php echo get_permalink($privacy_policy_id); ?>"  target="_blank"><?php echo get_the_title($privacy_policy_id); ?></a><?php _e("t", "friot"); ?>
                                    </label>
                                </div>
                                <!-- <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="booking_terms" name="booking_terms">
                                    <label class="form-check-label" for="booking_terms">
                                <?php /* $booking_terms_id = apply_filters('wpml_object_id', 1524); ?>
                                  <?php _e("I accept the", "friot"); ?> <a href="<?php echo get_permalink($booking_terms_id); ?>"  target="_blank"><?php echo get_the_title($booking_terms_id); ?></a><?php _e("et", "friot"); */ ?>
                                    </label>
                                </div> -->
                            </div>
                            <div class="button-wrapper mt-4 text-center">
                                <a id="turahajo_eloregisztracio_veglegesites" class="btn btn--red"><?php _e('Pre register', 'friot'); ?></a>
                            </div>
                            <?php wp_nonce_field('turahajoeloregisztracio', 'turahajoeloregisztraciocheck'); ?>
                        </form>
                        <?php
                   
                    // }
                } else {
                    $redirect_to = get_bloginfo('wpurl');
                    wp_safe_redirect($redirect_to);
                    die();
                }
                ?>
            </div>
        </section>
        <!-- /counter -->

    </div>
</main><!-- NOTE: Page content wrapper ENDS here -->
<?php get_footer(); ?>