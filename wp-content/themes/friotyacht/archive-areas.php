<?php
/*
 * Template name: Destinations list
 */

get_header();
?>

<!-- NOTE: Page content wrapper STARTS here -->
<main class="page-content-wrapper" role="main">
    <!-- NOTE: Page content -->
    <div class="container-fluid container--home">
        <section class="section section--header">
            <div class="container">
                <div class="row m-0">
                    <div class="text-block">
                        <h1 class="banner-title"><?php _e("The <span>happy</span> flow", "friot"); ?></h1>
                    </div>
                    <div class="image-block">
                        <div class="image-wrapper">
                            <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/home-hero.png" />
                        </div>
                    </div>
                </div>
            </div>
        </section>





        <!-- cabin charter -->
        <section class="section section--card-block extra-padding bg-lightblue" id="cabin_charter">
            <div class="container">
                <?php
                $post_id = apply_filters('wpml_object_id', 83);
                //$post_id = 83;
                ?>
                <div class="row section-title-row">
                    <div class="col-12 col-md-6">
                        <h2 class="section-title"><?php echo get_the_title($post_id); ?></h2>
                    </div>
                    <div class="col-12 col-md-6">
                        <span class="section-subtitle"> <?php echo apply_filters('the_content', get_the_content('', '', $post_id)); ?></span>
                    </div>
                </div>

                <!-- carousel display on small screen -->
                <div class="d-block d-lg-none mt-4">
                    <div class="carousel-wrapper">
                        <div class="owl-carousel card-layout-3">
                            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                    <div class="item">
                                        <div class="display-card">
                                            <a href="<?php the_permalink(); ?>">
                                                <div class="badge-container">
                                                    <div class="badge">
                                                        <span><?php the_title(); ?></span>
                                                    </div>
                                                </div>
                                                <?php the_post_thumbnail('areaimg'); ?>
                                                <div class="card-layer">
                                                    <p class="view"><?php _e("view", "friot"); ?></p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <?php
                                endwhile;
                            endif;
                            rewind_posts();
                            ?>
                        </div>

                    </div>
                </div>
                <!-- list view on large -->
                <div class="d-none d-lg-block mt-4">
                    <div class="row three-card-layout">
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                <div class = "display-card">
                                    <a href = "<?php the_permalink(); ?>">
                                        <div class="badge-container">
                                            <div class="badge">
                                                <span><?php the_title(); ?></span>
                                            </div>
                                        </div>
                                        <?php the_post_thumbnail('areaimg'); ?>
                                        <div class = "card-layer">
                                            <p class = "view">view</p>
                                        </div>
                                    </a>
                                </div>
                                <?php
                            endwhile;
                        endif;
                        rewind_posts();
                        ?>
                    </div>

                </div>

            </div>
        </section>
        <!-- cabin charter -->

    </div>
</main><!-- NOTE: Page content wrapper ENDS here -->
<?php get_footer(); ?>