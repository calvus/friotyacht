<!-- filter section -->
<?php
/*$bookingSiteLink = get_option('booking_site_link');
if (ICL_LANGUAGE_CODE == 'en') {
    $bookingSiteLink = get_option('booking_site_link_'.ICL_LANGUAGE_CODE);
}
$destinations = json_decode(file_get_contents(get_option('booking_site_link') . 'yacht/ajax-destinations?lang=' . ICL_LANGUAGE_CODE));
$boat_types = json_decode(file_get_contents(get_option('booking_site_link') . 'yacht/ajax-yacht-categories?lang=' . ICL_LANGUAGE_CODE));*/
?>
<section class="section section--filter">
    <div class="container search-iframe-container" style="background:transparent;">
        <?php if(false){?>
        <form action="<?php echo $bookingSiteLink ?>yachts/search" method="GET">
            <input type="hidden" name="lang" value="<?php echo ICL_LANGUAGE_CODE; ?>" />
            <?php $today = date('Y-m-d'); ?>
            <div class="row m-0">
                <div class="form-group col-12 col-md-6 col-xl-3">
                    <label for="place_of_dep" class="form-label"><?php _e("Place of departure", 'friot'); ?></label>

                    <select id="place_of_dep" name="place_of_dep" class="form-select" required="required" >
                        <option value="" ><?php _e("Choose...", 'friot'); ?></option>
                        <?php foreach ($destinations as $destination) { ?>
                            <option value="<?php echo $destination; ?>"><?php echo $destination; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group col-12 col-md-3 col-xl-2">
                    <label for="date_start" class="form-label"><?php _e("Starting date", 'friot'); ?></label>
                    <input type="text" class="form-control" data-date-format="mm/dd/yy" id="date_start" class="datepicker" required="required"  name="date_start" value="">
                </div>
                <div class="form-group col-12 col-md-3 col-xl-2">
                    <label for="date_end" class="form-label"><?php _e("Ending date", 'friot'); ?></label>
                    <input type="text" class="form-control" data-date-format="mm/dd/yy"  id="date_end" class="datepicker"  required="required"  name="date_end" value="">
                </div>
                <div class="form-group col-12 col-md-6 col-xl-3">
                    <label for="boat_type" class="form-label"><?php _e("Boat type", 'friot'); ?></label>
                    <select id="boat_type" name="boat_type" class="form-select" required="required" >
                        <option value="" ><?php _e("Choose...", 'friot'); ?></option>
                        <?php foreach ($boat_types as $boat_type) { ?>
                            <option value="<?php echo $boat_type; ?>"><?php echo $boat_type; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-12 col-md-6 col-xl-2" style="display: flex; align-items: flex-end; justify-content: center;">
                    <input type="submit" class="btn btn--red" formtarget="_blank" value="<?php _e("Search", 'friot'); ?>" />
                </div>
            </div>
        </form><?php }?>
        <iframe class="search-iframe" src="https://foglalas.friotyacht.com/iframe?lang=<?php echo ICL_LANGUAGE_CODE; ?>"></iframe>
    </div>
</section><!-- /filter section -->