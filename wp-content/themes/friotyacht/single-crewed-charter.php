<?php get_header(); ?>

<!-- NOTE: Page content wrapper STARTS here -->
<main class="page-content-wrapper" role="main">
    <!-- NOTE: Page content -->
    <div class="container-fluid container--home">
        <section class="section section--header section--page-header" style="background-image: url('<?php echo get_bloginfo('wpurl'); ?>/includes/images/destination-banner.jpg');">
            <div class="container">
                <div class="row page-breadcrumb-row ml-0 mr-0">
                    <nav style="--bs-breadcrumb-divider: '/';" class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <?php
                            if (function_exists('yoast_breadcrumb')) {
                                yoast_breadcrumb('<li class="breadcrumb-item">', '</li>');
                            }
                            ?>
                        </ol>
                    </nav>
                    <div class="button-wrapper col-12 col-md-4 p-0">

                    </div>
                </div>
                <div class="row m-0">
                    <div class="text-block text-center w-100">
                        <h3 class="banner-subtitle"><?php the_title(); ?></h3>
                        <h1 class="banner-title"><?php _e('HOW DOES IT WORK', 'friot'); ?></h1>
                        <div class="row">
                            <div class="step">
                                <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/step.png">
                                <div class="step-layer">
                                    <p>1</p>
                                    <hr>
                                    <p><?php _e('Choose <span>Boat</span>', 'friot'); ?></p>
                                </div>
                            </div>
                            <div class="step-arrow d-none d-md-block">
                                <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/step-arrow.png">
                            </div>
                            <div class="step">
                                <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/step.png">
                                <div class="step-layer">
                                    <p>2</p>
                                    <hr>
                                    <p><?php _e('Select <span>Destination</span>', 'friot'); ?></p>
                                </div>
                            </div>
                            <div class="step-arrow last d-none d-md-block">
                                <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/step-arrow-rotated.png">
                            </div>
                            <div class="step">
                                <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/step.png">
                                <div class="step-layer">
                                    <p>3</p>
                                    <hr>
                                    <p><?php _e('Sit back & <span>Enjoy</span>', 'friot'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /Boat rental -->

        <section class="section section--basic-page bg-lightblue">
            <div class="container mb-4">
                <?php
                $post_id = apply_filters('wpml_object_id', 3553); //3553
                $featured_img_url = get_the_post_thumbnail_url($post_id, 'full');
                $konzultacio_page_id = apply_filters('wpml_object_id', 3558); //3558
                ?>
                <div class="row">
                    <div class="col-12 col-lg-4 d-flex justify-content-center align-items-start mb-3 mb-lg-0">
                        <img src="<?php echo $featured_img_url; ?>" style="max-width:200px;" />
                    </div>
                    <div class="col-12 col-lg-8 d-flex align-items-center">
                        <div>
                            <p class="title"><strong><?php echo get_the_title($post_id); ?></strong></p>
                            <?php echo apply_filters('the_content', get_the_content('', '', $post_id)); ?>
                            <div class="button-wrapper mt-4">
                                <a href="<?php echo get_permalink($konzultacio_page_id); ?>" class="btn btn--red"><?php _e("Ingyenes konzultáció!", 'friot'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--
                <p class="title"><?php echo get_the_title($post_id); ?></p>
                <p class="description"><?php echo apply_filters('the_content', get_the_content('', '', $post_id)); ?></p>
                <div class="button-wrapper mt-4">
                    <a href="<?php echo get_permalink($konzultacio_page_id); ?>" class="btn btn--red"><?php _e("Ingyenes konzultáció!", 'friot'); ?></a>
                </div> -->

            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-4 d-flex justify-content-center align-items-start mb-3 mb-lg-0">
                        <?php the_post_thumbnail('areaimg'); ?>
                    </div>
                    <div class="col-12 col-lg-8">

                        <?php if (has_excerpt()) { ?>
                            <div class="excerpt">
                                <p class="text-center mb-4"><strong><?php the_excerpt(); ?></strong></p>
                            </div>
                        <?php } ?>
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
            <div class="button-wrapper text-center mt-4">
                <?php $enquiry_id = apply_filters('wpml_object_id', 388); ?>
                <a href="<?php echo get_permalink($enquiry_id); ?>?type=<?php echo get_the_title(); ?>" class="btn btn--red"><?php _e('Request an offer', 'friot'); ?></a>
            </div>
        </section>


        <?php
        $destinations = get_terms(array(
            'taxonomy' => 'destinations',
            'hide_empty' => false,
                //'hierarchical' => true,
        ));
        if (count($destinations) > 0) {
            ?>
            <!-- section destinations -->
            <section class="section section--card-block bg-lightblue" id="destinations">
                <div class="container">
                    <?php
                    $post_id = apply_filters('wpml_object_id', 83);
                    //$post_id = 83;
                    ?>
                    <div class="row section-title-row">
                        <div class="col-12 col-md-6">
                            <h2 class="section-title"><?php echo get_the_title($post_id); ?></h2>
                        </div>
                        <div class="col-12 col-md-6">
                            <p class="section-subtitle"><?php echo apply_filters('the_content', get_the_content('', '', $post_id)); ?></p>
                        </div>
                    </div>
                    <div class="carousel-wrapper mt-4">
                        <div class="owl-carousel card-layout-4">
                            <?php
                            foreach ($destinations as $destination) {
                                if ($destination->term_id === apply_filters('wpml_object_id', 677, 'destinations') || $destination->term_id === apply_filters('wpml_object_id', 678, 'destinations')) {
                                    continue;
                                }
                                ?>
                                <div class="item">
                                    <div class="display-card">
                                        <?php if ($destination->term_id === 209) { ?>
                                            <a href="https://foglalas.friotyacht.com/hajok/Horvátország/anytime/all-type">
                                            <?php } elseif ($destination->term_id === 97) { ?>
                                                <a href="https://booking.friotyacht.com/yachts/Croatia/anytime/all-type">
                                                <?php } else { ?>
                                                    <a href="<?php echo get_category_link($destination->term_id); ?>">
                                                    <?php } ?>
                                                    <div class="badge-container">
                                                        <div class="badge">
                                                            <span><?php echo $destination->name; ?></span>
                                                        </div>
                                                    </div>
                                                    <img src="<?php echo z_taxonomy_image_url($destination->term_id, 'areaimg'); ?>" />
                                                    <div class="card-layer">
                                                        <p class="view"><?php _e("view", 'friot'); ?></p>
                                                    </div>
                                                </a>
                                                </div>
                                                </div>
                                            <?php } ?>

                                            </div>
                                            <div class="button-wrapper text-center mt-3">
                                                <?php $destinations_list_id = apply_filters('wpml_object_id', 89); ?>
                                                <a href="<?php echo get_permalink($destinations_list_id); ?>" class="btn btn--outline--blue"><?php _e("More destinations", 'friot'); ?></a>
                                            </div>
                                            </div>
                                            </div>
                                            </section>
                                            <!-- /destinations -->
                                        <?php } ?>
                                        </div>
                                        </main><!-- NOTE: Page content wrapper ENDS here -->

                                        <?php get_footer(); ?>