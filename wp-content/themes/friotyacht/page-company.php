<?php
/*
 * Template name: Page Company
 */
get_header();
?>

<!-- NOTE: Page content wrapper STARTS here -->
<main class="page-content-wrapper" role="main">
    <?php
    if (have_posts()) : while (have_posts()) : the_post();
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
            ?>
            <!-- NOTE: Page content -->
            <div class="container-fluid container--home">
                <section class="section section--header section--page-header" style="background-image: url('<?php echo $featured_img_url; ?>');">
                    <div class="container">
                        <div class="row page-breadcrumb-row ml-0 mr-0">
                            <nav style="--bs-breadcrumb-divider: '/';" class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <?php
                                    if (function_exists('yoast_breadcrumb')) {
                                        yoast_breadcrumb('<li class="breadcrumb-item">', '</li>');
                                    }
                                    ?>
                                </ol>
                            </nav>
                            <div class="button-wrapper col-12 col-md-4 p-0">

                            </div>
                        </div>
                        <div class="row m-0">
                            <div class="text-block text-center w-100">
                                <!-- <h3 class="banner-subtitle">Crewed charter</h3> -->
                                <h1 class="banner-title"><?php the_title(); ?></h1>
                            </div>
                        </div>
                    </div>
                </section>


                <!-- section counter -->
                <section class="section section--basic-page bg-lightblue">
                    <div class="container">
                        <?php /* <div class="row mb-5">
                            <h1><?php the_title(); ?></h1>
                        </div> */ ?>
                        <div class="row mb-5">
                            <?php if (has_excerpt()) { ?>
                                <div class="excerpt col-12 col-md-6 d-flex justify-content-center align-items-md-center order-2 order-md-1">
                                    <p><?php echo get_the_excerpt(); ?></p>
                                </div>
                            <?php } ?>
                            <div class="excerpt col-12 col-md-6 d-flex justify-content-center align-items-md-center order-1 order-md-2">
                                <?php the_post_thumbnail('large'); ?>
                            </div>
                        </div>
                        <div class="row mb-5 w-100 mx-auto">
                            <?php the_content(); ?>
                            <div class="d-flex flex-row justify-content-between align-items-center mt-5 flex-wrap">
                                <img src="https://friotyacht.com/wp-content/uploads/2022/04/logo-darkblue.png" alt="" />
                                <h4 class="ms-0 mt-2 ms-md-3 mt-md-0 mb-0" style="color:#d5ecf4;font-size:27px!important;">The <span style="color:#e84250;">happy</span> flow!</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 mb-3 col-md-6 mb-md-0">
                                <div class="d-flex justify-content-center align-items-md-center h-100">
                                    <div class="grey-info-box">
                                        <?php dynamic_sidebar('contact-1'); ?>                                
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="d-flex justify-content-center align-items-md-center h-100">
                                    <div class="grey-info-box">
                                        <?php dynamic_sidebar('contact-2'); ?>                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /counter -->

            </div>
            <?php
        endwhile;
    endif;
    ?>
</main><!-- NOTE: Page content wrapper ENDS here -->

<?php get_footer() ?>