<?php
/*
 * Template name: Page Thank You Pre register
 */

get_header();
$postedToken = filter_input(INPUT_POST, 'token');
unset($_SESSION['errormessage']);
unset($_SESSION['bookingdata']);
if (!empty($postedToken)) {
    if (isTokenValid($postedToken)) {
        if (isset($_POST['action']) && ($_POST['action'] == "turahajo_eloregisztracio")) {
            $recaptcha_response = recaptcha_validate($_POST['googletoken']);
            if (!$recaptcha_response['success'] || $recaptcha_response['score'] < 0.5 || $recaptcha_response['action'] != 'request_send_form') {
                $url = get_bloginfo('wpurl');
                wp_safe_redirect($url);
                die();
            } else {

                if (!isset($_POST['turahajoeloregisztraciocheck']) || !wp_verify_nonce($_POST['turahajoeloregisztraciocheck'], 'turahajoeloregisztracio')) {
                    $redirect_to = get_bloginfo('wpurl');
                    wp_safe_redirect($redirect_to);
                    die();
                } else {

                    $has_error = false;
                    $reqired_fields = ['tura_id', 'firstname', 'lastname', 'email_address', 'phone'];
                    foreach ($reqired_fields as $reqired_field) {
                        if (empty($_POST[$reqired_field])) {
                            $error = __("Required field is empty!", 'friot');
                            $has_error = true;
                        }
                    }

                    $tura_id = intval($_POST['tura_id']);
                    $tura_neve = get_the_title($tura_id);

                    $firstname = sanitize_text_field($_POST['firstname']);
                    $lastname = sanitize_text_field($_POST['lastname']);
                    $email_address = sanitize_email($_POST['email_address']);
                    $phone = sanitize_text_field($_POST['phone']);

                    $other_information = sanitize_textarea_field($_POST['other_information']);

                    if ($has_error) {
                        $bookingdata['firstname'] = $firstname;
                        $bookingdata['lastname'] = $lastname;
                        $bookingdata['email_address'] = $email_address;
                        $bookingdata['phone'] = $phone;
                        $bookingdata['other_information'] = $other_information;
                        $_SESSION['errormessage'] = $error;
                        $_SESSION['bookingdata'] = $bookingdata;
                        /*                         * local -- 2152 */
                        /*                         * calvus.xyz -- 2151 */
                        /*                         * éles -- 2164 */
                        $preregister_form_id_option = $friot_options['tura_eloregisztracio'];
                        $preregister_finalize_id = apply_filters('wpml_object_id', $preregister_form_id_option);
                        $redirect_to = get_permalink($preregister_finalize_id) . "?action=turahajo_foglalas_turavalasztas&tura_id=" . $tura_id;
                        wp_safe_redirect($redirect_to);
                        exit;
                    } else {

                        // ide jön az checkelés és email kiküldés
                        $content .= "<h3>Friot Yacht -  " . __("Pre register ", "friot") . "</h3>";
                        $content .= "<strong>" . __("Selected Cabin Charter", "friot") . "</strong>: " . $tura_neve . "<br/>";

                        $content .= "<strong>" . __("Name", "friot") . "</strong>: " . $firstname . " " . $lastname . "<br/>";

                        $content .= "<strong>" . __("Email", "friot") . "</strong>: " . $email_address . "<br/> ";

                        $content .= "<strong>" . __("Telefon", "friot") . "</strong>: " . $phone . "<br/> ";
                        if (!empty($other_information)) {
                            $content .= "<br/><strong>" . __("Other information", "friot") . "</strong>:<br/>" . $other_information . "<br/> ";
                        }


                        $bookings = array(
                            'post_author' => 1,
                            'post_status' => 'private',
                            'post_title' => $firstname . " " . $lastname,
                            'post_content' => $content,
                            'post_type' => 'bookings'
                        );
                        $post_id = wp_insert_post($bookings);

                        if ($post_id) {
                            // check to make sure its a successful upload

                            update_post_meta($post_id, '_cabin_charter_bookings_mbox_tura_id', $tura_id);
                            update_post_meta($post_id, '_cabin_charter_bookings_mbox_phone', $phone);
                            update_post_meta($post_id, '_cabin_charter_bookings_mbox_email', $email_address);
                        }
                        $subject = "Friot Yacht - " . __("Pre register ", "friot") . $tura_neve;
                        $headers = array('Content-Type: text/html; charset=UTF-8');
                        $headers[] = 'From: Friot Yacht  <info@friotyacht.com>';
                        //wp_mail('oliver.szenyi@calv.us', $subject, $content, $headers);
                        wp_mail('info@friotyacht.com', $subject, $content, $headers);
                        $_thank_you_mailsubject = "Friot Yacht -  " . __("Pre register ", "friot") . $tura_neve;
                        $_thank_you_mailtext = __("Thank you, we have received your pre registration, our colleague will contact you within 24 hours to agree the details and answer any questions you may have.
                            <br /><br />
                            Best regards:<br />
                            Friot Yacht team", "friot");
                        wp_mail($email_address, $_thank_you_mailsubject, $_thank_you_mailtext, $headers);
                    }
                }
            }
        }
    } else {
        $url = get_bloginfo('wpurl');
        wp_safe_redirect($url);
        die();
    }
} else {
    $url = get_bloginfo('wpurl');
    wp_safe_redirect($url);
    die();
}
?>
<script>
    fbq('track', 'Lead');
</script>
<!-- NOTE: Page content wrapper STARTS here -->
<main class="page-content-wrapper" role="main">
    <?php
    if (have_posts()) : while (have_posts()) : the_post();
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
            ?>
            <!-- NOTE: Page content -->
            <div class="container-fluid container--home">
                <section class="section section--header section--page-header" style="background-image: url('<?= $featured_img_url; ?>');">
                    <div class="container">
                        <div class="row page-breadcrumb-row ml-0 mr-0">
                            <nav style="--bs-breadcrumb-divider: '/';" class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <?php
                                    if (function_exists('yoast_breadcrumb')) {
                                        yoast_breadcrumb('<li class="breadcrumb-item">', '</li>');
                                    }
                                    ?>
                                </ol>
                            </nav>
                            <div class="button-wrapper col-12 col-md-4 p-0">

                            </div>
                        </div>
                        <div class="row m-0">
                            <div class="text-block text-center w-100">
                                <!-- <h3 class="banner-subtitle"></h3> -->
                                <h1 class="banner-title"><?php the_title(); ?></h1>
                            </div>
                        </div>
                    </div>
                </section>


                <!-- section counter -->
                <section class="section section--basic-page bg-lightblue">
                    <div class="container">
                        <?php if (has_excerpt()) { ?>
                            <div class="excerpt">
                                <p class="text-center mb-4"><strong><?php the_excerpt(); ?></strong></p>
                            </div>
                        <?php } ?>
                        <?php the_content(); ?>
                        <?php
                        echo $content;
                        ?>
                    </div>
                </section>
                <!-- /counter -->

            </div>
            <?php
        endwhile;
    endif;
    ?>
</main><!-- NOTE: Page content wrapper ENDS here -->

<?php get_footer() ?>