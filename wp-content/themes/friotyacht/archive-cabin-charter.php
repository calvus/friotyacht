<?php
get_header();
?>

<!-- NOTE: Page content wrapper STARTS here -->
<main class="page-content-wrapper" role="main">
    <!-- NOTE: Page content -->
    <div class="container-fluid container--home">

        <!-- cabin charter -->
        <section class="section section--card-block extra-padding bg-lightblue" id="cabin_charter">
            <div class="container">
                <?php
                $post_id = apply_filters('wpml_object_id', 1246);
                //$post_id = 80;
                ?>
                <div class="row section-title-row">
                    <div class="col-12 col-md-3">
                        <h2 class="section-title"><?php echo get_the_title($post_id); ?></h2>
                    </div>
                    <div class="col-12 col-md-9">
                        <span class="section-subtitle"> <?php echo apply_filters('the_content', get_the_content('', '', $post_id)); ?></span>
                    </div>
                </div>

                <!-- carousel display on small screen -->
                <div class="d-block d-lg-none mt-4">
                    <div class="carousel-wrapper">
                        <div class="owl-carousel card-layout-3">
                            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                    <div class="item">
                                        <div class="display-card">
                                            <a href="<?php the_permalink(); ?>">
                                                <div class="badge-container">
                                                    <div class="badge">
                                                        <span><?php the_title(); ?></span>
                                                    </div>

                                                    <?php if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_discount', true)) { ?>
                                                        <div class="badge badge-dicount">
                                                            <span>-<?php echo intval(get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_discount', true)); ?>%</span>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>  
                                                <?php the_post_thumbnail('areaimg'); ?>
                                                <div class="card-layer">
                                                    <p class="view"><?php _e("view", "friot"); ?></p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <?php
                                endwhile;
                            endif;
                            rewind_posts();
                            ?>
                        </div>

                    </div>
                </div>
                <!-- list view on large -->
                <div class="d-none d-lg-block mt-4">
                    <div class="row three-card-layout">
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                <div class="display-card">
                                    <a href = "<?php the_permalink(); ?>">
                                        <div class="badge-container">
                                            <div class="badge">
                                                <span><?php the_title(); ?></span>
                                            </div>
                                            <?php if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_discount', true)) { ?>
                                                <div class="badge badge-dicount">
                                                    <span><?php echo intval(get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_discount', true)); ?>% <?php _e("discount", "friot"); ?></span>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <?php the_post_thumbnail('areaimg'); ?>
                                        <div class="card-layer">
                                            <p class="view"><?php _e("view", "friot"); ?></p>
                                        </div>
                                    </a>
                                </div>
                                <?php
                            endwhile;
                        endif;
                        rewind_posts();
                        ?>
                    </div>

                </div>

            </div>
        </section>
        <!-- cabin charter -->

        <?php
        $tesimonial_args = array('post_type' => 'testimonial', 'posts_per_page' => -1);
        $tesimonial_loop = new WP_Query($tesimonial_args);
        if ($tesimonial_loop->have_posts()) :
            ?>   
            <!-- section Testimonial -->
            <section class="section section--carousel bg-parallax testimonial-section">
                <div class="container">
                    <div class="row section-title-row">
                        <div class="col-12 col-md-6">
                            <h2 class="section-title"><?php _e('Testimonials', 'friot'); ?></h2>
                        </div>
                        <div class="col-12 col-md-6">
                            <p class="section-subtitle"><?php //_e('Our passengers said about us', 'friot'); ?></p>
                        </div>
                    </div>
                    <div class="carousel-wrapper">
                        <div id="owltesimonial" class="owl-carousel owl-3-col">
                            <?php while ($tesimonial_loop->have_posts()) : $tesimonial_loop->the_post(); ?>
                                <div class="item" id="konzultacio_slide">

                                    <div class="row">


                                        <div class="text-wrapper">                                            
                                            <div class="description"><?php the_content(); ?></div>
                                            <p class="title"><?php the_title(); ?></p>
                                        </div>

                                    </div>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /Testimonial -->
            <?php
        endif;
        wp_reset_query();
        ?>
    </div>
</main><!-- NOTE: Page content wrapper ENDS here -->
<?php get_footer(); ?>