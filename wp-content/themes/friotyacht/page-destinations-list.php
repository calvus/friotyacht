<?php
/*
 * Template name: Destinations list
 */

get_header();
?>

<!-- NOTE: Page content wrapper STARTS here -->
<main class="page-content-wrapper" role="main">
    <!-- NOTE: Page content -->
    <div class="container-fluid container--home">






        <!-- cabin charter -->
        <section class="section section--card-block extra-padding bg-lightblue" id="cabin_charter">
            <div class="container">
                <?php
                $post_id = apply_filters('wpml_object_id', 83);
                //$post_id = 83;
                ?>
                <div class="row section-title-row">
                    <div class="col-12 col-md-6">
                        <h2 class="section-title"><?php echo get_the_title($post_id); ?></h2>
                    </div>
                    <div class="col-12 col-md-6">
                        <span class="section-subtitle"> <?php echo apply_filters('the_content', get_the_content('', '', $post_id)); ?></span>
                    </div>
                </div>
                <div class="row mb-5 mt-4">
                    <nav class="p-0">
                        <div class="nav nav-tabs destinations-tab-navigation" id="nav-tab" role="tablist">
                            <button class="nav-link active" id="nav-ocean-tab" data-bs-toggle="tab" data-bs-target="#nav-ocean" type="button" role="tab" aria-controls="nav-ocean" aria-selected="true"><?php _e("Ocean Sailing", 'friot'); ?></button>
                            <button class="nav-link" id="nav-canal-tab" data-bs-toggle="tab" data-bs-target="#nav-canal" type="button" role="tab" aria-controls="nav-canal" aria-selected="false"><?php _e("Canal Sailing", 'friot'); ?></button>
                        </div>
                    </nav>
                </div>
                <div class="tab-content destinations-tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-ocean" role="tabpanel" aria-labelledby="nav-ocean-tab">
                        <?php
                        // local -> 372
                        // calvus.xyz -> 275
                        //friotyacht.com -> 677
                        $ocean_term_id = apply_filters('wpml_object_id', 677, 'destinations');
                        $destinations = get_terms(array(
                            'parent' => $ocean_term_id,
                            'taxonomy' => 'destinations',
                            'hide_empty' => true,
                                //'hierarchical' => true,
                        ));
                        if (count($destinations) > 0) {
                            ?>
                            <!-- carousel display on small screen -->
                            <div class="d-block d-lg-none mt-4">
                                <div class="carousel-wrapper">
                                    <div class="owl-carousel card-layout-3">
                                        <?php foreach ($destinations as $destination) { ?>
                                            <div class="item">
                                                <div class="display-card">
                                                    <a href="<?php echo get_category_link($destination->term_id); ?>">
                                                        <div class="badge-container">
                                                            <div class="badge">
                                                                <span><?php echo $destination->name; ?></span>
                                                            </div>
                                                        </div>
                                                        <img src="<?php echo z_taxonomy_image_url($destination->term_id); ?>">
                                                        <div class="card-layer">
                                                            <p class="view"><?php _e('view', 'friot'); ?></p>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <!-- list view on large -->
                            <div class="d-none d-lg-block mt-4">
                                <div class="row three-card-layout">
                                    <?php foreach ($destinations as $destination) { ?>
                                        <div class="display-card">
                                            <a href="<?php echo get_category_link($destination->term_id); ?>">
                                                <div class="badge-container">
                                                    <div class="badge">
                                                        <span><?php echo $destination->name; ?></span>
                                                    </div>
                                                </div>
                                                <img src="<?php echo z_taxonomy_image_url($destination->term_id, 'areaimg'); ?>">
                                                <div class="card-layer">
                                                    <p class="view"><?php _e('view', 'friot'); ?></p>
                                                </div>
                                            </a>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="tab-pane fade" id="nav-canal" role="tabpanel" aria-labelledby="nav-canal-tab">
                        <?php
                        // local -> 372
                        // calvus.xyz -> 276
                        //friotyacht.com -> 678
                        $canal_term_id = apply_filters('wpml_object_id', 678, 'destinations');
                        $destinations = get_terms(array(
                            'parent' => $canal_term_id,
                            'taxonomy' => 'destinations',
                            'hide_empty' => true,
                                //'hierarchical' => true,
                        ));
                        if (count($destinations) > 0) {
                            ?>
                            <!-- carousel display on small screen -->
                            <div class="d-block d-lg-none mt-4">
                                <div class="carousel-wrapper">
                                    <div class="owl-carousel card-layout-3">
                                        <?php foreach ($destinations as $destination) { ?>
                                            <div class="item">
                                                <div class="display-card">
                                                    <a href="<?php echo get_category_link($destination->term_id); ?>">
                                                        <div class="badge-container">
                                                            <div class="badge">
                                                                <span><?php echo $destination->name; ?></span>
                                                            </div>
                                                        </div>
                                                        <img src="<?php echo z_taxonomy_image_url($destination->term_id); ?>">
                                                        <div class="card-layer">
                                                            <p class="view"><?php _e('view', 'friot'); ?></p>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <!-- list view on large -->
                            <div class="d-none d-lg-block mt-4">
                                <div class="row three-card-layout">
                                    <?php foreach ($destinations as $destination) { ?>
                                        <div class="display-card">
                                            <a href="<?php echo get_category_link($destination->term_id); ?>">
                                                <div class="badge-container">
                                                    <div class="badge">
                                                        <span><?php echo $destination->name; ?></span>
                                                    </div>
                                                </div>
                                                <img src="<?php echo z_taxonomy_image_url($destination->term_id, 'areaimg'); ?>">
                                                <div class="card-layer">
                                                    <p class="view"><?php _e('view', 'friot'); ?></p>
                                                </div>
                                            </a>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <!-- cabin charter -->

    </div>
</main><!-- NOTE: Page content wrapper ENDS here -->
<?php get_footer(); ?>