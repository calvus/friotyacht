<!-- section Boat rental -->
<?php
$args = array('post_type' => 'boatrentals-links', 'posts_per_page' => -1);
$loop = new WP_Query($args);
if ($loop->have_posts()) :
    ?>
    <section class="section section--card-block bg-parallax" id="boat_rental">
        <div class="container">
            <?php
            $post_id = apply_filters('wpml_object_id', 84);
            //$post_id = 84;
            ?>
            <div class="row section-title-row">
                <div class="col-12 col-md-6">
                    <h2 class="section-title"><?php echo get_the_title($post_id); ?></h2>
                </div>
                <div class="col-12 col-md-6">
                    <p class="section-subtitle"><?php echo apply_filters('the_content', get_the_content('', '', $post_id)); ?></p>
                </div>
            </div>
            <div class="carousel-wrapper mt-4">
                <div class="owl-carousel card-layout-4">
                    <?php while ($loop->have_posts()) : $loop->the_post(); ?>
                        <div class="item">
                            <div class="display-card">
                                <a href="<?php echo get_the_excerpt(); ?>" target="_blank">
                                    <div class="badge-container">
                                        <div class="badge">
                                            <span><?php the_title(); ?></span>
                                        </div>
                                    </div>
                                    <?php the_post_thumbnail('areaimg'); ?>
                                    <div class="card-layer">
                                        <p class="view"><?php _e("view", 'friot'); ?></p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php endwhile; ?>


                </div>
                <div class="button-wrapper text-center mt-3">
                    <?php
                    $bookingSiteLink = get_option('booking_site_link');
                    if (ICL_LANGUAGE_CODE == 'en') {
                        $bookingSiteLink = get_option('booking_site_link_' . ICL_LANGUAGE_CODE);
                    }
                    ?>
                    <a href="<?php echo $bookingSiteLink ?>" class="btn btn--outline--blue"><?php _e("More offers", 'friot'); ?></a>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<!-- /Boat rental -->