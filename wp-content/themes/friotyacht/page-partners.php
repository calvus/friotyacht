<?php
/*
 * Template name: Partners
 */


get_header();
?>

<!-- NOTE: Page content wrapper STARTS here -->
<main class="page-content-wrapper" role="main">

    <!-- NOTE: Page content -->
    <div class="container-fluid container--home">
        <?php
        if (have_posts()) : while (have_posts()) : the_post();
                $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
                ?>
                <section class="section section--header section--page-header" style="background-image: url('<?php echo $featured_img_url; ?>');">
                    <div class="container">
                        <div class="row page-breadcrumb-row ml-0 mr-0">
                            <nav style="--bs-breadcrumb-divider: '/';" class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <?php
                                    if (function_exists('yoast_breadcrumb')) {
                                        yoast_breadcrumb('<li class="breadcrumb-item">', '</li>');
                                    }
                                    ?>
                                </ol>
                            </nav>
                            <div class="button-wrapper col-12 col-md-4 p-0">
                              
                            </div>
                        </div>
                        <div class="row m-0">
                            <div class="text-block text-center w-100">

                                <h1 class="banner-title"><?php the_title(); ?></h1>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="section section--basic-page bg-lightblue">
                    <div class="container">
                        <?php the_post_thumbnail('large', array('class' => 'aligncenter')); ?>
                        <?php if (has_excerpt()) { ?>
                            <strong><?php the_excerpt(); ?></strong>
                        <?php } ?>
                        <?php the_content(); ?>
                        <?php
                        $args = array('post_type' => 'partners', 'posts_per_page' => -1);
                        $loop = new WP_Query($args);
                        if ($loop->have_posts()):
                            ?>
                            <div class="carousel-wrapper mt-4">
                                <div class="owl-carousel partners-carosuel">
                                    <?php while ($loop->have_posts()) : $loop->the_post(); ?>
                                        <div class="item">
                                            <a href="<?php echo get_post_meta(get_the_ID(), '_partner_link', true); ?>" target="_blank">
                                                <div class="partner-item">
                                                    <div class="logo-display">
                                                        <?php the_post_thumbnail('medium'); ?>
                                                    </div>
                                                    <h3 ><?php the_title(); ?></h3>
                                                    <div class="mt-2"><?php the_content(); ?> </div>
                                                </div>
                                            </a>
                                        </div>
                                    <?php endwhile; ?>

                                </div>
                            </div>
                            <?php
                        endif;
                        wp_reset_query();
                        ?>
                    </div>
                </section>

                <?php
            endwhile;
        endif;
        ?>
        <?php get_template_part('boat-rental'); ?>
    </div>
</main><!-- NOTE: Page content wrapper ENDS here -->
<?php get_footer(); ?>