<?php

/*
  Template Name: Hírlevél CSV mentés
 */

if (!defined('ABSPATH')) {
    exit('Direct script access denied.');
}




global $current_user;
$uid = $current_user->ID;

$right = current_user_can('edit_post');

//   $right=true;

if (!$right) {
    $bl = get_bloginfo('wpurl');
    wp_safe_redirect($bl);
    die();
} else {

    global $wpdb;
    $newsletter = $wpdb->get_results('SELECT * FROM friotyacht_newsletter', ARRAY_A);
    $csv_output = '';

    $csv_output = "Hirlevel Id,Email cim, Hirlevel név,Hirlevel nyelv,Hirlevel datum,Hirlevel aktiv \n";
    foreach ($newsletter as $sor) {


        /* $sor1['newsletter_id'] = str_replace("\r", '', str_replace("\n", '', stripslashes($sor['newsletter_id'])));
          $sor1['newsletter_email' ] = str_replace("\r", '', str_replace("\n", '', stripslashes($sor['newsletter_email'])));
          // // $sor1['hirlevel_nev'] = str_replace("\r", '', str_replace("\n", '', stripslashes($sor['hirlevel_nev'])));
          $sor1['newsletter_name'] = str_replace("\r", '', str_replace("\n", '', stripslashes($sor['newsletter_name'])));
          $sor1['newsletter_subscribe_date'] = str_replace("\r", '', str_replace("\n", '', stripslashes($sor['newsletter_subscribe_date'])));
          $sor1['newsletter_email_sent'] = str_replace("\r", '', str_replace("\n", '', stripslashes($sor['newsletter_email_sent'])));
         */


        $csv_output .= $sor['newsletter_id'] . ",";
        $csv_output .= $sor['newsletter_email'] . ",";
        $csv_output .= $sor['newsletter_name'] . ",";
        $csv_output .= $sor['newsletter_lang'] . ",";
        $csv_output .= $sor['newsletter_subscribe_date'] . ",";
        $csv_output .= $sor['newsletter_email_sent'] . ", \n";
    }
//print_r($csv_output);

    $file = "hirlevel_feliratkozok";

    $filename = "friot_newsletter_subscribers_" . date("Y-m-d_H-i", time());
    header("Content-type: application/vnd.ms-excel");
    header("Content-disposition: csv" . date("Y-m-d") . ".csv");
    header("Content-disposition: filename=" . $filename . ".csv");

    echo $csv_output;
    //$csv_output = iconv("utf-8", "utf-8//ignore", $csv_output);
    // print_r(iconv("UTF-8", "ISO-8859-2//TRANSLIT", $csv_output));
    exit;
}
?>