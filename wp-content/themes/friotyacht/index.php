<?php
/*
 * Template name: Home page
 */

get_header();
global $my_home_url;
?>

<!-- NOTE: Page content wrapper STARTS here -->
<main class="page-content-wrapper" role="main">
    <!-- NOTE: Page content -->
    <div class="container-fluid container--home">
        <section class="section section--header">
            <div class="container">
                <div class="row m-0">
                    <div class="text-block">
                        <h1 class="banner-title"><?php _e("The <span>happy</span> flow", 'friot'); ?></h1>

                        <ul class="PrimaryNav with-indicator d-none d-md-block">

                            <li class="Nav-item"><a href="<?php echo $my_home_url; ?>/#boat_rental"><?php _e("Boat rental", 'friot'); ?></a></li>
                            <li class="Nav-item"><a href="<?php echo $my_home_url; ?>/#destinations"><?php _e("Destinations", 'friot'); ?></a></li>
                            <li class="Nav-item"><a href="<?php echo $my_home_url; ?>/#cabin_charter"><?php _e("Cabin Charter", 'friot'); ?></a></li>
                        </ul>
                    </div>
                    <div class="image-block">
                        <div class="image-wrapper">
                            <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/home-hero.png" />
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <?php /* get_template_part('charter-search'); */ ?>


        <!-- cabin charter -->
        <section class="section section--card-block extra-padding bg-lightblue section-below-iframe" id="cabin_charter">
            <div class="container">
                <?php
                $post_id = apply_filters('wpml_object_id', 80);
                //$post_id = 80;
                ?>
                <div class="row section-title-row">
                    <div class="col-12 col-md-6">
                        <h2 class="section-title"><?php echo get_the_title($post_id); ?></h2>
                    </div>
                    <div class="col-12 col-md-6">
                        <span class="section-subtitle"> <?php echo apply_filters('the_content', get_the_content('', '', $post_id)); ?></span>
                    </div>
                </div>
                <!-- carousel display on small screen -->
                <div class="d-block d-lg-none mt-4">
                    <div class="carousel-wrapper">
                        <div class="owl-carousel card-layout-3">
                            <?php
                            $args = array('post_type' => 'cabin-charter', 'posts_per_page' => -1);
                            $loop = new WP_Query($args);
                            if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
                                    ?>
                                    <div class="item">
                                        <div class="display-card">
                                            <a href="<?php the_permalink(); ?>">
                                                <div class="badge-container">
                                                    <div class="badge">
                                                        <span><?php the_title(); ?></span>
                                                    </div>
                                                    <?php if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_discount', true)) { ?>
                                                        <div class="badge badge-dicount">
                                                            <span><?php echo intval(get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_discount', true)); ?>% <?php _e("discount", "friot"); ?></span>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                                <?php the_post_thumbnail('areaimg'); ?>
                                                <div class="card-layer">
                                                    <p class="view"><?php _e("view", 'friot'); ?></p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <?php
                                endwhile;
                            endif;
                            ?>

                        </div>
                        <div class="button-wrapper text-center mt-3">
                            <a href="<?php echo get_post_type_archive_link('cabin-charter'); ?>" class="btn btn--red"><?php _e("More Cabin Charters", 'friot'); ?></a>
                        </div>
                    </div>
                </div>
                <!-- list view on large -->
                <div class="d-none d-lg-block mt-4">
                    <div class="row three-card-layout">
                        <?php
                        $args = array('post_type' => 'cabin-charter', 'posts_per_page' => -1);
                        $loop = new WP_Query($args);
                        if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
                                ?>
                                <div class="display-card">
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="badge-container">
                                            <div class="badge">
                                                <span><?php the_title(); ?></span>
                                            </div>
                                            <?php if (get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_discount', true)) { ?>
                                                <div class="badge badge-dicount">
                                                    <span><?php echo intval(get_post_meta(get_the_ID(), '_cabin_charter_adatok_mbox_discount', true)); ?>% <?php _e("discount", "friot"); ?></span>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <?php the_post_thumbnail('areaimg'); ?>
                                        <div class="card-layer">
                                            <p class="view"><?php _e("view", 'friot'); ?></p>
                                        </div>
                                    </a>
                                </div>
                                <?php
                            endwhile;
                        endif;
                        wp_reset_query();
                        ?>
                    </div>
                    <div class="button-wrapper text-center mt-3">
                        <a href="<?php echo get_post_type_archive_link('cabin-charter'); ?>" class="btn btn--red"><?php _e("More Cabin Charters", 'friot'); ?></a>
                    </div>
                </div>
            </div>
        </section>
        <!-- cabin charter -->


        <!-- section Skippered Charter -->
        <section class="section section--carousel bg-parallax">
            <div class="container">
                <div class="carousel-wrapper">
                    <div id="owlskipper" class="owl-carousel owl-2-col">
                        <div class="item" id="konzultacio_slide">
                            <?php
                            $post_id = apply_filters('wpml_object_id', 3553); //3553
                            $featured_img_url = get_the_post_thumbnail_url($post_id, 'full');
                            $konzultacio_page_id = apply_filters('wpml_object_id', 3558); //3558
                            ?>
                            <div class="row">
                                <div class="left">
                                    <div class="image-wrapper">
                                        <img src="<?php echo $featured_img_url; ?>" />
                                    </div>
                                </div>
                                <div class="right">
                                    <div class="text-wrapper">
                                        <p class="title"><?php echo get_the_title($post_id); ?></p>
                                        <p class="description"><?php echo apply_filters('the_content', get_the_content('', '', $post_id)); ?></p>
                                        <div class="button-wrapper mt-4">
                                            <a href="<?php echo get_permalink($konzultacio_page_id); ?>" class="btn btn--red"><?php _e("Ingyenes konzultáció!", 'friot'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item" id="skippered_charter">
                            <?php
                            $post_id = apply_filters('wpml_object_id', 81);
                            $featured_img_url = get_the_post_thumbnail_url($post_id, 'full');
                            $skipper_id = apply_filters('wpml_object_id', 316);
                            ?>
                            <div class="row">
                                <div class="left">
                                    <div class="image-wrapper">
                                        <img src="<?php echo $featured_img_url; ?>" />
                                    </div>
                                </div>
                                <div class="right">
                                    <div class="text-wrapper">
                                        <p class="title"><?php echo get_the_title($post_id); ?></p>
                                        <p class="description"><?php echo apply_filters('the_content', get_the_content('', '', $post_id)); ?></p>
                                        <div class="button-wrapper mt-4">
                                            <a href="<?php echo get_permalink($skipper_id); ?>" class="btn btn--red"><?php _e("More info", 'friot'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item" id="corporate_events">
                            <?php
                            $post_id = apply_filters('wpml_object_id', 82);
                            $featured_img_url = get_the_post_thumbnail_url($post_id, 'full');
                            $corporate_id = apply_filters('wpml_object_id', 322);
                            ?>
                            <div class="row">
                                <div class="left">
                                    <div class="image-wrapper">
                                        <img src="<?php echo $featured_img_url; ?>" />
                                    </div>
                                </div>
                                <div class="right">
                                    <div class="text-wrapper">
                                        <p class="title"><?php echo get_the_title($post_id); ?></p>
                                        <p class="description"><?php echo apply_filters('the_content', get_the_content('', '', $post_id)); ?></p>
                                        <div class="button-wrapper mt-4">
                                            <a href="<?php echo get_permalink($corporate_id); ?>" class="btn btn--red"><?php _e("More info", 'friot'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /Skippered Charter -->

        <?php
        $destinations = get_terms(array(
            'taxonomy' => 'destinations',
            'hide_empty' => false,
                //'hierarchical' => true,
        ));
        if (count($destinations) > 0) {
            ?>
            <!-- section destinations -->
            <section class="section section--card-block bg-lightblue" id="destinations">
                <div class="container">
                    <?php
                    $post_id = apply_filters('wpml_object_id', 83);
                    //$post_id = 83;
                    ?>
                    <div class="row section-title-row">
                        <div class="col-12 col-md-6">
                            <h2 class="section-title"><?php echo get_the_title($post_id); ?></h2>
                        </div>
                        <div class="col-12 col-md-6">
                            <p class="section-subtitle"><?php echo apply_filters('the_content', get_the_content('', '', $post_id)); ?></p>
                        </div>
                    </div>
                    <div class="carousel-wrapper mt-4">
                        <div class="owl-carousel card-layout-4">
                            <?php
                            foreach ($destinations as $destination) {
                                if ($destination->term_id === apply_filters('wpml_object_id', 677, 'destinations') || $destination->term_id === apply_filters('wpml_object_id', 678, 'destinations')) {
                                    continue;
                                }
                                ?>
                                <div class="item">
                                    <div class="display-card">
                                        <?php if ($destination->term_id === 209) { ?>
                                            <a href="https://foglalas.friotyacht.com/hajok/Horvátország/anytime/all-type">
                                            <?php } elseif ($destination->term_id === 97) { ?>
                                                <a href="https://booking.friotyacht.com/yachts/Croatia/anytime/all-type">
                                                <?php } else { ?>
                                                    <a href="<?php echo get_category_link($destination->term_id); ?>">
                                                    <?php } ?>
                                                    <div class="badge-container">
                                                        <div class="badge">
                                                            <span><?php echo $destination->name; ?></span>
                                                        </div>
                                                    </div>
                                                    <img src="<?php echo z_taxonomy_image_url($destination->term_id, 'areaimg'); ?>" />
                                                    <div class="card-layer">
                                                        <p class="view"><?php _e("view", 'friot'); ?></p>
                                                    </div>
                                                </a>
                                                </div>
                                                </div>
                                            <?php } ?>

                                            </div>
                                            <div class="button-wrapper text-center mt-3">
                                                <?php $destinations_list_id = apply_filters('wpml_object_id', 89); ?>
                                                <a href="<?php echo get_permalink($destinations_list_id); ?>" class="btn btn--outline--blue"><?php _e("More destinations", 'friot'); ?></a>
                                            </div>
                                            </div>
                                            </div>
                                            </section>
                                            <!-- /destinations -->
                                        <?php } ?>

                                        <!-- FROM HERE GOES TO WP FOOTER -->
                                        <?php // get_template_part('boat-rental'); ?>
                                        <?php // get_template_part('offers'); ?>
                                        </div>
                                        </main><!-- NOTE: Page content wrapper ENDS here -->
                                        <?php get_footer(); ?>
