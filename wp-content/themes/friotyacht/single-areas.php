<?php get_header(); ?>
<!-- NOTE: Page content wrapper STARTS here -->
<main class="page-content-wrapper" role="main">
    <?php
    if (have_posts()) : while (have_posts()) : the_post();
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
    ?>
            <!-- NOTE: Page content -->
            <div class="container-fluid container--home">
                <section class="section section--header section--destination-header" style="background-image: url('<?php echo esc_url($featured_img_url); ?>');">
                    <div class="container">
                        <div class="row page-breadcrumb-row ml-0 mr-0">
                            <nav class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <?php
                                    $taxonomy_list_page_id = apply_filters('wpml_object_id', 89);
                                    $term_list = wp_get_post_terms(get_the_ID(), 'destinations', array('fields' => 'all'));
                                    ?>
                                    <li class="breadcrumb-item">
                                        <span><span><a href="<?php echo get_bloginfo('wpurl'); ?>"><?php _e('Home', 'friot'); ?></a> »
                                                <span><a href="<?php echo get_permalink($taxonomy_list_page_id); ?>"><?php _e('Destinations', 'friot'); ?></a> »
                                                    <span><a href="<?php echo get_term_link($term_list[0]); ?>"><?php echo $term_list[0]->name; ?></a> »
                                                        <span class="breadcrumb_last" aria-current="page"><?php the_title(); ?></span>
                                                    </span>
                                                </span>
                                            </span>
                                        </span>
                                    </li>
                                </ol>
                            </nav>
                            <div class="button-wrapper col-12 col-md-4 p-0">
                                <?php if (false) { ?><a href="<?php echo get_option('booking_site_link'); ?>" class="btn btn--red" target="_blank"><?php _e('Search charters', 'friot'); ?></a><?php } ?>
                            </div>
                        </div>
                        <div class="row m-0">
                            <div class="text-block">
                                <h1 class="banner-title"><?php the_title(); ?></h1>
                                <?php if (has_excerpt()) { ?>
                                    <div class="floating-excerpt">
                                        <p><?php the_excerpt(); ?></p>
                                        <a href="#nav-tabContent"><?php _e('Read more', 'friot'); ?></a>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="image-block">
                                <div class="image-wrapper">
                                    <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/white-wave.png" />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <!-- section counter -->

                <?php

                $fleet_link = get_post_meta(get_the_ID(), 'browse_fleet_link_text', true);
                $current_lang_code = apply_filters('wpml_current_language', null);
                $show_section = false;

                if ($fleet_link) {

                    $fleet_link = urlencode($fleet_link);
                    $fleet_list = file_get_contents("https://foglalas.friotyacht.com/api/$current_lang_code/yachts/" . $fleet_link);

                    if ($fleet_list) {
                        $fleets = json_decode($fleet_list, true);

                        if (isset($fleets)) {
                            $show_section = true;
                        }
                    }
                }
                ?>

                <?php
                // if ($show_section) {
                if (false) { ?>
                    <section class="section section--card-block bg-parallax">
                        <div class="container">

                            <div class="row section-title-row">
                                <div class="col-12 text-center">
                                    <h2 class="section-title"><?php _e('Our fleet', 'friot'); ?></h2>
                                </div>
                            </div>
                            <div class="carousel-wrapper mt-4">
                                <div class="owl-carousel card-layout-4 area-carousel">
                                    <?php foreach ($fleets as $fleet_name => $fleet_value) { ?>
                                        <div class="item">
                                            <div class="counter counter--large" style="margin-left: auto;">
                                                <div class="data text-center">
                                                    <p class="number"><?php echo $fleet_value; ?></p>
                                                    <p class="name"><?php echo $fleet_name; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                </div>
                                <div class="button-wrapper text-center mt-3">
                                    <?php if ($current_lang_code == "hu") { ?>
                                        <a href="https://foglalas.friotyacht.com/hajok/<?php echo $fleet_link ?>/any-date/all-type" class="btn btn--red" target="_blank">Foglalás</a>
                                    <?php } else { ?>
                                        <a href="https://booking.friotyacht.com/yachts/<?php echo $fleet_link ?>/any-date/all-type" class="btn btn--red" target="_blank">Booking</a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </section>
                <?php }  ?>
                <!-- /counter -->



                <!-- accordion/tab -->
                <section class="section section--card-block bg-lightblue">
                    <div class="container">
                        <!-- accordion display on small screen -->
                        <div class="d-block d-lg-none">
                            <div class="accordion accordion-flush" id="mainAccordion">
                                <?php if (!empty(get_the_content())) { ?>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="accordion-item-heading_1">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-item-collapse_1">
                                                <?php _e('Introduction', 'friot'); ?>
                                            </button>
                                        </h2>
                                        <div id="accordion-item-collapse_1" class="accordion-collapse collapse" data-bs-parent="#mainAccordion">
                                            <div class="accordion-body">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">
                                                        <?php the_post_thumbnail('areaimg'); ?>
                                                    </a>
                                                </div>
                                                <div class="text-block">
                                                    <?php the_content(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }
                                if (get_post_meta(get_the_ID(), 'things_to_do_text', true)) { ?>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="accordion-item-heading_2">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-item-collapse_2">
                                                <?php _e('Things to do', 'friot'); ?>
                                            </button>
                                        </h2>
                                        <div id="accordion-item-collapse_2" class="accordion-collapse collapse" data-bs-parent="#mainAccordion">
                                            <div class="accordion-body">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">
                                                        <?php
                                                        $things_to_do_image = get_post_meta(get_the_ID(), 'things_to_do_image', true);
                                                        $attachment_id = attachment_url_to_postid($things_to_do_image);
                                                        if ($attachment_id != 0) {
                                                            $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'areaimg')
                                                        ?>
                                                            <img src="<?php echo $attachment_image_src[0]; ?>">
                                                        <?php } else { ?>
                                                            <img src="<?php echo $things_to_do_image; ?>">
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                                <div class="accordion accordion-flush accordion-sub" id="smSubAccordion">
                                                    <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'things_to_do_text', true)); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }
                                if (get_post_meta(get_the_ID(), 'weather_text', true)) { ?>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="accordion-item-heading_3">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-item-collapse_3">
                                                <?php _e('Weather', 'friot'); ?>
                                            </button>
                                        </h2>
                                        <div id="accordion-item-collapse_3" class="accordion-collapse collapse" data-bs-parent="#mainAccordion">
                                            <div class="accordion-body">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">
                                                        <?php
                                                        $weather_image = get_post_meta(get_the_ID(), 'weather_image', true);
                                                        $attachment_id = attachment_url_to_postid($weather_image);
                                                        if ($attachment_id != 0) {
                                                            $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'areaimg')
                                                        ?>
                                                            <img src="<?php echo $attachment_image_src[0]; ?>">
                                                        <?php } else { ?>
                                                            <img src="<?php echo $weather_image; ?>">
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                                <div class="accordion accordion-flush accordion-sub" id="smSubAccordion">
                                                    <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'weather_text', true)); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }
                                if (get_post_meta(get_the_ID(), 'ports_text', true)) { ?>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="accordion-item-heading_4">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-item-collapse_4">
                                                <?php _e('Ports', 'friot'); ?>
                                            </button>
                                        </h2>
                                        <div id="accordion-item-collapse_4" class="accordion-collapse collapse" data-bs-parent="#mainAccordion">
                                            <div class="accordion-body">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">

                                                        <?php
                                                        $ports_image = get_post_meta(get_the_ID(), 'ports_image', true);
                                                        $attachment_id = attachment_url_to_postid($ports_image);
                                                        if ($attachment_id != 0) {
                                                            $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'areaimg')
                                                        ?>
                                                            <img src="<?php echo $attachment_image_src[0]; ?>">
                                                        <?php } else { ?>
                                                            <img src="<?php echo $ports_image; ?>">
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                                <div class="accordion accordion-flush accordion-sub" id="smSubAccordion">
                                                    <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'ports_text', true)); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }
                                if (get_post_meta(get_the_ID(), 'information_text', true)) { ?>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="accordion-item-heading_5">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-item-collapse_5">
                                                <?php _e('Information', 'friot'); ?>
                                            </button>
                                        </h2>
                                        <div id="accordion-item-collapse_5" class="accordion-collapse collapse" data-bs-parent="#mainAccordion">
                                            <div class="accordion-body">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">

                                                        <?php
                                                        $information_image = get_post_meta(get_the_ID(), 'information_image', true);
                                                        $attachment_id = attachment_url_to_postid($information_image);
                                                        if ($attachment_id != 0) {
                                                            $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'areaimg')
                                                        ?>
                                                            <img src="<?php echo $attachment_image_src[0]; ?>">
                                                        <?php } else { ?>
                                                            <img src="<?php echo $information_image; ?>">
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                                <div class="accordion accordion-flush accordion-sub" id="smSubAccordion">
                                                    <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'information_text', true)); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }
                                if (get_post_meta(get_the_ID(), 'itiner_text', true)) { ?>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="accordion-item-heading_6">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-item-collapse_6">
                                                <?php _e('Itinerary', 'friot'); ?>
                                            </button>
                                        </h2>
                                        <div id="accordion-item-collapse_6" class="accordion-collapse collapse" data-bs-parent="#mainAccordion">
                                            <div class="accordion-body">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">

                                                        <?php
                                                        $itiner_image = get_post_meta(get_the_ID(), 'itiner_image', true);
                                                        $attachment_id = attachment_url_to_postid($itiner_image);
                                                        if ($attachment_id != 0) {
                                                            $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'areaimg')
                                                        ?>
                                                            <img src="<?php echo $attachment_image_src[0]; ?>">
                                                        <?php } else { ?>
                                                            <img src="<?php echo $itiner_image; ?>">
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                                <div class="accordion accordion-flush accordion-sub" id="smSubAccordion">
                                                    <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'itiner_text', true)); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }
                                if (get_post_meta(get_the_ID(), 'gallery_text', true)) { ?>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="accordion-item-heading_7">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#accordion-item-collapse_7">
                                                <?php _e('Gallery', 'friot'); ?>
                                            </button>
                                        </h2>
                                        <div id="accordion-item-collapse_7" class="accordion-collapse collapse" data-bs-parent="#mainAccordion">
                                            <div class="accordion-body">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">
                                                        <?php
                                                        $gallery_image = get_post_meta(get_the_ID(), 'gallery_image', true);
                                                        $attachment_id = attachment_url_to_postid($gallery_image);
                                                        if ($attachment_id != 0) {
                                                            $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'areaimg')
                                                        ?>
                                                            <img src="<?php echo $attachment_image_src[0]; ?>">
                                                        <?php } else { ?>
                                                            <img src="<?php echo $gallery_image; ?>">
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                                <div class="accordion accordion-flush accordion-sub" id="smSubAccordion">
                                                    <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'gallery_text', true)); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="button-wrapper text-center mt-4">
                                    <a href="<?php echo get_permalink($areanquiry_id); ?>?area=<?php echo get_the_title(); ?>" class="btn btn--red"><?php _e('I would like an offer', 'friot'); ?></a>
                                </div>
                            </div>
                        </div>
                        <!-- tab view on large -->
                        <div class="d-none d-lg-block">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <?php if (!empty(get_the_content())) { ?>
                                        <button class="nav-link active" id="tab-1" data-bs-toggle="tab" data-bs-target="#tab-1-toggle" type="button" role="tab"><?php _e('Introduction', 'friot'); ?></button>
                                    <?php }
                                    if (get_post_meta(get_the_ID(), 'things_to_do_text', true)) { ?>
                                        <button class="nav-link" id="tab-2" data-bs-toggle="tab" data-bs-target="#tab-2-toggle" type="button" role="tab"><?php _e('Things to do', 'friot'); ?></button>
                                    <?php }
                                    if (get_post_meta(get_the_ID(), 'weather_text', true)) { ?>
                                        <button class="nav-link" id="tab-3" data-bs-toggle="tab" data-bs-target="#tab-3-toggle" type="button" role="tab"><?php _e('Weather', 'friot'); ?></button>
                                    <?php }
                                    if (get_post_meta(get_the_ID(), 'ports_text', true)) { ?>
                                        <button class="nav-link" id="tab-4" data-bs-toggle="tab" data-bs-target="#tab-4-toggle" type="button" role="tab"><?php _e('Ports', 'friot'); ?></button>
                                    <?php }
                                    if (get_post_meta(get_the_ID(), 'information_text', true)) { ?>
                                        <button class="nav-link" id="tab-5" data-bs-toggle="tab" data-bs-target="#tab-5-toggle" type="button" role="tab"><?php _e('Information', 'friot'); ?></button>
                                    <?php }
                                    if (get_post_meta(get_the_ID(), 'itiner_text', true)) { ?>
                                        <button class="nav-link" id="tab-6" data-bs-toggle="tab" data-bs-target="#tab-6-toggle" type="button" role="tab"><?php _e('Itinerary', 'friot'); ?></button>
                                    <?php }
                                    if (get_post_meta(get_the_ID(), 'gallery_text', true)) { ?>
                                        <button class="nav-link" id="tab-7" data-bs-toggle="tab" data-bs-target="#tab-7-toggle" type="button" role="tab"><?php _e('Gallery', 'friot'); ?></button>
                                    <?php } ?>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <?php if (!empty(get_the_content())) { ?>
                                    <div class="tab-pane fade show active" id="tab-1-toggle" role="tabpanel">
                                        <div class="row tab-row">
                                            <div class="display-card-wrapper">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">
                                                        <?php the_post_thumbnail('areaimg'); ?>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="text-block">
                                                <?php the_content(); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php }
                                if (get_post_meta(get_the_ID(), 'things_to_do_text', true)) { ?>
                                    <div class="tab-pane fade show" id="tab-2-toggle" role="tabpanel">
                                        <div class="row tab-row">
                                            <div class="display-card-wrapper">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">
                                                        <?php
                                                        $things_to_do_image = get_post_meta(get_the_ID(), 'things_to_do_image', true);
                                                        $attachment_id = attachment_url_to_postid($things_to_do_image);
                                                        if ($attachment_id != 0) {
                                                            $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'areaimg')
                                                        ?>
                                                            <img src="<?php echo $attachment_image_src[0]; ?>">
                                                        <?php } else { ?>
                                                            <img src="<?php echo $things_to_do_image; ?>">
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="text-block">
                                                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'things_to_do_text', true)); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php }
                                if (get_post_meta(get_the_ID(), 'weather_text', true)) { ?>
                                    <div class="tab-pane fade show" id="tab-3-toggle" role="tabpanel">
                                        <div class="row tab-row">
                                            <div class="display-card-wrapper">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">
                                                        <?php
                                                        $weather_image = get_post_meta(get_the_ID(), 'weather_image', true);
                                                        $attachment_id = attachment_url_to_postid($weather_image);
                                                        if ($attachment_id != 0) {
                                                            $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'areaimg')
                                                        ?>
                                                            <img src="<?php echo $attachment_image_src[0]; ?>">
                                                        <?php } else { ?>
                                                            <img src="<?php echo $weather_image; ?>">
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="text-block">
                                                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'weather_text', true)); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php }
                                if (get_post_meta(get_the_ID(), 'ports_text', true)) { ?>
                                    <div class="tab-pane fade show" id="tab-4-toggle" role="tabpanel">
                                        <div class="row tab-row">
                                            <div class="display-card-wrapper">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">
                                                        <?php
                                                        $ports_image = get_post_meta(get_the_ID(), 'ports_image', true);
                                                        $attachment_id = attachment_url_to_postid($ports_image);
                                                        if ($attachment_id != 0) {
                                                            $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'areaimg')
                                                        ?>
                                                            <img src="<?php echo $attachment_image_src[0]; ?>">
                                                        <?php } else { ?>
                                                            <img src="<?php echo $ports_image; ?>">
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="text-block">
                                                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'ports_text', true)); ?>
                                            </div>
                                        </div>

                                    </div>
                                <?php }
                                if (get_post_meta(get_the_ID(), 'information_text', true)) { ?>
                                    <div class="tab-pane fade show" id="tab-5-toggle" role="tabpanel">
                                        <div class="row tab-row">
                                            <div class="display-card-wrapper">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">
                                                        <?php
                                                        $information_image = get_post_meta(get_the_ID(), 'information_image', true);
                                                        $attachment_id = attachment_url_to_postid($information_image);
                                                        if ($attachment_id != 0) {
                                                            $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'areaimg')
                                                        ?>
                                                            <img src="<?php echo $attachment_image_src[0]; ?>">
                                                        <?php } else { ?>
                                                            <img src="<?php echo $information_image; ?>">
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="text-block">
                                                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'information_text', true)); ?>
                                            </div>
                                        </div>

                                    </div>
                                <?php }
                                if (get_post_meta(get_the_ID(), 'itiner_text', true)) { ?>
                                    <div class="tab-pane fade show" id="tab-6-toggle" role="tabpanel">
                                        <div class="row tab-row">
                                            <div class="display-card-wrapper">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">
                                                        <?php
                                                        $itiner_image = get_post_meta(get_the_ID(), 'itiner_image', true);
                                                        $attachment_id = attachment_url_to_postid($itiner_image);
                                                        if ($attachment_id != 0) {
                                                            $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'areaimg')
                                                        ?>
                                                            <img src="<?php echo $attachment_image_src[0]; ?>">
                                                        <?php } else { ?>
                                                            <img src="<?php echo $itiner_image; ?>">
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="text-block">
                                                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'itiner_text', true)); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php }
                                if (get_post_meta(get_the_ID(), 'gallery_text', true)) { ?>
                                    <div class="tab-pane fade show" id="tab-7-toggle" role="tabpanel">
                                        <div class="row tab-row">
                                            <div class="display-card-wrapper">
                                                <div class="display-card disabled-hover mb-4">
                                                    <a href="javascript:void(0);">
                                                        <?php
                                                        $gallery_image = get_post_meta(get_the_ID(), 'gallery_image', true);
                                                        $attachment_id = attachment_url_to_postid($gallery_image);
                                                        if ($attachment_id != 0) {
                                                            $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'areaimg')
                                                        ?>
                                                            <img src="<?php echo $attachment_image_src[0]; ?>">
                                                        <?php } else { ?>
                                                            <img src="<?php echo $gallery_image; ?>">
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="text-block">
                                                <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'gallery_text', true)); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="button-wrapper text-center mt-4">
                                <?php $areanquiry_id = apply_filters('wpml_object_id', 402); ?>
                                <a href="<?php echo get_permalink($areanquiry_id); ?>?area=<?php echo get_the_title(); ?>" class="btn btn--red"><?php _e('Request an offer', 'friot'); ?></a>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- accordion/tab -->


                <!-- FROM HERE GOES TO WP FOOTER -->
                <?php get_template_part('boat-rental'); ?>
            </div>
    <?php
        endwhile;
    endif;
    rewind_posts();
    ?>
</main><!-- NOTE: Page content wrapper ENDS here -->

<?php get_footer(); ?>