<?php get_header(); ?>

<!-- NOTE: Page content wrapper STARTS here -->
<main class="page-content-wrapper" role="main">
    <?php if (have_posts()) : while (have_posts()) : the_post();
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
    ?>
            <!-- NOTE: Page content -->
            <div class="container-fluid container--home">
                <section class="section section--header section--page-header" style="background-image: url('<?php echo $featured_img_url; ?>');">
                    <div class="container">
                        <div class="row page-breadcrumb-row ml-0 mr-0">
                            <nav style="--bs-breadcrumb-divider: '/';" class="breadcrumb-wrapper">
                                <ol class="breadcrumb">
                                    <?php
                                    if (function_exists('yoast_breadcrumb')) {
                                        yoast_breadcrumb('<li class="breadcrumb-item">', '</li>');
                                    }
                                    ?>
                                </ol>
                            </nav>
                            <div class="button-wrapper col-12 col-md-4 p-0">

                            </div>
                        </div>
                        <div class="row m-0">
                            <div class="text-block text-center w-100">
                                <!-- <h3 class="banner-subtitle">Crewed charter</h3> -->
                                <h1 class="banner-title"><?php the_title(); ?></h1>
                            </div>
                        </div>
                    </div>
                </section>


                <!-- section counter -->
                <section class="section section--basic-page bg-lightblue">
                    <div class="container">
                        <?php if (has_excerpt()) { ?>
                            <div class="excerpt">
                                <p class="text-center mb-4"><strong><?php the_excerpt(); ?></strong></p>
                            </div>
                        <?php } ?>
                        <?php the_content(); ?>
                    </div>
                </section>
                <!-- /counter -->

            </div>
    <?php
        endwhile;
    endif;
    ?>
</main><!-- NOTE: Page content wrapper ENDS here -->

<?php get_footer() ?>