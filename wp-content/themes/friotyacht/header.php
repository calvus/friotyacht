<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">

        <title><?php wp_title(); ?></title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Lexend:wght@300;400;500;600;700&display=swap" rel="stylesheet">

        <!-- FANCYBOX CSS-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css" />

        <!-- CSS -->
        <link rel="stylesheet" href="<?php echo get_bloginfo('wpurl'); ?>/includes/css/style.css?v=0.294">

        <!-- Favicon -->
        <link rel="shortcut icon" href="<?php echo get_bloginfo('wpurl'); ?>/includes/images/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" href="<?php echo get_bloginfo('wpurl'); ?>/includes/images/favicon.ico" type="image/x-icon">
        <meta name="msapplication-TileImage" content="<?php echo get_bloginfo('wpurl'); ?>/includes/images/favicon.ico" />


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
                <![endif]-->
        <?php wp_head(); ?>
        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-W3VXVXV');</script>
        <!-- End Google Tag Manager -->
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />
        <!-- Meta Pixel Code -->
        <script>
            !function (f, b, e, v, n, t, s)
            {
                if (f.fbq)
                    return;
                n = f.fbq = function () {
                    n.callMethod ?
                            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)
                    f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
                    'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '589160263158553');
            fbq('track', 'PageView');
        </script>
        <script>  var currentLangCode = <?php echo '"' . ICL_LANGUAGE_CODE . '"'; ?>;</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=589160263158553&ev=PageView&noscript=1"
                   /></noscript>
    <!-- End Meta Pixel Code -->
</head>


<body <?php body_class(); ?> >
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W3VXVXV"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- NOTE: IE Hide -->
    <div class="ie-container" style="display: none;">
        <div class="inner-wrapper">
            <div class="w-100">
                <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/logo-darkblue.svg" />
            </div>
            <div class="text-wrapper">
                <p class="text-heading text-center"><?php _e("A keresett oldal Internet Explorer böngészőben nem nyitható meg :(", "friot"); ?></p>
                <p class="text-heading text-center"><?php _e("Kérjük, hogy az oldalt", "friot"); ?> <a href="https://www.google.com/chrome/?brand=GGRF&utm_campaign=ww&utm_source=ww-cws-banner&utm_medium=et" target="_blank">Chrome</a>, <a href="https://www.microsoft.com/en-us/edge" target="_blank">Edge</a> <?php _e("vagy", "friot"); ?> <a href="https://www.mozilla.org/en-US/firefox/new/" target="_blank">Firefox</a> <?php _e("böngészőben szíveskedjen megnyitni!", "friot"); ?></p>
            </div>
        </div>
    </div>

    <!-- NOTE: Main container STARTS here -->
    <div class="container-fluid container--main pl-0 pr-0">

        <!-- NOTE: Navbar -->
        <header id="fixedHeader">
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container navbar-inner-wrapper">
                    <div class="nav-content-wrapper">
                        <?php
                        global $my_home_url;
                        $my_home_url = apply_filters('wpml_home_url', get_option('home'));
                        ?>
                        <a class="navbar-brand" href="<?php echo $my_home_url; ?>"><img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/logo-darkblue.svg" /></a>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navContent" aria-controls="navContent" aria-expanded="false" aria-label="Toggle navigation">
                            <div class="nav-icon">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </button>

                        <div class="collapse navbar-collapse" id="navContent">
                            <ul class="navbar-nav me-auto navbar-top">
                                <?php
                                wp_nav_menu(array(
                                    'theme_location' => 'main_menu_top',
                                    'container' => false,
                                    'menu_class' => '',
                                    'fallback_cb' => '__return_false',
                                    //'items_wrap' => '<ul id="%1$s" class="navbar-nav me-auto %2$s">%3$s</ul>',
                                    'items_wrap' => '%3$s',
                                    'depth' => 2,
                                    'walker' => new bootstrap_5_wp_nav_menu_walker()
                                ));
                                ?>
                                <li class="nav-item phone-number">
                                    <?php if (get_locale() === 'hu_HU') { ?>
                                        <a href="tel:+36703806576" class="nav-link">
                                            <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/phone.svg" /> +36 70 380 6576
                                        </a>
                                    <?php } else { ?>
                                        <a href="tel:+442032902487" class="nav-link">
                                            <img src="<?php echo get_bloginfo('wpurl'); ?>/includes/images/phone.svg" /> +44 20 3290 2487
                                        </a>
                                    <?php } ?>
                                </li>
                            </ul>
                            <ul class="navbar-nav me-auto" id="mobileMenu">
                                <?php
                                wp_nav_menu(array(
                                    'theme_location' => 'main_menu_bottom',
                                    'container' => false,
                                    'menu_class' => '',
                                    'fallback_cb' => '__return_false',
                                    //'items_wrap' => '<ul id="%1$s" class="navbar-nav me-auto %2$s">%3$s</ul>',
                                    'items_wrap' => '%3$s',
                                    'depth' => 2,
                                    'walker' => new bootstrap_5_wp_nav_menu_walker()
                                ));
                                ?>

                                <?php
                                wp_nav_menu(array(
                                    'theme_location' => 'main_menu_bottom_2',
                                    'container' => false,
                                    'menu_class' => '',
                                    'fallback_cb' => '__return_false',
                                    //'items_wrap' => '<ul id="%1$s" class="navbar-nav me-auto %2$s">%3$s</ul>',
                                    'items_wrap' => '%3$s',
                                    'depth' => 2,
                                    'walker' => new bootstrap_5_wp_nav_menu_walker()
                                ));
                                ?>

                            </ul>

                        </div>
                    </div>
                </div>
            </nav>
        </header>
        <div class="sticky-button">
            <?php
            $konzultacio_page_id = apply_filters('wpml_object_id', 3558); //3558
            if (!is_page($konzultacio_page_id)) :
                ?>
                <a href="<?php echo get_permalink($konzultacio_page_id); ?>" class="btn btn--red" style="padding: 14px 40px 14px 28px;">
                    <img src="<?php bloginfo('wpurl'); ?>/includes/images/tap.png" style="width:32px; margin-right:8px;" />
                    <?php _e("Kérj ingyenes konzultációt!", 'friot'); ?>
                </a>
            <?php endif; ?>
        </div>