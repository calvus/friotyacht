<?php
/*
  Plugin Name: Friot Yacht newsletter plugin
 * Plugin URI: 
 * Description: 
 * Version: 0.1
 * Author: Calv.us
 */

add_action('init', 'check_db_version');
add_action('wp_ajax_subscribe_to_newsletter', 'subscribe_to_newsletter');
add_action('wp_ajax_nopriv_subscribe_to_newsletter', 'subscribe_to_newsletter');

//add_action('wp_enqueue_scripts', 'friotyacht_script_enqueue');
add_action('wp_head', 'hook_javascript');

function friotyacht_script_enqueue($pagehook) {
    wp_enqueue_script('friotyacht_newsletter-js', plugin_dir_url(__FILE__) . '/js/friotyacht_newsletter-js.js?v=0.1', array('jquery', 'wp-i18n'), '0.0.1');
    wp_set_script_translations('friotyacht_newsletter-js', 'friotyacht');
}

function check_db_version() {

    global $wpdb;

    $sql = "CREATE TABLE friotyacht_newsletter (
              newsletter_id bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
              newsletter_email varchar(255) NOT NULL,
              newsletter_name text NOT NULL,
              newsletter_lang varchar(2) NOT NULL,
              newsletter_subscribe_date datetime NOT NULL,
              newsletter_email_sent varchar(1) NOT NULL,
               PRIMARY KEY (`newsletter_id`), 
               UNIQUE KEY newsletter_email (newsletter_email)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
           ";

    $db_version = md5($sql);

    if (get_option('friotyacht_plugin_db_version', '') !== $db_version) {
        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        dbDelta($sql);

        add_option('friotyacht_plugin_db_version', $db_version);
    }
}

function newsletter_subscribe_show_form() {
    ?>
    <form class="woocommerce-form " method="post" name="subscribe_to_newsletter_form" id="subscribe_to_newsletter_form">
        <input type="hidden" name="newsletter_lang" id="newsletter_lang" value="<?php echo ICL_LANGUAGE_CODE; ?>">
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
            <input type="text" class="form-control form-control-sm footerTextArea subscriberName" name="newsletter_name" id="newsletter_name" required="required" placeholder="<?php _e('Enter your name...', 'friotyacht-newsletter'); ?>">
        </p>
        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
            <input type="email" cols="40" class="form-control form-control-sm footerTextArea mt-3 subscriberEmail"  name="newsletter_email" id="newsletter_email"  required="required" placeholder="<?php _e('Enter your email address...', 'friotyacht-newsletter'); ?>">
        </p>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="adatkezelesi_tajekoztato" name="adatkezelesi_tajekoztato" required="required"/>
            <label class="form-check-label footerText mb-3" for="adatkezelesi_tajekoztato">
                <?php _e('I accept the', 'friotyacht-newsletter'); ?> <a href="<?php echo get_privacy_policy_url(); ?>" target="_blank" class="colorRed"><?php _e('Privacy policy', 'friotyacht-newsletter'); ?></a><?php _e("et", "friot"); ?>
            </label>
        </div>
        <input type="submit" class="btn btn--red newsletterSubmit mb-4" style="white-space: break-spaces;height: max-content;" name="subscribe_to_newsletter" id="subscribe_to_newsletter" value="<?php _e('Join', 'friotyacht-newsletter'); ?>">
        <script type="text/javascript" language="javascript" >
            var ajax_nonce = "<?php echo wp_create_nonce("subscribe-to-newsletter-security"); ?>";
        </script>
        <?php wp_nonce_field('friotyachtsubscribetonewsletter', 'friotyachtsubscribetonewslettercheck');
        ?>
    </form>
    <script>
        function hideerrors() {
            errortext = "";
            noerror = true;
            jQuery(".validation-advice").remove();
            jQuery(".alert").removeClass("alert");
            jQuery(".validation-failed").removeClass("validation-failed");
            jQuery("#alertext").html("");
            jQuery(".alertbox").html("");
            jQuery(".alertbox").hide();
        }
        function showerrors(errordiv) {
            jQuery("#" + errordiv).html(errortext);
            jQuery("#" + errordiv + ".error").show();
        }
        function adderror(id, text) {
            jQuery("#" + id).addClass("alert validation-failed");
            jQuery("#" + id).after('<div class="validation-advice"> ' + text + "</div>");
            noerror = false;
        }
        function adderror2(id, text) {
            jQuery("#" + id).addClass("alert");
            jQuery("label[for='" + id + "']").addClass("alert");
            jQuery("#" + id)
                    .parent()
                    .append('<div class="validation-advice">' + text + " </div>");

            //jQuery("#"+id).val(text);
            //errortext+="<p>"+text+"</p>";
            noerror = false;
        }
        var noerror = true;

        jQuery(document).ready(function () {
            jQuery("#subscribe_to_newsletter").on("click", function (e) {
                e.preventDefault();
                hideerrors();
                //console.log(ajax_nonce);
                var newsletter_name = jQuery("#newsletter_name").val();
                var newsletter_lang = jQuery("#newsletter_lang").val();
                var newsletter_email = jQuery("#newsletter_email").val();
                // var adatkezelesi_tajekoztato = jQuery("#adatkezelesi_tajekoztato").is(":checked");

                //console.log(newsletter_name, newsletter_email);

                if (newsletter_name == "") {
                    adderror("newsletter_name", "<?php _e("Please fill in the name field!", "friotyacht"); ?>");
                }
                if (newsletter_email == "") {
                    adderror("newsletter_email", "<?php _e("Please fill in the email address field!", "friotyacht"); ?>");
                }
                if (!jQuery("#adatkezelesi_tajekoztato").is(":checked")) {
                    adderror2("adatkezelesi_tajekoztato", "<?php _e("Please accept the privacy policy!", "friotyacht"); ?>");
                }
                if (noerror) {
                    document.getElementById("subscribe_to_newsletter").disabled = true;
                    jQuery.ajax({
                        url: ajaxurl,
                        type: "post",

                        data: {
                            action: "subscribe_to_newsletter",
                            newsletter_name: newsletter_name,
                            newsletter_lang: newsletter_lang,
                            newsletter_email: newsletter_email,
                            security: ajax_nonce,
                        },
                        cache: false,
                        success: function (msg) {
                            var ret = JSON.parse(msg);
                            console.log(ret);
                            if (ret.error) {
                                adderror("subscribe_to_newsletter", ret.error);
                                document.getElementById("subscribe_to_newsletter").disabled = false;
                            } else {
                                // var subscriptionResponse = 'Sikeres feliratkozás!';
                                jQuery("#subscribe_to_newsletter_form").html(
                                        "<h5>" + ret.success + "</h5>"
                                        );
                            }
                        },
                        error: function () {
                            document.getElementById("subscribe_to_newsletter").disabled = false;
                        },
                    });
                }
            });
        });
    </script>
    <?php
}

function subscribe_to_newsletter() {
    check_ajax_referer('subscribe-to-newsletter-security', 'security');
    global $wpdb;

    $reqired_fields = ['newsletter_name', 'newsletter_email'];
    foreach ($reqired_fields as $reqired_field) {
        if (empty($_POST[$reqired_field])) {
            $message['error'] = __("Error, required field is missing!", 'friotyacht');
            echo json_encode($message);
            die();
        }
    }



    if (!is_email($_REQUEST['newsletter_email'])) {
        $message['error'] = __("Error, we were unable to subscribe to the newsletter!", 'friotyacht');
    } else {
        $newsletter_email = sanitize_email($_REQUEST['newsletter_email']);
        $newsletter_name = sanitize_text_field($_REQUEST['newsletter_name']);
        $newsletter_lang = sanitize_text_field($_REQUEST['newsletter_lang']);
        $newsletter_subscribe_date = date("Y-m-d H:i:s");
        $newsletter_email_sent = 0;

        unset($message, $vanemail);

        $vanemail = $wpdb->get_var("SELECT COUNT(newsletter_id) FROM friotyacht_newsletter WHERE newsletter_email='" . $newsletter_email . "' ");
        if ($vanemail != 0) {
            $message['error'] = __("You have already subscribed to this newsletter with this email address!", 'friotyacht');
        } else {
            if ($wpdb->insert(
                            'friotyacht_newsletter',
                            array(
                                'newsletter_email' => $newsletter_email,
                                'newsletter_name' => $newsletter_name,
                                'newsletter_lang' => $newsletter_lang,
                                'newsletter_subscribe_date' => $newsletter_subscribe_date,
                                'newsletter_email_sent' => $newsletter_email_sent,
                            ),
                            array(
                                '%s',
                                '%s',
                                '%s',
                                '%s',
                                '%d'
                            )
                    ) != false) {
                $api_key = get_option('mailchimp_apikey');
                $list_id = get_option('mailchimp_list_id');

                $result = rudr_mailchimp_subscriber_status($newsletter_email, 'subscribed', $list_id, $api_key, array('FNAME' => $newsletter_name));

                $message['success'] = __("Successful newsletter subscribtion!", 'friotyacht');
            } else {
                $message['error'] = __("Error, we were unable to subscribe to the newsletter!", 'friotyacht');
            }
        }
    }
    echo json_encode($message);
    die();
}

function hook_javascript() {
    ?>
    <script type="text/javascript">
        if (typeof ajaxurl === 'undefined' || ajaxurl === null) {
            var ajaxurl = "<?php echo admin_url('admin-ajax.php') ?>";
        }
    </script>
    <?php
}

function rudr_mailchimp_subscriber_status($email, $status, $list_id, $api_key, $merge_fields = array('FNAME' => '', 'LNAME' => '')) {

    $data = array(
        'apikey' => $api_key,
        'email_address' => $email,
        'status' => $status,
        'merge_fields' => $merge_fields
    );
    $mch_api = curl_init(); // initialize cURL connection

    curl_setopt($mch_api, CURLOPT_URL, 'https://' . substr($api_key, strpos($api_key, '-') + 1) . '.api.mailchimp.com/3.0/lists/' . $list_id . '/members/' . md5(strtolower($data['email_address'])));
    curl_setopt($mch_api, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic ' . base64_encode('user:' . $api_key)));
    curl_setopt($mch_api, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
    curl_setopt($mch_api, CURLOPT_RETURNTRANSFER, true); // return the API response
    curl_setopt($mch_api, CURLOPT_CUSTOMREQUEST, 'PUT'); // method PUT
    curl_setopt($mch_api, CURLOPT_TIMEOUT, 10);
    curl_setopt($mch_api, CURLOPT_POST, true);
    curl_setopt($mch_api, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($mch_api, CURLOPT_POSTFIELDS, json_encode($data)); // send data in json

    $result = curl_exec($mch_api);
    /* var_dump($result);
      die(); */
    return $result;
}
