const {__, _x, _n, _nx} = wp.i18n;
function hideerrors() {
    errortext = "";
    noerror = true;
    jQuery(".validation-advice").remove();
    jQuery(".alert").removeClass("alert");
    jQuery(".validation-failed").removeClass("validation-failed");
    jQuery("#alertext").html("");
    jQuery(".alertbox").html("");
    jQuery(".alertbox").hide();
}
function showerrors(errordiv) {
    jQuery("#" + errordiv).html(errortext);
    jQuery("#" + errordiv + ".error").show();
}
function adderror(id, text) {
    jQuery("#" + id).addClass("alert validation-failed");
    jQuery("#" + id).after('<div class="validation-advice"> ' + text + "</div>");
    noerror = false;
}
function adderror2(id, text) {
    jQuery("#" + id).addClass("alert");
    jQuery("label[for='" + id + "']").addClass("alert");
    jQuery("#" + id)
            .parent()
            .append('<div class="validation-advice">' + text + " </div>");

    //jQuery("#"+id).val(text);
    //errortext+="<p>"+text+"</p>";
    noerror = false;
}
var noerror = true;

jQuery(document).ready(function () {
    jQuery("#subscribe_to_newsletter").on("click", function (e) {
        e.preventDefault();
        hideerrors();
        //console.log(ajax_nonce);
        var newsletter_name = jQuery("#newsletter_name").val();
        var newsletter_lang = jQuery("#newsletter_lang").val();
        var newsletter_email = jQuery("#newsletter_email").val();
        // var adatkezelesi_tajekoztato = jQuery("#adatkezelesi_tajekoztato").is(":checked");

        //console.log(newsletter_name, newsletter_email);

        if (newsletter_name == "") {
            adderror("newsletter_name", __("Please fill in the name field!", "friotyacht"));
        }
        if (newsletter_email == "") {
            adderror("newsletter_email", __("Please fill in the email address field!", "friotyacht"));
        }
        if (!jQuery("#adatkezelesi_tajekoztato").is(":checked")) {
            adderror2(
                    "adatkezelesi_tajekoztato",
                    __("Please accept the privacy policy!", "friotyacht")
                    );
        }
        if (noerror) {
            document.getElementById("subscribe_to_newsletter").disabled = true;
            jQuery.ajax({
                url: ajaxurl,
                type: "post",

                data: {
                    action: "subscribe_to_newsletter",
                    newsletter_name: newsletter_name,
                    newsletter_lang: newsletter_lang,
                    newsletter_email: newsletter_email,
                    security: ajax_nonce,
                },
                cache: false,
                success: function (msg) {
                    var ret = JSON.parse(msg);
                    console.log(ret);
                    if (ret.error) {
                        adderror("subscribe_to_newsletter", ret.error);
                        document.getElementById("subscribe_to_newsletter").disabled = false;
                    } else {
                        // var subscriptionResponse = 'Sikeres feliratkozás!';
                        jQuery("#subscribe_to_newsletter_form").html(
                                "<h5>" + ret.success + "</h5>"
                                );
                    }
                },
                error: function () {
                    document.getElementById("subscribe_to_newsletter").disabled = false;
                },
            });
        }
    });
});
