<?php

add_action('init', 'add_friotyacht_custom_post_types');

function add_friotyacht_custom_post_types() {

    //---- CUSTOM POST TYPE TEMPLATE
    //---- Főoldal - részletek
    register_post_type('homepage', array(
        'labels' => array(
            'name' => __('Home page - contents', 'friotyacht'),
            'singular_name' => __('Home page', 'friotyacht'),
            'add_new' => __('Add new', 'friotyacht'),
            'add_new_item' => __('Add new content', 'friotyacht'),
            'edit_item' => __('Edit content', 'friotyacht'),
            'new_item' => __('New', 'friotyacht'),
            'view_item' => __('View content', 'friotyacht'),
            'search_items' => __('Search content', 'friotyacht'),
            'not_found' => __('Nothing Found', 'friotyacht'),
            'not_found_in_trash' => __('Nothing Found', 'friotyacht')
        ),
        'public' => true,
        'publicly_queryable' => true,
        'menu_position' => 3,
        'menu_icon' => '',
        'has_archive' => false,
        'rewrite' => true,
        'capability_type' => 'post',
        // 'taxonomies' => array('post_tag'),
        'hierarchical' => false,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'page-attributes', 'revisions')
            )
    );

    register_post_type('areas', array(
        'labels' => array(
            'name' => __('Areas', 'friotyacht'),
            'singular_name' => __('Area', 'friotyacht'),
            'add_new' => __('Add new', 'friotyacht'),
            'add_new_item' => __('Add new  Area', 'friotyacht'),
            'edit_item' => __('Edit Area', 'friotyacht'),
            'new_item' => __('New Area', 'friotyacht'),
            'view_item' => __('View Area', 'friotyacht'),
            'search_items' => __('Search Area', 'friotyacht'),
            'not_found' => __('Nothing found', 'friotyacht'),
            'not_found_in_trash' => __('Nothing found in trash', 'friotyacht')
        ),
        'public' => true,
        'publicly_queryable' => true,
        //'show_in_rest' => true,
        'menu_position' => 3,
        'menu_icon' => '',
        'has_archive' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        // 'taxonomies' => array('post_tag'),
        'hierarchical' => false,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'page-attributes', 'revisions')
            )
    );
    register_post_type('ships', array(
        'labels' => array(
            'name' => __('Ships', 'friotyacht'),
            'singular_name' => __('Ship', 'friotyacht'),
            'add_new' => __('Add new', 'friotyacht'),
            'add_new_item' => __('Add new  Ship', 'friotyacht'),
            'edit_item' => __('Edit Ship', 'friotyacht'),
            'new_item' => __('New Ship', 'friotyacht'),
            'view_item' => __('View Ship', 'friotyacht'),
            'search_items' => __('Search Ship', 'friotyacht'),
            'not_found' => __('Nothing found', 'friotyacht'),
            'not_found_in_trash' => __('Nothing found in trash', 'friotyacht')
        ),
        'public' => true,
        'publicly_queryable' => true,
        'menu_position' => 4,
        'menu_icon' => '',
        'has_archive' => false,
        'rewrite' => true,
        'capability_type' => 'post',
        // 'taxonomies' => array('post_tag'),
        'hierarchical' => false,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'page-attributes', 'revisions')
            )
    );
    register_post_type('bookings', array(
        'labels' => array(
            'name' => __('Bookings', 'friotyacht'),
            'singular_name' => __('Booking', 'friotyacht'),
            'add_new' => __('Add new', 'friotyacht'),
            'add_new_item' => __('Add new  Booking', 'friotyacht'),
            'edit_item' => __('Edit Booking', 'friotyacht'),
            'new_item' => __('New Booking', 'friotyacht'),
            'view_item' => __('View Booking', 'friotyacht'),
            'search_items' => __('Search Booking', 'friotyacht'),
            'not_found' => __('Nothing found', 'friotyacht'),
            'not_found_in_trash' => __('Nothing found in trash', 'friotyacht')
        ),
        'public' => true,
        'exclude_from_search' => true,
        'publicly_queryable' => false,
        'menu_position' => 4,
        'menu_icon' => '',
        'has_archive' => false,
        'rewrite' => true,
        'capability_type' => 'post',
        // 'taxonomies' => array('post_tag'),
        'hierarchical' => false,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'page-attributes', 'revisions')
            )
    );

    register_post_type('enquiries', array(
        'labels' => array(
            'name' => __('Enquiries', 'friotyacht'),
            'singular_name' => __('Enquiry', 'friotyacht'),
            'add_new' => __('Add new', 'friotyacht'),
            'add_new_item' => __('Add new  Enquiry', 'friotyacht'),
            'edit_item' => __('Edit Enquiry', 'friotyacht'),
            'new_item' => __('New Enquiry', 'friotyacht'),
            'view_item' => __('View Enquiry', 'friotyacht'),
            'search_items' => __('Search Enquiry', 'friotyacht'),
            'not_found' => __('Nothing found', 'friotyacht'),
            'not_found_in_trash' => __('Nothing found in trash', 'friotyacht')
        ),
        'public' => true,
        'exclude_from_search' => true,
        'publicly_queryable' => false,
        'menu_position' => 4,
        'menu_icon' => '',
        'has_archive' => false,
        'rewrite' => true,
        'capability_type' => 'post',
        // 'taxonomies' => array('post_tag'),
        'hierarchical' => false,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'page-attributes', 'revisions')
            )
    );

    register_post_type('crewed-charter', array(
        'labels' => array(
            'name' => __('Rendezv/Kapit', 'friotyacht'),
            'singular_name' => __('Rendezvények/Kapitánnyal', 'friotyacht'),
            'add_new' => __('Add new', 'friotyacht'),
            'add_new_item' => __('Add new  Rendezvények/Kapitánnyal', 'friotyacht'),
            'edit_item' => __('Edit Rendezvények/Kapitánnyal ', 'friotyacht'),
            'new_item' => __('New Rendezvények/Kapitánnyal', 'friotyacht'),
            'view_item' => __('View Rendezvények/Kapitánnyal', 'friotyacht'),
            'search_items' => __('Search Rendezvények/Kapitánnyal', 'friotyacht'),
            'not_found' => __('Nothing found', 'friotyacht'),
            'not_found_in_trash' => __('Nothing found in trash', 'friotyacht')
        ),
        'public' => true,
        'publicly_queryable' => true,
        'menu_position' => 6,
        'menu_icon' => '',
        'has_archive' => false,
        'rewrite' => true,
        'capability_type' => 'post',
        // 'taxonomies' => array('post_tag'),
        'hierarchical' => false,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'page-attributes', 'revisions')
            )
    );

    register_post_type('cabin-charter', array(
        'labels' => array(
            'name' => __('Cabin charter', 'friotyacht'),
            'singular_name' => __('Cabin charter', 'friotyacht'),
            'add_new' => __('Add new', 'friotyacht'),
            'add_new_item' => __('Add new  Cabin charter', 'friotyacht'),
            'edit_item' => __('Edit Cabin charter ', 'friotyacht'),
            'new_item' => __('New Cabin charter', 'friotyacht'),
            'view_item' => __('View Cabin charter', 'friotyacht'),
            'search_items' => __('Search Cabin charter', 'friotyacht'),
            'not_found' => __('Nothing found', 'friotyacht'),
            'not_found_in_trash' => __('Nothing found in trash', 'friotyacht')
        ),
        'public' => true,
        'publicly_queryable' => true,
        'show_in_rest' => true,
        'menu_position' => 3,
        'menu_icon' => '',
        'has_archive' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        // 'taxonomies' => array('post_tag'),
        'hierarchical' => false,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'page-attributes', 'revisions')
            )
    );

    register_post_type('cabin-charter_days', array(
        'labels' => array(
            'name' => __('Túra napok', 'friotyacht'),
            'singular_name' => __('Túra nap', 'friotyacht'),
            'add_new' => __('Új', 'friotyacht'),
            'add_new_item' => __('Új  Túra nap', 'friotyacht'),
            'edit_item' => __('Szerkeztése Túra nap', 'friotyacht'),
            'new_item' => __('Új Túra nap', 'friotyacht'),
            'view_item' => __('View Túra nap', 'friotyacht'),
            'search_items' => __('Search Túra nap', 'friotyacht'),
            'not_found' => __('Nothing found', 'friotyacht'),
            'not_found_in_trash' => __('Nothing found in trash', 'friotyacht')
        ),
        'public' => true,
        'publicly_queryable' => false,
        'menu_position' => 5,
        'menu_icon' => '',
        'has_archive' => false,
        'rewrite' => false,
        'capability_type' => 'post',
        // 'taxonomies' => array('post_tag'),
        'hierarchical' => false,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'page-attributes', 'revisions')
            )
    );

    register_post_type('turabeszamolok', array(
        'labels' => array(
            'name' => __('Túrabeszámolók', 'friotyacht'),
            'singular_name' => __('Túrabeszámoló', 'friotyacht'),
            'add_new' => __('Add new', 'friotyacht'),
            'add_new_item' => __('Add new  Túrabeszámoló', 'friotyacht'),
            'edit_item' => __('Edit Túrabeszámoló ', 'friotyacht'),
            'new_item' => __('New Túrabeszámoló', 'friotyacht'),
            'view_item' => __('View Túrabeszámoló', 'friotyacht'),
            'search_items' => __('Search Túrabeszámoló', 'friotyacht'),
            'not_found' => __('Nothing found', 'friotyacht'),
            'not_found_in_trash' => __('Nothing found in trash', 'friotyacht')
        ),
        'public' => true,
        'publicly_queryable' => true,
        'menu_position' => 6,
        'menu_icon' => '',
        'has_archive' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        // 'taxonomies' => array('post_tag'),
        'hierarchical' => false,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'page-attributes', 'revisions')
            )
    );

    register_post_type('tour-report-days', array(
        'labels' => array(
            'name' => __('Túrabeszámoló napok', 'friotyacht'),
            'singular_name' => __('Túrabeszámoló nap', 'friotyacht'),
            'add_new' => __('Add new', 'friotyacht'),
            'add_new_item' => __('Add new Túrabeszámoló nap', 'friotyacht'),
            'edit_item' => __('Edit Túrabeszámoló nap', 'friotyacht'),
            'new_item' => __('New Túrabeszámoló nap', 'friotyacht'),
            'view_item' => __('View Túrabeszámoló nap', 'fśiotyacht'),
            'search_items' => __('Search Túrabeszámoló nap', 'friotyacht'),
            'not_found' => __('Nothing found', 'friotyacht'),
            'not_found_in_trash' => __('Nothing found in trash', 'friotyacht')
        ),
        'public' => true,
        'publicly_queryable' => false,
        'menu_position' => 6,
        'menu_icon' => '',
        'has_archive' => false,
        'rewrite' => false,
        'capability_type' => 'post',
        'hierarchical' => false,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'page-attributes', 'revisions')
            )
    );

    register_post_type('partners', array(
        'labels' => array(
            'name' => __('Partners', 'friotyacht'),
            'singular_name' => __('Partner', 'friotyacht'),
            'add_new' => __('Add new', 'friotyacht'),
            'add_new_item' => __('Add new  Partner', 'friotyacht'),
            'edit_item' => __('Edit Partner', 'friotyacht'),
            'new_item' => __('New Partner', 'friotyacht'),
            'view_item' => __('View Partner', 'friotyacht'),
            'search_items' => __('Search Partner', 'friotyacht'),
            'not_found' => __('Nothing found', 'friotyacht'),
            'not_found_in_trash' => __('Nothing found in trash', 'friotyacht')
        ),
        'public' => true,
        'publicly_queryable' => true,
        'menu_position' => 7,
        'menu_icon' => '',
        'has_archive' => false,
        'rewrite' => true,
        'capability_type' => 'post',
        // 'taxonomies' => array('post_tag'),
        'hierarchical' => false,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'page-attributes', 'revisions')
            )
    );

    register_post_type('boatrentals-links', array(
        'labels' => array(
            'name' => __('Boatrentals', 'friotyacht'),
            'singular_name' => __('Boatrental type', 'friotyacht'),
            'add_new' => __('Add new', 'friotyacht'),
            'add_new_item' => __('Add new  Boatrental type', 'friotyacht'),
            'edit_item' => __('Edit Boatrental type', 'friotyacht'),
            'new_item' => __('New Boatrental type', 'friotyacht'),
            'view_item' => __('View Boatrental type', 'friotyacht'),
            'search_items' => __('Search Boatrental type', 'friotyacht'),
            'not_found' => __('Nothing found', 'friotyacht'),
            'not_found_in_trash' => __('Nothing found in trash', 'friotyacht')
        ),
        'public' => true,
        'publicly_queryable' => true,
        'menu_position' => 8,
        'menu_icon' => '',
        'has_archive' => false,
        'rewrite' => true,
        'capability_type' => 'post',
        // 'taxonomies' => array('post_tag'),
        'hierarchical' => false,
        'supports' => array('title', 'thumbnail', 'excerpt', 'revisions')
            )
    );

    register_post_type('testimonial', array(
        'labels' => array(
            'name' => __('Testimonial', 'friotyacht'),
            'singular_name' => __('Testimonial', 'friotyacht'),
            'add_new' => __('Add new', 'friotyacht'),
            'add_new_item' => __('Add new Testimonial', 'friotyacht'),
            'edit_item' => __('Edit Testimonial', 'friotyacht'),
            'new_item' => __('New Testimonial', 'friotyacht'),
            'view_item' => __('View Testimonial', 'fśiotyacht'),
            'search_items' => __('Search Testimonial', 'friotyacht'),
            'not_found' => __('Nothing found', 'friotyacht'),
            'not_found_in_trash' => __('Nothing found in trash', 'friotyacht')
        ),
        'public' => true,
        'publicly_queryable' => false,
        'menu_position' => 6,
        'menu_icon' => '',
        'has_archive' => false,
        'rewrite' => false,
        'capability_type' => 'post',
        'hierarchical' => false,
        'supports' => array('title', 'editor', 'author',  'excerpt', 'page-attributes', 'revisions')
            )
    );

    register_post_type('campaigns', array(
        'labels' => array(
            'name' => __('Kampány lead elemek', 'friotyacht'),
            'singular_name' => __('elem', 'friotyacht'),
            'add_new' => __('Add new', 'friotyacht'),
            'add_new_item' => __('Add new  elem', 'friotyacht'),
            'edit_item' => __('Edit elem', 'friotyacht'),
            'new_item' => __('New elem', 'friotyacht'),
            'view_item' => __('View elem', 'friotyacht'),
            'search_items' => __('Search elem', 'friotyacht'),
            'not_found' => __('Nothing found', 'friotyacht'),
            'not_found_in_trash' => __('Nothing found in trash', 'friotyacht')
        ),
        'public' => true,
        'publicly_queryable' => true,
        'menu_position' => 8,
        'menu_icon' => '',
        'has_archive' => false,
        'rewrite' => true,
        'capability_type' => 'post',
        // 'taxonomies' => array('post_tag'),
        'hierarchical' => false,
        'supports' => array('title', 'thumbnail', 'editor', 'revisions')
            )
    );

    //--- TAXONOMY TEMPLATE
    register_taxonomy(
            'destinations', 'areas', array(
        'hierarchical' => true,
        'labels' => array(
            'name' => _x('Destinations', 'taxonomy general name', 'friotyacht'),
            'singular_name' => _x('Destination', 'taxonomy singular name', 'friotyacht'),
            'search_items' => __('Search Destination', 'friotyacht'),
            'all_items' => __('All Destinations', 'friotyacht'),
            'parent_item' => __('Parent Destination', 'friotyacht'),
            'parent_item_colon' => __('Parent Destination:', 'friotyacht'),
            'edit_item' => __('Edit Destination', 'friotyacht'),
            'update_item' => __('Update Destination', 'friotyacht'),
            'add_new_item' => __('Add New Destination', 'friotyacht'),
            'new_item_name' => __('New Destination Name', 'friotyacht'),
            'menu_name' => __('Destinations', 'friotyacht'),
            'name_field_description' => __('Date Range', 'friotyacht'),
            'desc_field_description' => __('Price', 'friotyacht'),
        ),
        'public' => true,
        'query_var' => true,
        'show_in_rest' => true,
        'rewrite' => array('slug' => 'destinations'),
        'capability_type' => 'post',
            )
    );



    //--- TAXONOMY TEMPLATE
    /*  register_taxonomy(
      'date', array('ships', 'cabin-charter'),
      array(
      'hierarchical' => true,
      'labels' => array(
      'name' => _x('Dates', 'taxonomy general name', 'friotyacht'),
      'singular_name' => _x('Date', 'taxonomy singular name', 'friotyacht'),
      'search_items' => __('Search Dates', 'friotyacht'),
      'all_items' => __('All Dates', 'friotyacht'),
      'parent_item' => __('Parent Date', 'friotyacht'),
      'parent_item_colon' => __('Parent Date:', 'friotyacht'),
      'edit_item' => __('Edit Date', 'friotyacht'),
      'update_item' => __('Update Date', 'friotyacht'),
      'add_new_item' => __('Add New Date', 'friotyacht'),
      'new_item_name' => __('New Date Name', 'friotyacht'),
      'menu_name' => __('Date', 'friotyacht'),
      'name_field_description' => __('Date Range', 'friotyacht'),
      'desc_field_description' => __('Price', 'friotyacht'),
      ),
      'public' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'date'),
      'capability_type' => 'post',
      )
      );
     */


    //register_taxonomy('special_tags', 'product', array('hierarchical' => true, 'label' => 'Kiemelés', 'query_var' => true, 'rewrite' => array('slug' => 'specialis')));
}

?>
