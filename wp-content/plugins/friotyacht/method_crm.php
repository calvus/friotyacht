<?php

function send_method_customer($originalData) {
  if (!getenv('METHOD_API_KEY')) {
    return;
  }

  $standardFields = [
    'Notes',
    'FirstName',
    'LastName',
    'Name',
    'Email',
    'Phone',
  ];

  $data = [];

  foreach ($originalData as $key => $value) {
    if (in_array($key, $standardFields)) {
      $data[$key] = $value;
    } else {
      $data['Notes'] .= "\n" . $key . ': ' . $value;
    }
  }

  $mch_api = curl_init(); // initialize cURL connection
  
  curl_setopt($mch_api, CURLOPT_URL, 'https://rest.method.me/api/v1/tables/Customer');
  curl_setopt($mch_api, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: APIKey ' . getenv('METHOD_API_KEY') ));
  curl_setopt($mch_api, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
  curl_setopt($mch_api, CURLOPT_RETURNTRANSFER, true); // return the API response
  // curl_setopt($mch_api, CURLOPT_CUSTOMREQUEST, 'PUT'); // method PUT
  curl_setopt($mch_api, CURLOPT_TIMEOUT, 10);
  curl_setopt($mch_api, CURLOPT_POST, true);
  curl_setopt($mch_api, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($mch_api, CURLOPT_POSTFIELDS, json_encode($data)); // send data in json
  
  $result = curl_exec($mch_api);
  
  return $result;
}

function wpcf7_to_method( $cf7, &$abort, $submission ) {
  // Get the form data
  $data = $submission->get_posted_data();
  $methodData = [];

  foreach ([
    'first-name' => 'FirstName',
    'last-name' => 'LastName',
    'your-email' => 'Email',
    'phone' => 'Phone',
    'other_information' => 'Notes',
    'method-tag' => 'TagList'
  ] as $cfField => $mField) {
    if (isset($data[$cfField])) {
      $methodData[$mField] = $data[$cfField];
    }
  }

  if (count($methodData)) {
    $methodData['Name'] = $methodData['FirstName'] . ' ' . $methodData['LastName'];
    send_method_customer($methodData);
  }
}

add_action( 'wpcf7_before_send_mail', 'wpcf7_to_method', 10, 3 );