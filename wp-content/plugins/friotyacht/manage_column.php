<?php

function sortArrayByArray(Array $array, Array $orderArray) {
    $ordered = array();
    foreach ($orderArray as $key) {
        if (array_key_exists($key, $array)) {
            $ordered[$key] = $array[$key];
            unset($array[$key]);
        }
    }
    return $ordered + $array;
}

add_action("manage_slider_posts_custom_column", "slider_custom_columns");
add_filter("manage_edit-slider_columns", "slider_edit_columns");

function slider_edit_columns($columns) {
    $new_columns = array(
        "title" => __('Slide címe', 'friotyacht'),
        "thumbnail" => __('Kép', 'friotyacht'),
    );

    $orderarray = array("cb", "title", "thumbnail", "date");
    $mergedarray = array_merge($columns, $new_columns);
    return sortArrayByArray($mergedarray, $orderarray);
}

function slider_custom_columns($column) {
    global $post;

    switch ($column) {
        case "thumbnail":
            echo "<center>" . the_post_thumbnail('thumbnail') . "</center>";
            break;
    }
}

// Cabin Charters
add_action("manage_cabin-charter_posts_custom_column", "cabin_charter_custom_columns");
add_filter("manage_edit-cabin-charter_columns", "cabin_charter_edit_columns");

function cabin_charter_edit_columns($columns) {
    $new_columns = array(
        "title" => __('Cabin charter name', 'friotyacht'),
        "date_from_to" => __('Date from-to', 'friotyacht'),
        "thumbnail" => __('Image', 'friotyacht'),
    );

    $orderarray = array("cb", "title", "date_from_to", "thumbnail", "date");
    $mergedarray = array_merge($columns, $new_columns);
    return sortArrayByArray($mergedarray, $orderarray);
}

function cabin_charter_custom_columns($column) {
    global $post;

    switch ($column) {
        case "date_from_to":
            $cabin_charter_from = get_post_meta($post->ID, '_cabin_charter_adatok_mbox_date_from', true);
            $cabin_charter_to = get_post_meta($post->ID, '_cabin_charter_adatok_mbox_date_to', true);
            echo "<center>" . mysql2date('Y. m. d.', $cabin_charter_from) . " - " . mysql2date('Y. m. d.', $cabin_charter_to) . "</center>";

            break;
        case "thumbnail":
            echo "<center>" . the_post_thumbnail('thumbnail') . "</center>";
            break;
    }
}

// Areas
add_action("manage_areas_posts_custom_column", "areas_custom_columns");
add_filter("manage_edit-areas_columns", "areas_edit_columns");
add_filter("manage_edit-areas_sortable_columns", "order_areas_column_register_sortable");
add_action("restrict_manage_posts", "areas_restrict_manage_posts");

function areas_edit_columns($columns) {


    $new_columns = array(
        "title" => __('Country name', 'friotyacht'),
        "destinations" => __('Destinations', 'friotyacht'),
        "thumbnail" => __('Image', 'friotyacht'),
    );

    $orderarray = array("cb", "title", "destinations", "thumbnail", "date");
    $mergedarray = array_merge($columns, $new_columns);
    return sortArrayByArray($mergedarray, $orderarray);
}

function areas_custom_columns($column) {
    global $post;

    switch ($column) {

        case "destinations":
            echo get_the_term_list($post->ID, 'destinations', '', ', ', '');
            break;
        case "thumbnail":
            echo "<center>" . the_post_thumbnail('thumbnail', array('style' => 'max-width:90px;max-height:90px;width:auto;height:auto;')) . "</center>";
            break;
    }
}

function order_areas_column_register_sortable($columns) {
    $custom = array(
// meta column id => sortby value used in query
        'destinations' => 'destinations',
    );

    return wp_parse_args($custom, $columns);

    return $columns;
}

function areas_restrict_manage_posts() {

// only display these taxonomy filters on desired custom post_type listings
    global $typenow;
    if ($typenow == 'areas') {

// create an array of taxonomy slugs you want to filter by - if you want to retrieve all taxonomies, could use get_taxonomies() to build the list
        $filters = array('destinations');

        foreach ($filters as $tax_slug) {
// retrieve the taxonomy object
            $tax_obj = get_taxonomy($tax_slug);
            $tax_name = $tax_obj->labels->name;
// retrieve array of term objects per taxonomy
            $terms = get_terms($tax_slug);

// output html for taxonomy dropdown filter
            echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
            echo "<option value=''>" . __('Minden', 'friotyacht') . " $tax_name</option>";
            foreach ($terms as $term) {
// output each select option line, check against the last $_GET to show the current option selected
                echo '<option value=' . $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '', '>' . $term->name . ' (' . $term->count . ')</option>';
            }
            echo "</select>";
        }
    }
}

// Partners
add_action("manage_partners_posts_custom_column", "partners_custom_columns");
add_filter("manage_edit-partners_columns", "partners_edit_columns");

function partners_edit_columns($columns) {
    $new_columns = array(
        "title" => __('Partner', 'friotyacht'),
        "thumbnail" => __('Image', 'friotyacht'),
    );

    $orderarray = array("cb", "title", "thumbnail", "date");
    $mergedarray = array_merge($columns, $new_columns);
    return sortArrayByArray($mergedarray, $orderarray);
}

function partners_custom_columns($column) {
    global $post;

    switch ($column) {
        case "thumbnail":
            echo "<center>" . the_post_thumbnail('thumbnail') . "</center>";
            break;
    }
}

// Cabin charter days
add_action("manage_cabin-charter_days_posts_custom_column", "cabin_charter_days_custom_columns");
add_filter("manage_edit-cabin-charter_days_columns", "cabin_charter_days_edit_columns");

//add_filter("manage_edit-cabin-charter_days_sortable_columns", "order_cabin_charter_days_column_register_sortable");
//add_action("restrict_manage_posts", "cabin_charter_days_restrict_manage_posts");

function cabin_charter_days_edit_columns($columns) {


    $new_columns = array(
        "title" => __('Country name', 'friotyacht'),
        "cabin-charter" => __('Cabin Charter', 'friotyacht'),
        "thumbnail" => __('Image', 'friotyacht'),
    );

    $orderarray = array("cb", "title", "cabin-charter", "thumbnail", "date");
    $mergedarray = array_merge($columns, $new_columns);
    return sortArrayByArray($mergedarray, $orderarray);
}

function cabin_charter_days_custom_columns($column) {
    global $post;

    switch ($column) {

        case "cabin-charter":
            $cabin_charter_id = get_post_meta($post->ID, '_cabin_charter_id', true);
            echo get_the_title($cabin_charter_id);
            break;
        case "thumbnail":
            echo "<center>" . the_post_thumbnail('thumbnail', array('style' => 'max-width:90px;max-height:90px;width:auto;height:auto;')) . "</center>";
            break;
    }
}

// Tour reposrt days
add_action("manage_tour-report-days_posts_custom_column", "tour_report_days_custom_columns");
add_filter("manage_edit-tour-report-days_columns", "tour_report_days_edit_columns");

//add_filter("manage_edit-tour-report-days_sortable_columns", "order_tour_report_days_column_register_sortable");
//add_action("restrict_manage_posts", "tour_report_days_restrict_manage_posts");
//add_filter('parse_query', 'friot_select_tour_for_tourdays');

function tour_report_days_edit_columns($columns) {


    $new_columns = array(
        "title" => __('Country name', 'friotyacht'),
        "turabeszamolok" => __('Túrabeszámoló ', 'friotyacht'),
        "thumbnail" => __('Image', 'friotyacht'),
    );

    $orderarray = array("cb", "title", "turabeszamolok", "thumbnail", "date");
    $mergedarray = array_merge($columns, $new_columns);
    return sortArrayByArray($mergedarray, $orderarray);
}

function tour_report_days_custom_columns($column) {
    global $post;

    switch ($column) {

        case "turabeszamolok":
            $turabeszamolok_id = get_post_meta($post->ID, '_tour_report_id', true);
            echo get_the_title($turabeszamolok_id);
            break;
        case "thumbnail":
            echo "<center>" . the_post_thumbnail('thumbnail', array('style' => 'max-width:90px;max-height:90px;width:auto;height:auto;')) . "</center>";
            break;
    }
}

function tour_report_days_restrict_manage_posts() {

    global $typenow;
    // global $wp_query;
    if ($typenow == 'tour-report-days') { // Your custom post type slug
        $tour_report_days_args = array(
            'post_type' => 'turabeszamolok',
            'posts_per_page' => -1,
            'post_status' => 'publish'
        );
        $tour_report_days_query = new WP_Query($tour_report_days_args);
        //print_r($tour_report_days_query);


        $current_tour_id = '';
        if (isset($_GET['current_tour_id'])) {
            $current_tour_id = $_GET['current_tour_id']; // Check if option has been selected
        }
        ?>
        <select name="current_tour_id" id="current_tour_id">
            <option value="all" <?php //selected('all', $current_tour_id);        ?>><?php _e('All', 'friot'); ?></option>
            <?php if ($tour_report_days_query->have_posts()) : while ($tour_report_days_query->have_posts()) : $tour_report_days_query->the_post(); ?>
                    <option value="<?php the_ID(); ?>" <?php selected(the_ID(), $current_tour_id); ?>><?php the_title(); ?>(<?php the_time('Y'); ?>)</option>
                    <?php
                endwhile;
            endif;
            wp_reset_query();
            ?>
        </select>
        <?php
    }
}

function friot_select_tour_for_tourdays($query) {
    global $pagenow;
    // Get the post type
    $post_type = isset($_GET['post_type']) ? $_GET['post_type'] : '';
    if (is_admin() && $pagenow == 'edit.php' && $post_type == 'tour-report-days' && isset($_GET['current_tour_id']) && $_GET['current_tour_id'] != 'all') {
        $query->query_vars['meta_key'] = '_tour_report_id';
        $query->query_vars['meta_value'] = $_GET['current_tour_id'];
        $query->query_vars['meta_compare'] = '=';
    }
}

/*
  add_filter('manage_edit-date_columns', 'wpdocs_add_new_date_columns');
  add_action('manage_date_custom_column', 'wpdocs_show_date_meta_info_in_columns', 10, 3);

  function wpdocs_add_new_date_columns($columns) {

  $new_columns = array(
  "date_price" => __('Price on date', 'friotyacht'),
  "discount_percentage" => __('Discount', 'friotyacht'),
  );

  $orderarray = array("cb", "name", "date_price", "discount_percentage", "description", "slug", "posts");
  $mergedarray = array_merge($columns, $new_columns);
  return sortArrayByArray($mergedarray, $orderarray);
  return $columns;
  }

  function wpdocs_show_date_meta_info_in_columns($string, $columns, $term_id) {
  switch ($columns) {
  // in this example, we had saved some term meta as "date_price"
  case 'date_price' :
  echo "&euro;" . esc_html(get_term_meta($term_id, 'date_price', true));
  break;
  case 'discount_percentage' :
  echo esc_html(get_term_meta($term_id, 'discount_percentage', true)) . '%';
  break;
  }
  }
 */

// Jelentkezők
add_action("manage_bookings_posts_custom_column", "bookings_custom_columns");
add_filter("manage_edit-bookings_columns", "bookings_edit_columns");

function bookings_edit_columns($columns) {


    $new_columns = array(
        "id" => "ID",
        "title" => __('Name', 'friotyacht'),
        "tura" => __('Tour', 'friotyacht'),
        "telefon_szam" => __('Phone', 'friotyacht'),
        "email_cim" => __('Email', 'friotyacht'),
    );

    $orderarray = array("cb", "id", "title", "tura", "telefon_szam", "email_cim", "date");
    $mergedarray = array_merge($columns, $new_columns);
    return sortArrayByArray($mergedarray, $orderarray);
}

function bookings_custom_columns($column) {
    global $post;

    switch ($column) {

        case "id":
            echo "#" . get_the_ID();
            break;
        case "telefon_szam":
            echo get_post_meta($post->ID, '_cabin_charter_bookings_mbox_phone', true);
            break;

        case "email_cim":
            echo get_post_meta($post->ID, '_cabin_charter_bookings_mbox_email', true);
            break;
        case "tura":
            if (!get_post_meta($post->ID, '_cabin_charter_bookings_mbox_tura_id', true)) {
                
            } else {
                $tura = get_post_meta($post->ID, '_cabin_charter_bookings_mbox_tura_id', true);
                $tura_post_type_slug = get_post_type($tura);
                $tura_post_type = get_post_type_object($tura_post_type_slug);
                echo '<a href="' . get_permalink($tura) . '" target="_blank"><span class="dashicons dashicons-external"></span> ' . $tura_post_type->labels->singular_name . ': ' . get_the_title($tura) . '</a>';
            }
            break;
    }
}

// Ajánlatkérések
add_action("manage_enquiries_posts_custom_column", "enquiries_custom_columns");
add_filter("manage_edit-enquiries_columns", "enquiries_edit_columns");

function enquiries_edit_columns($columns) {


    $new_columns = array(
        "id" => "ID",
        "title" => __('Name', 'friotyacht'),
        "budget" => __('Budget', 'friotyacht'),
        "telefon_szam" => __('Phone', 'friotyacht'),
        "email_cim" => __('Email', 'friotyacht'),
    );

    $orderarray = array("cb", "id", "title", "budget", "telefon_szam", "email_cim", "date");
    $mergedarray = array_merge($columns, $new_columns);
    return sortArrayByArray($mergedarray, $orderarray);
}

function enquiries_custom_columns($column) {
    global $post;

    switch ($column) {

        case "id":
            echo "#" . get_the_ID();
            break;
        case "telefon_szam":
            echo get_post_meta($post->ID, '_general_enquiry_mbox_phone', true);
            break;

        case "email_cim":
            echo get_post_meta($post->ID, '_general_enquiry_mbox_email', true);
            break;
        case "budget":
            echo get_post_meta($post->ID, '_general_enquiry_mbox_budget', true);

            break;
    }
}

// Ships
add_action("manage_ships_posts_custom_column", "ships_custom_columns");
add_filter("manage_edit-ships_columns", "ships_edit_columns");

//add_filter("manage_edit-ships_sortable_columns", "order_ships_column_register_sortable");
//add_action("restrict_manage_posts", "ships_restrict_manage_posts");

function ships_edit_columns($columns) {


    $new_columns = array(
        "title" => __('Ship', 'friotyacht'),
        "cabin-charter" => __('Charter', 'friotyacht'),
        "thumbnail" => __('Image', 'friotyacht'),
    );

    $orderarray = array("cb", "title", "cabin-charter", "thumbnail", "date");
    $mergedarray = array_merge($columns, $new_columns);
    return sortArrayByArray($mergedarray, $orderarray);
}

function ships_custom_columns($column) {
    global $post;

    switch ($column) {

        case "cabin-charter":
            if (get_post_meta($post->ID, '_ships_adatok_mbox_cabin_charter_id', true)) {
                $cabin_charter_id = get_post_meta($post->ID, '_ships_adatok_mbox_cabin_charter_id', true);
                echo get_the_title($cabin_charter_id);
            } else {
                echo '-';
            }
            break;
        case "thumbnail":
            echo "<center>" . the_post_thumbnail('thumbnail', array('style' => 'max-width:90px;max-height:90px;width:auto;height:auto;')) . "</center>";
            break;
    }
}

// boatrentals-links

add_action("manage_boatrentals-links_posts_custom_column", "boatrentals_links_custom_columns");
add_filter("manage_edit-boatrentals-links_columns", "boatrentals_links_edit_columns");

function boatrentals_links_edit_columns($columns) {
    $new_columns = array(
        "title" => __('Slide címe', 'friotyacht'),
        "thumbnail" => __('Kép', 'friotyacht'),
    );

    $orderarray = array("cb", "title", "thumbnail", "date");
    $mergedarray = array_merge($columns, $new_columns);
    return sortArrayByArray($mergedarray, $orderarray);
}

function boatrentals_links_custom_columns($column) {
    global $post;

    switch ($column) {
        case "thumbnail":
            echo "<center>" . the_post_thumbnail('thumbnail') . "</center>";
            break;
    }
}

// Testimonial
add_action("manage_testimonial_posts_custom_column", "testimonial_custom_columns");
add_filter("manage_edit-testimonial_columns", "testimonial_edit_columns");

//add_filter("manage_edit-testimonial_sortable_columns", "order_testimonial_column_register_sortable");
//add_action("restrict_manage_posts", "testimonial_restrict_manage_posts");

function testimonial_edit_columns($columns) {


    $new_columns = array(
        "title" => __('Name', 'friotyacht'),
    );

    $orderarray = array("cb", "title", "date");
    $mergedarray = array_merge($columns, $new_columns);
    return sortArrayByArray($mergedarray, $orderarray);
}

function testimonial_custom_columns($column) {
    global $post;
}
