<?php

/* WP-HeaD() Clearing */
remove_action('wp_head', 'feed_links_extra', 3); // Removes the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Removes links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Removes the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Removes the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Removes the index link
remove_action('wp_head', 'parent_post_rel_link'); // Removes the prev link
remove_action('wp_head', 'start_post_rel_link'); // Removes the start link
remove_action('wp_head', 'adjacent_posts_rel_link'); // Removes the relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Removes the relational links for the posts adjacent
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'print_emoji_detection_script', 7);  //
remove_action('wp_print_styles', 'print_emoji_styles');
add_filter('show_admin_bar', '__return_false');

/* Image sizes */
add_theme_support('post-thumbnails');
add_image_size('sliderimg', 1370, 370, true);
add_image_size('headerimg', 1920, 1080, true);
add_image_size('areaimg', 400, 400, true);
add_image_size('charter_slider_thumb', 158, 98, true);
add_image_size('charter_slider', 628, 374, true);

// add excerpt to pages
add_action('init', 'page_custom_init');

function page_custom_init() {
    add_post_type_support('page', 'excerpt');
}

/* Custom length excerpts */

function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    } else {
        $excerpt = implode(" ", $excerpt);
    }
    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
    return $excerpt;
}

/* Menu setups */
add_theme_support('menus');

function menu_setup() {

    register_nav_menus(
            array(
                'main_menu_top' => __('Fejléc menü - felső', 'friotyacht'),
                'main_menu_bottom' => __('Fejléc menü - alsó', 'friotyacht'),
                'main_menu_bottom_2' => __('Fejléc menü mobil - alsó', 'friotyacht'),
            )
    );
}

add_action('init', 'menu_setup');

// Add homepage to menu editor
function home_page_menu_args($args) {
    $args['show_home'] = true;
    return $args;
}

add_filter('wp_page_menu_args', 'home_page_menu_args');

// add tag support to custom posts
function post_type_tags_fix($request) {
    if (isset($request['tag']) && !isset($request['post_type']))
        $request['post_type'] = 'any';
    return $request;
}

add_filter('request', 'post_type_tags_fix');

add_action('wp_enqueue_scripts', 'load_dashicons_front_end');

function load_dashicons_front_end() {
    wp_enqueue_style('dashicons');
}

function add_menu_icons_styles() {
    ?>

    <style>
        .icon16.icon-areas:before,
        #adminmenu .menu-icon-areas div.wp-menu-image:before {
            content: "\f11d";
        }

        .icon16.icon-cabin-charter:before,
        #adminmenu .menu-icon-cabin-charter div.wp-menu-image:before {
            content: "\f527";
        }

        .icon16.icon-ships:before,
        #adminmenu .menu-icon-ships div.wp-menu-image:before {
            content: "\f325";
        }

        .icon16.icon-ships:before,
        #adminmenu .menu-icon-ships div.wp-menu-image:before {
            content: "\f325";
        }

        .icon16.icon-cabin-charter_days:before,
        #adminmenu .menu-icon-cabin-charter_days div.wp-menu-image:before {
            content: "\f164";
        }

        .icon16.icon-turabeszamolok:before,
        #adminmenu .menu-icon-turabeszamolok div.wp-menu-image:before {
            content: "\f529";
        }

        .icon16.icon-campaigns:before,
        #adminmenu .menu-icon-campaigns div.wp-menu-image:before {
            content: "\f164";
        }

        .icon16.icon-tour-report-days:before,
        #adminmenu .menu-icon-tour-report-days div.wp-menu-image:before {
            content: "\f164";
        }

        .icon16.icon-homepage:before,
        #adminmenu .menu-icon-homepage div.wp-menu-image:before {
            content: "\f495";
        }

        .icon16.icon-crewed-charter:before,
        #adminmenu .menu-icon-crewed-charter div.wp-menu-image:before {
            content: "\f307";
        }

        .icon16.icon-partners:before,
        #adminmenu .menu-icon-partners div.wp-menu-image:before {
            content: "\f338";
        }

        .icon16.icon-bookings:before,
        #adminmenu .menu-icon-bookings div.wp-menu-image:before {
            content: "\f508";
        }

        .icon16.icon-enquiries:before,
        #adminmenu .menu-icon-enquiries div.wp-menu-image:before {
            content: "\f481";
        }

        .icon16.icon-boatrentals-links:before,
        #adminmenu .menu-icon-boatrentals-links div.wp-menu-image:before {
            content: "\f504";
        }

        .icon16.icon-testimonial:before,
        #adminmenu .menu-icon-testimonial div.wp-menu-image:before {
            content: "\f122";
        }
    </style>

    <?php

}

add_action('admin_head', 'add_menu_icons_styles');

function change_wp_login_title() {
    return get_option('blogname');
}

add_filter('login_headertitle', 'change_wp_login_title');

function remove_footer_admin() {
    echo '<p>' . get_bloginfo('name') . ' -  © ' . date('Y') . ' All rights reserved.</p>';
}

add_filter('admin_footer_text', 'remove_footer_admin');

function custom_login_logo() {
    echo '<style type="text/css">
    html {background-color:#ffffff !important;}
    .login #backtoblog a,
    .login #nav a{color: #1aa687;}
                .login h1 a { background-size: contain;    height: 140px;    width:  300px; background-image:url(' . plugin_dir_url(__FILE__) . 'logo.jpg) !important;background-size: contain; }
                body.login {   background: none repeat scroll 0 0 #092c3f;
    }
        </style>';
}

add_action('login_head', 'custom_login_logo');

add_action('admin_footer-edit-tags.php', 'wpse_56569_remove_cat_tag_description');

function wpse_56569_remove_cat_tag_description() {
    global $current_screen;
    switch ($current_screen->id) {
        case 'edit-category':
            // WE ARE AT /wp-admin/edit-tags.php?taxonomy=category
            // OR AT /wp-admin/edit-tags.php?action=edit&taxonomy=category&tag_ID=1&post_type=post
            break;
        case 'edit-post_tag':
            // WE ARE AT /wp-admin/edit-tags.php?taxonomy=post_tag
            // OR AT /wp-admin/edit-tags.php?action=edit&taxonomy=post_tag&tag_ID=3&post_type=post
            break;
    }
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $('.column-description').remove();
        });
    </script>
    <?php

}

// WIDGET
if (function_exists("register_sidebar")) {
    widgets_init();
}

function widgets_init() {
    register_sidebar(array(
        'name' => __('Footer Contact', 'friotyacht'),
        'id' => 'footer-contact',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => '',
    ));
    register_sidebar(array(
        'name' => __('Footer 0', 'friotyacht'),
        'id' => 'footer-0',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => '',
    ));
    register_sidebar(array(
        'name' => __('Footer Newsletter head', 'friotyacht'),
        'id' => 'footer-newsletterhead',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => '',
    ));
    register_sidebar(array(
        'name' => __('Footer 1', 'friotyacht'),
        'id' => 'footer-1',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<p class="footer-subtitle">',
        'after_title' => '</p>',
    ));
    register_sidebar(array(
        'name' => __('Footer 2', 'friotyacht'),
        'id' => 'footer-2',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<p class="footer-subtitle">',
        'after_title' => '</p>',
    ));
    register_sidebar(array(
        'name' => __('Footer 3', 'friotyacht'),
        'id' => 'footer-3',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<p class="footer-subtitle">',
        'after_title' => '</p>',
    ));
    register_sidebar(array(
        'name' => __('Footer 4', 'friotyacht'),
        'id' => 'footer-4',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<p class="footer-subtitle">',
        'after_title' => '</p>',
    ));
    register_sidebar(array(
        'name' => __('Contact 1', 'friotyacht'),
        'id' => 'contact-1',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<p class="footer-subtitle">',
        'after_title' => '</p>',
    ));
    register_sidebar(array(
        'name' => __('Contact 2', 'friotyacht'),
        'id' => 'contact-2',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<p class="footer-subtitle">',
        'after_title' => '</p>',
    ));
}

function fix_media_permissions() {
    global $wp_post_types;
    $wp_post_types['attachment']->cap->edit_post = 'upload_files';
    $wp_post_types['attachment']->cap->read_post = 'upload_files';
    $wp_post_types['attachment']->cap->delete_post = 'upload_files';
    $wp_post_types['attachment']->cap->edit_posts = 'upload_files';
    $wp_post_types['attachment']->cap->edit_others_posts = 'upload_files';
    $wp_post_types['attachment']->cap->edit_published_posts = 'upload_files';
    $wp_post_types['attachment']->cap->publish_posts = 'upload_files';
    $wp_post_types['attachment']->cap->delete_posts = 'upload_files';
    $wp_post_types['attachment']->cap->delete_others_posts = 'upload_files';
    $wp_post_types['attachment']->cap->delete_published_posts = 'upload_files';
    $wp_post_types['attachment']->cap->delete_private_posts = 'upload_files';
    $wp_post_types['attachment']->cap->edit_private_posts = 'upload_files';
    $wp_post_types['attachment']->cap->read_private_posts = 'upload_files';
}

add_action('init', 'fix_media_permissions');

add_action('init', 'add_multiple_thumbnails');

function add_multiple_thumbnails() {
    if (class_exists('MultiPostThumbnails')) {
        new MultiPostThumbnails(
                array(
            'label' => 'Térkép',
            'id' => 'map_image',
            'post_type' => 'libraryinfos'
                )
        );
    }
}

/**
 * Creates a token usable in a form
 * @return string
 */
function getToken() {
    $token = sha1(mt_rand());
    if (!isset($_SESSION['tokens'])) {
        $_SESSION['tokens'] = array($token => 1);
    } else {
        $_SESSION['tokens'][$token] = 1;
    }
    return $token;
}

/**
 * Check if a token is valid. Removes it from the valid tokens list
 * @param string $token The token
 * @return bool
 */
function isTokenValid($token) {
    if (!empty($_SESSION['tokens'][$token])) {
        unset($_SESSION['tokens'][$token]);
        return true;
    }
    return false;
}

/**
 * Captcha
 */
add_action('wp_ajax_nopriv_create_login', 'create_login');

function create_login() {
    $captcha_error = false;
    if (isset($_REQUEST['g-recaptcha-response'])) {
        $captcha = $_REQUEST['g-recaptcha-response'];
    }
    if (!$captcha) {
        $captcha_error = true;
    } else {
        $secretKey = "6Lf_ouEUAAAAABXEz8uAGuBg_BfUaVSEwWs5qRGc";
        $ip = $_SERVER['REMOTE_ADDR'];
        // post request to server
        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) . '&response=' . urlencode($captcha);
        $response = file_get_contents($url);
        $responseKeys = json_decode($response, true);
        // should return JSON with success as true
        if (!$responseKeys["success"]) {
            $captcha_error = true;
        }
    }
    if ($captcha_error) {
        $ret['msg'] = 'Hibás recaptcha';
        $ret['loggedin'] = false;
        $ret['error'] = true;
    } else {
        $user_name = $_REQUEST['user_name'];
        $user_pass = $_REQUEST['user_pass'];
        $ret = array();
        $creds['user_login'] = $user_name;
        $creds['user_password'] = $user_pass;
        $creds['remember'] = true;
        $user = wp_signon($creds, false);
        if (is_wp_error($user)) {
            $ret['msg'] = 'Hibás felhasználónév vagy jelszó';
            $ret['loggedin'] = false;
            $ret['error'] = true;
        } elseif ($user->roles[0] == 'torolt') {
            $ret['msg'] = 'Letiltott felhasználó';
            $ret['loggedin'] = false;
            $ret['error'] = true;
            wp_logout();
        } else {
            $ret['loggedin'] = true;
            $ret['error'] = false;
        }
        $uid = $user->ID;
    }
    echo json_encode($ret);
    die();
}

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if (getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if (getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if (getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if (getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if (getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

add_action('wp_ajax_get_post_datas', 'get_post_datas');
add_action('wp_ajax_nopriv_get_post_datas', 'get_post_datas');

function get_post_datas() {

    // IDE JÖN A LOOP

    $post_id = intval($_REQUEST['post_id']);

    if ($post_id == 0) {

        $response['error'] = 'true';
        $response['result'] = 'Invalid Input';
    } else {

        // get the post
        $thispost = get_post($post_id);
        $large_image_url = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'large');

        // check if post exists
        if (!is_object($thispost)) {

            $response['error'] = 'true';
            $response['result'] = 'There is no post with the ID ' . $post_id;
        } else {

            $response['error'] = 'false';
            $response['result']['title'] = $thispost->post_title;
            $response['result']['content'] = wpautop($thispost->post_content);
            $response['result']['image'] = $large_image_url[0];
            $response['result']['permalink'] = str_replace('http://', 'https://', get_permalink($post_id));
        }
    }

    wp_send_json($response);
}

add_action('wp_ajax_unset_session_error', 'unset_session_error');
add_action('wp_ajax_nopriv_unset_session_error', 'unset_session_error');

function unset_session_error() {
    unset($_SESSION['errormessage']);
}

function szavazatok($postid) {
    global $wpdb;
    if (!isset($postid)) {
        $postid = get_the_ID();
    }
    $szavazatok = $wpdb->get_results(
            "SELECT COUNT(id)
                     FROM votes
                     WHERE palyazat = $postid",
            ARRAY_A
    );
    return $szavazatok[0]["COUNT(id)"];
}

class InstagramApi {

    public function GetAccessToken($client_id, $redirect_uri, $client_secret, $code) {
        $url = 'https://api.instagram.com/oauth/access_token';

        $curlPost = 'client_id=' . $client_id . '&redirect_uri=' . $redirect_uri . '&client_secret=' . $client_secret . '&code=' . $code . '&grant_type=authorization_code';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $data = json_decode(curl_exec($ch), true);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($http_code != '200')
            throw new Exception('Error : Failed to receieve access token');

        return $data['access_token'];
    }

    public function GetUserProfileInfo($access_token) {
        $url = 'https://api.instagram.com/v1/users/self/?access_token=' . $access_token;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $data = json_decode(curl_exec($ch), true);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($data['meta']['code'] != 200 || $http_code != 200)
            throw new Exception('Error : Failed to get user information');

        return $data['data'];
    }
}

add_action('wp_ajax_get_votes', 'get_votes');
add_action('wp_ajax_nopriv_get_votes', 'get_votes');

function get_votes() {


    global $wpdb;

    $fbid = $_REQUEST['fbid'];
    $palyazatok = $wpdb->get_col("SELECT palyazat FROM votes WHERE fbid = '" . $fbid . "'");

    die(json_encode($palyazatok));
}

//  function to geocode address, it will return false if unable to geocode address
function geocode($address) {
    // url encode the address
    $address = urlencode($address);
    // google map geocode api url
    $url = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key=AIzaSyA0uafbGa7QE9fGyTvWk__dM-EEDLtRpGU&language=hu&region=HU";
    // get the json response
    $resp_json = file_get_contents($url);
    // decode the json
    $resp = json_decode($resp_json, true);
    // print_r($resp);die();
    // response status will be 'OK', if able to geocode given address
    if ($resp['status'] == 'OK') {
        // get the important data
        $lati = isset($resp['results'][0]['geometry']['location']['lat']) ? $resp['results'][0]['geometry']['location']['lat'] : "";
        $longi = isset($resp['results'][0]['geometry']['location']['lng']) ? $resp['results'][0]['geometry']['location']['lng'] : "";
        $formatted_address = isset($resp['results'][0]['formatted_address']) ? $resp['results'][0]['formatted_address'] : "";
        // verify if data is complete
        if ($lati && $longi && $formatted_address) {
            // put the data in the array
            $data_arr = array();
            array_push(
                    $data_arr,
                    $lati,
                    $longi,
                    $formatted_address
            );
            return $data_arr;
        } else {
            return false;
        }
    } else {
        echo "<strong>ERROR: {$resp['status']}</strong>";
        return false;
    }
}

add_action('save_post', 'save_post_callback');

function save_post_callback($post_id) {
    global $post, $meta_box_epuletek;

    if ($post->post_type != 'epuletek') {
        return;
    } else {

        $old = get_post_meta($post_id, '_adatok_epulet_cime', true);
        $new = $_POST['_adatok_epulet_cime'];

        $latlong = geocode($new);
        // print_r ($latlong);die();

        update_post_meta($post_id, '_adatok_hosszusagi_fok', $latlong[1]);
        update_post_meta($post_id, '_adatok_szelessegi_kor', $latlong[0]);

        // if ($new && $new != $old) {
        // } elseif ('' == $new && $old) {
        //     // delete_post_meta($post_id, $field['id'], $old);
        // }
    }
    //if you get here then it's your post type so do your thing....
}

// f4 media taxonomies
add_action('init', function () {
    register_taxonomy_for_object_type('markanev', 'attachment');
    register_taxonomy_for_object_type('testtaj', 'attachment');
    register_taxonomy_for_object_type('product_cat', 'attachment');
});

function mail_template_reset_password($reset_link) {
    
}

function mail_template_registration($user_full_name) {
    
}

// bootstrap 5 wp_nav_menu walker
class bootstrap_5_wp_nav_menu_walker extends Walker_Nav_menu {

    private $current_item;
    private $dropdown_menu_alignment_values = [
        'dropdown-menu-start',
        'dropdown-menu-end',
        'dropdown-menu-sm-start',
        'dropdown-menu-sm-end',
        'dropdown-menu-md-start',
        'dropdown-menu-md-end',
        'dropdown-menu-lg-start',
        'dropdown-menu-lg-end',
        'dropdown-menu-xl-start',
        'dropdown-menu-xl-end',
        'dropdown-menu-xxl-start',
        'dropdown-menu-xxl-end'
    ];

    function start_lvl(&$output, $depth = 0, $args = null) {
        $dropdown_menu_class[] = '';
        foreach ($this->current_item->classes as $class) {
            if (in_array($class, $this->dropdown_menu_alignment_values)) {
                $dropdown_menu_class[] = $class;
            }
        }
        $indent = str_repeat("\t", $depth);
        $submenu = ($depth > 0) ? ' sub-menu' : '';
        $output .= "\n$indent<ul class=\"dropdown-menu$submenu " . esc_attr(implode(" ", $dropdown_menu_class)) . " depth_$depth\">\n";
    }

    function start_el(&$output, $item, $depth = 0, $args = null, $id = 0) {
        $this->current_item = $item;

        $indent = ($depth) ? str_repeat("\t", $depth) : '';

        $li_attributes = '';
        $class_names = $value = '';

        $classes = empty($item->classes) ? array() : (array) $item->classes;

        $classes[] = ($args->walker->has_children) ? 'dropdown' : '';
        $classes[] = 'nav-item';
        $classes[] = 'nav-item-' . $item->ID;
        if ($depth && $args->walker->has_children) {
            $classes[] = 'dropdown-menu dropdown-menu-end';
        }

        $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));
        $class_names = ' class="' . esc_attr($class_names) . '"';

        $id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args);
        $id = strlen($id) ? ' id="' . esc_attr($id) . '"' : '';

        $output .= $indent . '<li ' . $id . $value . $class_names . $li_attributes . '>';

        $attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
        $attributes .= !empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
        $attributes .= !empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
        $attributes .= !empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';

        $active_class = ($item->current || $item->current_item_ancestor || in_array("current_page_parent", $item->classes, true) || in_array("current-post-ancestor", $item->classes, true)) ? 'active' : '';
        $nav_link_class = ($depth > 0) ? 'dropdown-item ' : 'nav-link ';
        $attributes .= ($args->walker->has_children) ? ' class="' . $nav_link_class . $active_class . ' dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"' : ' class="' . $nav_link_class . $active_class . '"';

        $item_output = $args->before;
        $item_output .= '<a' . $attributes . '>';
        $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }
}

add_action('admin_init', 'your_function');

function your_function() {
    register_setting('general', 'facebook_link');
    add_settings_field(
            'facebook_link',
            'Facebook link',
            'facebook_link_setting_callback_function',
            'general',
            'default',
            array('label_for' => 'facebook_link')
    );
    register_setting('general', 'instagram_link');
    add_settings_field(
            'instagram_link',
            'Instagram link',
            'instagram_link_setting_callback_function',
            'general',
            'default',
            array('label_for' => 'instagram_link')
    );
    register_setting('general', 'youtube_link');
    add_settings_field(
            'youtube_link',
            'Youtube link',
            'youtube_link_setting_callback_function',
            'general',
            'default',
            array('label_for' => 'youtube_link')
    );
    register_setting('general', 'booking_site_link');
    add_settings_field(
            'booking_site_link',
            'Booking site link',
            'booking_site_link_setting_callback_function',
            'general',
            'default',
            array('label_for' => 'booking_site_link')
    );
    do_action('wpml_multilingual_options', 'booking_site_link');
    register_setting('general', 'mailchimp_list_id');
    add_settings_field(
            'mailchimp_list_id',
            'Maichimp List ID',
            'mailchimp_list_id_setting_callback_function',
            'general',
            'default',
            array('label_for' => 'mailchimp_list_id')
    );

    register_setting('general', 'mailchimp_apikey');
    add_settings_field(
            'mailchimp_apikey',
            'Maichimp API key',
            'mailchimp_apikey_setting_callback_function',
            'general',
            'default',
            array('label_for' => 'mailchimp_apikey')
    );
}

function facebook_link_setting_callback_function($args) {
    echo '<input type="text" name="facebook_link" value="' . get_option('facebook_link') . '" />';
}

function instagram_link_setting_callback_function($args) {
    echo '<input type="text" name="instagram_link" value="' . get_option('instagram_link') . '" />';
}

function youtube_link_setting_callback_function($args) {
    echo '<input type="text" name="youtube_link" value="' . get_option('youtube_link') . '" />';
}

function booking_site_link_setting_callback_function($args) {
    echo '<input type="text" name="booking_site_link" value="' . get_option('booking_site_link') . '" />';
}

function mailchimp_list_id_setting_callback_function($args) {
    echo '<input type="text" name="mailchimp_list_id" value="' . get_option('mailchimp_list_id') . '" />';
}

function mailchimp_apikey_setting_callback_function($args) {
    echo '<input type="text" name="mailchimp_apikey" value="' . get_option('mailchimp_apikey') . '" />';
}

add_action('wp_ajax_get_cabins_avaible_on_ship', 'get_cabins_avaible_on_ship');
add_action('wp_ajax_nopriv_get_cabins_avaible_on_ship', 'get_cabins_avaible_on_ship');

function get_cabins_avaible_on_ship() {
    $currentLangCode = sanitize_text_field($_REQUEST['currentLangCode']);
    $ship_id = intval($_REQUEST['ship_id']);
    $cabin_charter_id = get_post_meta($ship_id, '_ships_adatok_mbox_cabin_charter_id', true);
    do_action('wpml_switch_language', $currentLangCode);

    unset($avaible_cabin_list);
    if (get_post_meta($ship_id, '_ships_adatok_mbox_whole_ship_price', true)) {
        $whole_ship_price = intval(get_post_meta($ship_id, '_ships_adatok_mbox_whole_ship_price', true));
        $whole_ship_price_text = number_format($whole_ship_price, 0, '', ' ');
        if (get_post_meta($cabin_charter_id, '_cabin_charter_adatok_mbox_discount', true)) {
            $whole_ship_price_old =$whole_ship_price;
            $discount_number = intval(get_post_meta($cabin_charter_id, '_cabin_charter_adatok_mbox_discount', true));
            $whole_ship_price = (1 - ($discount_number / 100)) * intval($whole_ship_price);
            $whole_ship_price_text = '<span class="strike-trough"> &euro;' . number_format($whole_ship_price_old, 0, '', ' ') . '</span> <span class="text-red">&euro;' . number_format($whole_ship_price, 0, '', ' ') . '</span>';
        }
        
        $avaible_cabin_list .= '<div class="item first" data-value="' . __('Whole Ship Price &euro;', 'friot') . number_format($whole_ship_price, 0, '', ' ') . '"><p class="cabin">' . __('Whole Ship Price ', 'friot') . $whole_ship_price_text . '</p></div>';
    }
    foreach (range('a', 'f') as $cabin_letter) {
        if (get_post_meta($ship_id, '_ships_adatok_mbox_' . $cabin_letter . '_cabin', true)) {
            $cabin_price = get_post_meta($ship_id, '_ships_adatok_mbox_' . $cabin_letter . '_cabin', true);
            $persontext = '';
            if ((get_post_meta($cabin_charter_id, '_cabin_charter_adatok_mbox_discount', true)) && (get_post_meta($ship_id, '_ships_adatok_mbox_' . $cabin_letter . '_cabin', true) !== __('Foglalt', 'friot'))) {
                $discount_number = intval(get_post_meta($cabin_charter_id, '_cabin_charter_adatok_mbox_discount', true));
                $cabin_price_old = $cabin_price;
                $cabin_price = (1 - ($discount_number / 100)) * intval($cabin_price);
            }
            if (get_post_meta($ship_id, '_ships_adatok_mbox_' . $cabin_letter . '_cabin', true) !== __('Foglalt', 'friot')) {
                $cabin_price = number_format($cabin_price, 0, '', ' ');
                if (get_post_meta($cabin_charter_id, '_cabin_charter_adatok_mbox_discount', true)) {
                    $pericetext = ' &euro;' . $cabin_price . __(' for 2 person', 'friot');
                    $pericetext_extra = '<span class="strike-trough"> &euro;' . number_format($cabin_price_old, 0, '', ' ') . '</span> <span class="text-red">&euro;' . $cabin_price . '</span>' . __(' for 2 person', 'friot');
                } else {
                    $pericetext = '&euro;' . $cabin_price . __(' for 2 person', 'friot');
                    $pericetext_extra = $pericetext;
                }
            } else {
                $pericetext = $pericetext_extra = __('Foglalt', 'friot');
            }

            $avaible_cabin_list .= '<div class="item second" data-value="' . strtoupper($cabin_letter) . __(' Cabin ', 'friot') . $pericetext . '"><p class="cabin">' . strtoupper($cabin_letter) . __(' Cabin ', 'friot') . $pericetext_extra . '</p></div>';
        }
    }
    if (empty($avaible_cabin_list)) {
        $avaible_cabin_list .= '<div class="item third" data-value=""><p class="cabin">' . __('No cabin available', 'friot') . '</p></div>';
    }

    die(json_encode($avaible_cabin_list));
}

add_action('template_redirect', 'technical_redirect_post');

function technical_redirect_post() {
    $queried_post_type = get_query_var('post_type');
    if (is_single() && ('ships' == $queried_post_type || 'cabin-charter_days' == $queried_post_type)) {
        wp_redirect(get_post_type_archive_link('cabin-charter'), 301);
        exit;
    }
    if (is_single() && 'partners' == $queried_post_type) {
        $partners_page_id = apply_filters('wpml_object_id', 609);
        wp_redirect(get_permalink($partners_page_id), 301);
        exit;
    }
}

function friot_filter_post_thumbnail_html($html, $post_id, $post_thumbnail_id, $size, $attr) {
    // If there is no post thumbnail,
    // Return a default image
    if ('' == $html) {
        if (is_array($attr)) {
            $class = $attr['class'];
        }
        $src = wp_get_attachment_image_src(666, $size);
        return '<img src="' . $src[0] . '" data-src="' . $src[0] . '" data-alt="' . $alt . '" class="' . $class . '" />';
    }
    // Else, return the post thumbnail
    return $html;
}

add_filter('post_thumbnail_html', 'friot_filter_post_thumbnail_html', 99, 5);

add_filter('gettext', 'friot_boatrentals_link_excerpt_rename', 10, 2);

function friot_boatrentals_link_excerpt_rename($translation, $original) {
    if (get_post_type() == 'boatrentals-links') {
        if ('Excerpt' == $original) {
            return 'Link';
        } else {
            $pos = strpos($original, 'Excerpts are optional hand-crafted summaries of your');
            if ($pos !== false) {
                return 'Boatrentals external link';
            }
        }
    }
    return $translation;
}

function add_custom_data() {
    // Register the category type
    register_rest_field(
            'destinations',
            'taxomomy_image',
            array(
                'get_callback' => 'get_taxonomy_image__friot',
                'update_callback' => null,
                'schema' => null
            )
    );
    register_rest_field(
            'cabin-charter',
            'post_image',
            array(
                'get_callback' => 'get_post_image__friot',
                'update_callback' => null,
                'schema' => null
            )
    );
}

/**
 * Handler for getting custom data.
 *
 */
function get_taxonomy_image__friot($object, $field_name, $request) {
    if (function_exists('z_taxonomy_image_url')) {
        return z_taxonomy_image_url($object['id'], 'areaimg');
    }
}

function get_post_image__friot($object, $field_name, $request) {
    if (function_exists('get_the_post_thumbnail_url')) {
        return get_the_post_thumbnail_url($object['id'], 'areaimg');
    }
}

add_action('rest_api_init', 'add_custom_data');

function stackoverflow_remove_cpt_slug($post_link, $post) {
    if ('crewed-charter' === $post->post_type && 'publish' === $post->post_status) {
        $post_link = str_replace('/' . $post->post_type . '/', '/', $post_link);
    }
    return $post_link;
}

add_filter('post_type_link', 'stackoverflow_remove_cpt_slug', 10, 2);

function stackoverflow_add_cpt_post_names_to_main_query($query) {
    // Return if this is not the main query.
    if (!$query->is_main_query()) {
        return;
    }
    // Return if this query doesn't match our very specific rewrite rule.
    if (!isset($query->query['page']) || 2 !== count($query->query)) {
        return;
    }
    // Return if we're not querying based on the post name.
    if (!empty($query->query['name'])) {
        $query->set('post_type', array('post', 'page', 'crewed-charter'));
    }
    // Add CPT to the list of post types WP will include when it queries based on the post name.
}

add_action('pre_get_posts', 'stackoverflow_add_cpt_post_names_to_main_query');

define("offers_time", '620');

function offers() {
    $offers = array();
    //delete_transient('offers');
    $offers = get_transient('offers_' . ICL_LANGUAGE_CODE);
    if ($offers === false) {
        $offers = file_get_contents("https://foglalas.friotyacht.com/site/ajax-offers?lang=" . ICL_LANGUAGE_CODE);
        set_transient('offers_' . ICL_LANGUAGE_CODE, $offers, offers_time);
    }
    return $offers;
}

/**
 * Validate form with reCaptcha
 * Return true or false
 */
function recaptcha_validate($token) {
    if (!isset($token)) {
        return false;
    }
    $siteverify = 'https://www.google.com/recaptcha/api/siteverify';
    $secret = '6Lda_DIjAAAAAGlhfwWV_YzS4qRm-oh17xVyIOzq';
    $response = file_get_contents($siteverify . '?secret=' . $secret . '&response=' . $token);
    $response = json_decode($response, true);
    return $response;
}

function get_avaible_cabins_on_tour($tour_id) {
    $cabins = 0;
    $args = array(
        'post_type' => 'ships',
        'posts_per_page' => -1,
        'meta_query' => array(
            array(
                'key' => '_ships_adatok_mbox_cabin_charter_id',
                'value' => array($tour_id),
                'compare' => 'IN',
            ),
        )
    );
    $loop = new WP_Query($args);
    if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
            $ship_id = get_the_ID();

            foreach (range('a', 'f') as $cabin_letter) {
                if (get_post_meta($ship_id, '_ships_adatok_mbox_' . $cabin_letter . '_cabin', true)) {
                    if (ICL_LANGUAGE_CODE === 'hu') {
                        if ((str_contains(get_post_meta($ship_id, '_ships_adatok_mbox_' . $cabin_letter . '_cabin', true), 'Foglalt') != 1)) {
                            $cabins++;
                        }
                    } elseif (ICL_LANGUAGE_CODE === 'en') {
                        if (str_contains(get_post_meta($ship_id, '_ships_adatok_mbox_' . $cabin_letter . '_cabin', true), 'Full') != 1) {
                            $cabins++;
                        }
                    }
                }
            }
        endwhile;
    endif;
    wp_reset_postdata();

    return $cabins;
}

if (!function_exists('str_contains')) {

    function str_contains($haystack, $needle) {
        return $needle !== '' && mb_strpos($haystack, $needle) !== false;
    }

}

// define the wpcf7_posted_data callback 
function action_wpcf7_posted_data($array) {
    //'checkbox-name' is the name that you gave the field in the CF7 admin.
    $wpcf7 = WPCF7_ContactForm::get_current();
    $form_id = $wpcf7->id();

    $campaign_form_id = apply_filters('wpml_object_id', 4294);
    if ($form_id == $campaign_form_id) {
        $email = $array['your-email'];
        $salt = "gQ5}dO9{zM2_bX3~";
        $download_code = hash('sha512', $email . $salt);
        $array['pdf_link'] .= get_permalink(4296) . '?email=' . urlencode($email) . '&pdf=' . $download_code;
    }
    return $array;
}

add_filter('wpcf7_posted_data', 'action_wpcf7_posted_data', 10, 1);
