<?php

/**
 * This snippet has been updated to reflect the official supporting of options pages by CMB2
 * in version 2.2.5.
 *
 * If you are using the old version of the options-page registration,
 * it is recommended you swtich to this method.
 */
add_action('cmb2_admin_init', 'friot_register_theme_options_metabox');

/**
 * Hook in and register a metabox to handle a theme options page and adds a menu item.
 */
function friot_register_theme_options_metabox() {

    /**
     * Registers options page menu item and form.
     */
    $cmb_options = new_cmb2_box(array(
        'id' => 'friot_option_metabox',
        'title' => esc_html__('Friot beállítások', 'friot'),
        'object_types' => array('options-page'),
        /*
         * The following parameters are specific to the options-page box
         * Several of these parameters are passed along to add_menu_page()/add_submenu_page().
         */
        'option_key' => 'friot_options', // The option key and admin menu page slug.
        'icon_url' => 'dashicons-layout', // Menu icon. Only applicable if 'parent_slug' is left empty.
        // 'menu_title'      => esc_html__( 'Options', 'friot' ), // Falls back to 'title' (above).
        // 'parent_slug'     => 'themes.php', // Make options page a submenu item of the themes menu.
        'capability' => 'manage_options', // Cap required to view options-page.
        // 'position'        => 1, // Menu position. Only applicable if 'parent_slug' is left empty.
        // 'admin_menu_hook' => 'network_admin_menu', // 'network_admin_menu' to add network-level options page.
        // 'display_cb'      => false, // Override the options-page form output (CMB2_Hookup::options_page_output()).
        'save_button' => esc_html__('Beállítások mentése', 'friot'), // The text for the options-page save button. Defaults to 'Save'.
    ));

    /*
     * Options fields ids only need
     * to be unique within this box.
     * Prefix is not needed.
     */
    $cmb_options->add_field(array(
        'name' => 'Oldal válsztó',
        'desc' => '',
        'type' => 'title',
        'id' => 'box_1_settings'
    ));
    $cmb_options->add_field(array(
        'name' => __('Túra  ajánlatkérő oldal'),
        'id' => 'tura_ajanlatkero',
        'type' => 'page_select_text', // This field type
        // post type also as array
        'post_type' => 'page',
        // Default is 'checkbox', used in the modal view to select the post type
        'select_type' => 'radio',
        // Will replace any selection with selection from modal. Default is 'add'
        'select_behavior' => 'replace',
    ));
    $cmb_options->add_field(array(
        'name' => __('Túra  ajánlatkérő köszönő oldal'),
        'id' => 'tura_ajanlatkero_koszono',
        'type' => 'page_select_text', // This field type
        // post type also as array
        'post_type' => 'page',
        // Default is 'checkbox', used in the modal view to select the post type
        'select_type' => 'radio',
        // Will replace any selection with selection from modal. Default is 'add'
        'select_behavior' => 'replace',
    ));
        $cmb_options->add_field(array(
        'name' => __('Túra előregisztráció oldal'),
        'id' => 'tura_eloregisztracio',
        'type' => 'page_select_text', // This field type
        // post type also as array
        'post_type' => 'page',
        // Default is 'checkbox', used in the modal view to select the post type
        'select_type' => 'radio',
        // Will replace any selection with selection from modal. Default is 'add'
        'select_behavior' => 'replace',
    ));
    $cmb_options->add_field(array(
        'name' => __('Túra előregisztráció köszönő oldal'),
        'id' => 'tura_eloregisztracio_koszono',
        'type' => 'page_select_text', // This field type
        // post type also as array
        'post_type' => 'page',
        // Default is 'checkbox', used in the modal view to select the post type
        'select_type' => 'radio',
        // Will replace any selection with selection from modal. Default is 'add'
        'select_behavior' => 'replace',
    ));
    $cmb_options->add_field(array(
        'name' => __('Túra  foglaló köszönő oldal'),
        'id' => 'tura_foglalo_koszono',
        'type' => 'page_select_text', // This field type
        // post type also as array
        'post_type' => 'page',
        // Default is 'checkbox', used in the modal view to select the post type
        'select_type' => 'radio',
        // Will replace any selection with selection from modal. Default is 'add'
        'select_behavior' => 'replace',
    ));
    $cmb_options->add_field(array(
        'name' => 'Email szövegek MAGYAR',
        'desc' => '',
        'type' => 'title',
        'id' => 'box_2_settings'
    ));
    $cmb_options->add_field(array(
        'name' => 'Túra ajánlatkérő email',
        'desc' => 'Túra ajánlatkérő form beküldése után az ügyfélnek kiküldött email szövege.',
        'id' => 'tura_ajanlatkero_email_szoveg',
        'type' => 'wysiwyg',
        'options' => array(),
    ));
    $cmb_options->add_field(array(
        'name' => 'Túra  foglalás email',
        'desc' => 'Túra  foglalás form beküldése után az ügyfélnek kiküldött email szövege.',
        'id' => 'tura_foglalas_email_szoveg',
        'type' => 'wysiwyg',
        'options' => array(),
    ));
    $cmb_options->add_field(array(
        'name' => 'Desztináció ajánlatkérés email',
        'desc' => 'Desztináció ajánlatkéréső form beküldése után az ügyfélnek kiküldött email szövege.',
        'id' => 'desztinacio_ajanlatkero_email_szoveg',
        'type' => 'wysiwyg',
        'options' => array(),
    ));
    $cmb_options->add_field(array(
        'name' => 'Hajózás kapitánnyal ajánlatkérő email',
        'desc' => 'Hajózás kapitánnyal ajánlatkérő form beküldése után az ügyfélnek kiküldött email szövege.',
        'id' => 'hajozas_kapitannyal_ajanlatkero_email_szoveg',
        'type' => 'wysiwyg',
        'options' => array(),
    ));
    $cmb_options->add_field(array(
        'name' => 'Általános ajánlatkérő email',
        'desc' => 'Általános ajánlatkérő form beküldése után az ügyfélnek kiküldött email szövege.',
        'id' => 'altalanos_ajanlatkero_email_szoveg',
        'type' => 'wysiwyg',
        'options' => array(),
    ));
    $cmb_options->add_field(array(
        'name' => 'Ingyenes konzultáció email',
        'desc' => 'Ingyenes konzultáció form beküldése után az ügyfélnek kiküldött email szövege.',
        'id' => 'ingyenes_konzultacio_email_szoveg',
        'type' => 'wysiwyg',
        'options' => array(),
    ));
    $cmb_options->add_field(array(
        'name' => 'EIS email szöveg',
        'desc' => 'EIS checkbox túrahajó foglalás bepipálására az ügyfélnek kiküldött email szövege.',
        'id' => 'eis_email_szoveg',
        'type' => 'wysiwyg',
        'options' => array(),
    ));
    $cmb_options->add_field(array(
        'name' => 'Email szövegek ANGOL',
        'desc' => '',
        'type' => 'title',
        'id' => 'box_3_settings'
    ));
    $cmb_options->add_field(array(
        'name' => 'Túra ajánlatkérő email ANGOL',
        'desc' => 'Túra ajánlatkérő form beküldése után az ügyfélnek kiküldött email szövege.',
        'id' => 'tura_ajanlatkero_email_szoveg_eng',
        'type' => 'wysiwyg',
        'options' => array(),
    ));
    $cmb_options->add_field(array(
        'name' => 'Túra  foglalás email ANGOL',
        'desc' => 'Túra  foglalás form beküldése után az ügyfélnek kiküldött email szövege.',
        'id' => 'tura_foglalas_email_szoveg_eng',
        'type' => 'wysiwyg',
        'options' => array(),
    ));
    $cmb_options->add_field(array(
        'name' => 'Desztináció ajánlatkérés email ANGOL',
        'desc' => 'Desztináció ajánlatkéréső form beküldése után az ügyfélnek kiküldött email szövege.',
        'id' => 'desztinacio_ajanlatkero_email_szoveg_eng',
        'type' => 'wysiwyg',
        'options' => array(),
    ));
    $cmb_options->add_field(array(
        'name' => 'Hajózás kapitánnyal ajánlatkérő email ANGOL',
        'desc' => 'Hajózás kapitánnyal ajánlatkérő form beküldése után az ügyfélnek kiküldött email szövege.',
        'id' => 'hajozas_kapitannyal_ajanlatkero_email_szoveg_eng',
        'type' => 'wysiwyg',
        'options' => array(),
    ));
    $cmb_options->add_field(array(
        'name' => 'Általános ajánlatkérő email ANGOL',
        'desc' => 'Általános ajánlatkérő form beküldése után az ügyfélnek kiküldött email szövege.',
        'id' => 'altalanos_ajanlatkero_email_szoveg_eng',
        'type' => 'wysiwyg',
        'options' => array(),
    ));
    $cmb_options->add_field(array(
        'name' => 'Ingyenes konzultáció email ANGOL',
        'desc' => 'Ingyenes konzultáció form beküldése után az ügyfélnek kiküldött email szövege.',
        'id' => 'ingyenes_konzultacio_email_szoveg_eng',
        'type' => 'wysiwyg',
        'options' => array(),
    ));
    $cmb_options->add_field(array(
        'name' => 'EIS email szöveg ANGOL',
        'desc' => 'EIS checkbox túrahajó foglalás bepipálására az ügyfélnek kiküldött email szövege.',
        'id' => 'eis_email_szoveg_eng',
        'type' => 'wysiwyg',
        'options' => array(),
    ));
}

/**
 * Wrapper function around cmb2_get_option
 * @since  0.1.0
 * @param  string $key     Options array key
 * @param  mixed  $default Optional default value
 * @return mixed           Option value
 */
function friot_get_option($key = '', $default = false) {
    if (function_exists('cmb2_get_option')) {
        // Use cmb2_get_option as it passes through some key filters.
        return cmb2_get_option('friot_options', $key, $default);
    }

    // Fallback to get_option if CMB2 is not loaded yet.
    $opts = get_option('friot_options', $default);

    $val = $default;

    if ('all' == $key) {
        $val = $opts;
    } elseif (is_array($opts) && array_key_exists($key, $opts) && false !== $opts[$key]) {
        $val = $opts[$key];
    }

    return $val;
}
