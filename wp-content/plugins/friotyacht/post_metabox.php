<?php
// Area
$meta_box_area_information = array(
    'id' => 'area-post-meta-box',
    'title' => 'Area informations',
    'page' => 'areas',
    'context' => 'normal',
    'priority' => 'high',
    'fields' => array(
        array(
            'name' => 'Fleet Sailing yacht:',
            'desc' => 'The number of Sailing yachts',
            'id' => 'sailing_yacht_number',
            'type' => 'number',
            'std' => ''
        ),
        array(
            'name' => 'Fleet Catamaran:',
            'desc' => 'The number of Catamarans',
            'id' => 'catamaran_number',
            'type' => 'number',
            'std' => ''
        ),
        array(
            'name' => 'Browse Fleet link:',
            'desc' => '',
            'id' => 'browse_fleet_link_text',
            'type' => 'text',
            'std' => ''
        ), array(
            'name' => 'Things to do image:',
            'desc' => '',
            'id' => 'things_to_do_image',
            'type' => 'upload',
            'std' => ''
        ), array(
            'name' => 'Things to do:',
            'desc' => '',
            'id' => 'things_to_do_text',
            'type' => 'wp_editor',
            'std' => ''
        ), array(
            'name' => 'Weather image:',
            'desc' => '',
            'id' => 'weather_image',
            'type' => 'upload',
            'std' => ''
        ), array(
            'name' => 'Weather:',
            'desc' => '',
            'id' => 'weather_text',
            'type' => 'wp_editor',
            'std' => ''
        ), array(
            'name' => 'Ports image:',
            'desc' => '',
            'id' => 'ports_image',
            'type' => 'upload',
            'std' => ''
        ), array(
            'name' => 'Ports:',
            'desc' => '',
            'id' => 'ports_text',
            'type' => 'wp_editor',
            'std' => ''
        ), array(
            'name' => 'Information image:',
            'desc' => '',
            'id' => 'information_image',
            'type' => 'upload',
            'std' => ''
        ), array(
            'name' => 'Information:',
            'desc' => '',
            'id' => 'information_text',
            'type' => 'wp_editor',
            'std' => ''
        ), array(
            'name' => 'Itiner image:',
            'desc' => '',
            'id' => 'itiner_image',
            'type' => 'upload',
            'std' => ''
        ), array(
            'name' => 'Itiner:',
            'desc' => '',
            'id' => 'itiner_text',
            'type' => 'wp_editor',
            'std' => ''
        ), array(
            'name' => 'Gallery image:',
            'desc' => '',
            'id' => 'gallery_image',
            'type' => 'upload',
            'std' => ''
        ), array(
            'name' => 'Gallery:',
            'desc' => '',
            'id' => 'gallery_text',
            'type' => 'wp_editor',
            'std' => ''
        ),
    )
);

add_action('admin_menu', 'area_information_add_box');

// Add meta box
function area_information_add_box() {
    global $meta_box_area_information;

    add_meta_box($meta_box_area_information['id'], $meta_box_area_information['title'], 'area_information_show_box', $meta_box_area_information['page'], $meta_box_area_information['context'], $meta_box_area_information['priority']);
}

// Callback function to show fields in meta box
function area_information_show_box() {
    global $meta_box_area_information, $post;

    // Use nonce for verification
    echo '<input type="hidden" name="area_information_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

    echo '<table class="form-table">';

    meta_box_fields($meta_box_area_information['fields'], $post);

    echo '</table>';
}

add_action('save_post', 'area_information_save_data');

// Save data from meta box
function area_information_save_data($post_id) {
    global $meta_box_area_information;

    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }

    //Don't update on Quick Edit
    if (defined('DOING_AJAX')) {
        return $post_id;
    }

    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return $post_id;
        }
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }

    meta_save_fields($meta_box_area_information['fields'], $post_id);
}

function meta_box_fields($fields, $post) {
    foreach ($fields as $field) {
        // get current post meta data
        $meta = get_post_meta($post->ID, $field['id'], true);

        echo '<tr>';

        if ($field['name']) {
            echo '<th style="width:20%"><label for="',
            $field['id'],
            '">',
            $field['name'],
            '</label></th>',
            '<td>';
        } else {
            echo '<td colspan="2">';
        }
        switch ($field['type']) {
            case 'title':
                echo '<h4>', $meta ? $meta : $field['std'], '</h2>', $field['desc'], '<hr />';
                break;
            case 'wp_editor':
                wp_editor($meta ? $meta : $field['std'], $field['id'], $field);
                break;
            case 'email':
            case 'number':
            case 'text':
                echo '<input type="', $field['type'], '"  name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />', '<br />', $field['desc'];
                break;
            case 'textarea':
                echo '<textarea name="', $field['id'], '" id="', $field['id'], '" cols="60" rows="4" style="width:97%">', $meta ? $meta : $field['std'], '</textarea>', '<br />', $field['desc'];
                break;
            case 'select':
                echo '<select name="', $field['id'], '" id="', $field['id'], '">';
                foreach ($field['options'] as $option) {
                    echo '<option', $meta == $option ? ' selected="selected"' : '', '>', $option, '</option>';
                }
                echo '</select>';
                break;
            case 'radio':
                foreach ($field['options'] as $option) {
                    echo '<input type="radio" name="', $field['id'], '" value="', $option['value'], '"', $meta == $option['value'] ? ' checked="checked"' : '', ' />', $option['name'];
                }
                break;
            case 'checkbox':
                echo '<input type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />', '<br /><br />', $field['desc'];
                break;
            case 'date':
                echo '<input type="date"   name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="38" style="max-width: 200px;"   />', '<br />', $field['desc'];
                break;
            case 'upload':
                add_filter('upload_mimes', 'restict_mime');
                if (true) {
                    ?>
                    <input name="<?php echo $field['id'] ?>" id="<?php echo $field['id']; ?>" value="<?php echo $meta ? $meta : $field['std']; ?>" class="<?php echo (!empty($field['class']) ? $field['class'] : 'large-text'); ?>" />
                    <input id="upload_image_button<?php echo $field['id'] ?>" type="button" class="button add_media" value="Upload" />
                    <?php
                    $attachment_id = attachment_url_to_postid($meta);
                    if ($attachment_id != 0) {
                        $attachment_image_src = wp_get_attachment_image_src($attachment_id, 'medium')
                        ?>
                        <img src="<?php echo $attachment_image_src[0]; ?>" id="<?php echo $field['id'] ?>_preview" style="margin:50px;width:300px;"> 
                    <?php } else { ?>
                        <img src="<?php echo $meta; ?>" id="<?php echo $field['id'] ?>_preview" style="margin:50px;width:300px;"> 
                    <?php } ?>
                    <script>
                        jQuery(document).ready(function () {

                            var custom_uploader;

                            jQuery('#upload_image_button<?php echo $field['id'] ?>').click(function (e) {

                                e.preventDefault();

                                //If the uploader object has already been created, reopen the dialog
                                if (custom_uploader) {
                                    custom_uploader.open();
                                    return;
                                }

                                //Extend the wp.media object
                                custom_uploader = wp.media.frames.file_frame = wp.media({
                                    title: 'CSV feltöltése',
                                    button: {
                                        text: 'Feltölt'
                                    },
                                    multiple: false
                                });

                                //When a file is selected, grab the URL and set it as the text field's value
                                custom_uploader.on('select', function () {
                                    attachment = custom_uploader.state().get('selection').first().toJSON();

                                    // if (attachment.url.indexOf('.csv') !== -1) {
                                    jQuery('#<?php echo $field['id']; ?>').val(attachment.url);
                                    jQuery('#<?php echo $field['id'] . '_preview'; ?>').attr('src', attachment.url);
                                    // } else {
                                    //     jQuery('#<?php echo $field['id']; ?>').val(" ");
                                    //     alert('Nem támogatott formátum!');
                                    //     return false;
                                    // }
                                });

                                //Open the uploader dialog
                                custom_uploader.open();
                            });
                        });
                    </script>
                    <?php
                }
                break;
            case 'cabincharter':
                ?>

                <select style="width:99%;" type="text" name="<?php echo $field['id']; ?>" id="<?php echo $field['id']; ?>" >
                    <option value="">-- Semmi -- </option>
                    <?php
                    $cabincharter_args = array(
                        'post_type' => 'cabin-charter',
                        'posts_per_page' => -1,
                        'post_status' => 'publish'
                    );
                    $cabincharter_query = new WP_Query($cabincharter_args);

                    if ($cabincharter_query->have_posts()) : while ($cabincharter_query->have_posts()) : $cabincharter_query->the_post();
                            ?>
                            <option value="<?php the_ID(); ?>" <?php
                            if ($meta == get_the_ID()) {
                                echo "selected";
                            }
                            ?> ><?php the_title(); ?>(<?php the_time('Y'); ?>)</option>
                                    <?php
                                endwhile;
                            endif;
                            wp_reset_query();
                            wp_reset_postdata();
                            ?>
                </select>

                <?php
                break;
            case 'tourreport':
                ?>

                <select style="width:99%;" type="text" name="<?php echo $field['id']; ?>" id="<?php echo $field['id']; ?>" >
                    <option value="">-- Semmi -- </option>
                    <?php
                    $cabincharter_args = array(
                        'post_type' => 'turabeszamolok',
                        'posts_per_page' => -1,
                        'post_status' => 'publish'
                    );
                    $cabincharter_query = new WP_Query($cabincharter_args);

                    if ($cabincharter_query->have_posts()) : while ($cabincharter_query->have_posts()) : $cabincharter_query->the_post();
                            ?>
                            <option value="<?php the_ID(); ?>" <?php
                            if ($meta == get_the_ID()) {
                                echo "selected";
                            }
                            ?> ><?php the_title(); ?>(<?php the_time('Y'); ?>)</option>
                                    <?php
                                endwhile;
                            endif;
                            wp_reset_query();
                            wp_reset_postdata();
                            ?>
                </select>

                <?php
                break;
        }
        echo '<td>',
        '</tr>';
    }
}

function meta_save_fields($fields, $post_id) {
    foreach ($fields as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];

        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);

            if (isset($field['callback']) && is_callable($field['callback'])) {
                call_user_func($field['callback'], $new, $post_id, $field['id']);
            }
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    }
}

// Destinations
// Add term page
function destinations_taxonomy_add_new_meta_field() {
    // this will add the custom meta field to the add new term page
    if (false) {
        ?>
        <div class="form-field">
            <label for="term_sailing_yacht_number">Fleet Sailing yacht</label>
            <input type="text" name="term_sailing_yacht_number" id="term_sailing_yacht_number"  />
            <p class="description">The number of Sailing yachts</p>
        </div>
        <div class="form-field">
            <label for="term_catamaran_number">Fleet Catamaran</label>
            <input type="text" name="term_catamaran_number" id="term_catamaran_number"  />
            <p class="description">The number of Catamarans</p>
        </div>
    <?php } ?>
    <div class="form-field">
        <label for="term_long_description">Long description</label>
        <?php wp_editor('', 'term_long_description'); ?>      
        <p class="description">The longer descriotion of Destionation (2. text on Destination page)</p>
    </div>
    <?php
}

add_action('destinations_add_form_fields', 'destinations_taxonomy_add_new_meta_field', 10, 2);

// Edit term page
function destinations_taxonomy_edit_meta_fields($term, $taxonomy) {

    // put the term ID into a variable
    //$t_id = $term->term_id;
    // retrieve the existing value(s) for this meta field. This returns an array
    //$term_meta = get_option("taxonomy_$t_id");
    //$term_sailing_yacht_number = get_term_meta($term->term_id, 'term_sailing_yacht_number', true);
    //$term_catamaran_number = get_term_meta($term->term_id, 'term_catamaran_number', true);
    $term_long_description = get_term_meta($term->term_id, 'term_long_description', true);
    ?>
    <table class="form-table">
        <tbody>
            <?php if (false) { ?>
                <tr class="form-field">
                    <th scope="row" valign="top"><label for="term_sailing_yacht_number">Fleet Sailing yacht</label></th>
                    <td>                   
                        <input type="text" value="<?php echo $term_sailing_yacht_number; ?>" id="term_sailing_yacht_number" name="term_sailing_yacht_number"/>
                        <p class="description">The number of Sailing yachts</p>
                    </td>
                </tr>
                <tr class="form-field">
                    <th scope="row" valign="top"><label for="term_catamaran_number">Fleet Catamaran</label></th>
                    <td>                   
                        <input type="text" value="<?php echo $term_catamaran_number; ?>" id="term_catamaran_number" name="term_catamaran_number"/>
                        <p class="description">The number of Catamarans</p>
                    </td>
                </tr>
            <?php } ?>
            <tr class="form-field">
                <th scope="row" valign="top"> <label for="term_long_description">Long description</label></th>
                <td>         <?php wp_editor($term_long_description, 'term_long_description'); ?>      
                    <p class="description">The longer descriotion of Destionation (2. text on Destination page)</p> </td>
            </tr>
        </tbody>
    </table>
    <?php
}

add_action('destinations_edit_form_fields', 'destinations_taxonomy_edit_meta_fields', 10, 2);

function save_destinations_taxonomy_custom_meta($term_id) {
    /* update_term_meta(
      $term_id,
      'term_sailing_yacht_number',
      sanitize_text_field($_POST['term_sailing_yacht_number'])
      );
      update_term_meta(
      $term_id,
      'term_catamaran_number',
      sanitize_text_field($_POST['term_catamaran_number'])
      ); */
    update_term_meta(
            $term_id,
            'term_long_description',
            sanitize_textarea_field($_POST['term_long_description'])
    );
}

add_action('edited_destinations', 'save_destinations_taxonomy_custom_meta');
add_action('created_destinations', 'save_destinations_taxonomy_custom_meta');

/* Save Custom Meta for Revisions */

function add_meta_keys_to_revision($keys) {
    global $meta_box_area_information;
    $fields = $meta_box_area_information['fields'];
    foreach ($fields as $field) {
        $keys[] = $field['id'];
    }
    return $keys;
}

add_filter('wp_post_revision_meta_keys', 'add_meta_keys_to_revision');

//Define the field on the revisions screen
function extrev_custom_fields($fields) {
    global $meta_box_area_information;
    $my_fields = $meta_box_area_information['fields'];
    foreach ($my_fields as $field) {
        $fields[$field['id']] = $field['name'];
    }
    return $fields;
}

add_filter('_wp_post_revision_fields', 'extrev_custom_fields');

//For some reason (havent looked into it) the value is in a array, just pick it out
/* function extrev_field($value, $field) {
  print_r($value);
  $test = $value[0];
  return $test;
  }

  add_filter('_wp_post_revision_field_rev_meta', 'extrev_field', 10, 2); */
/*
  function ca_field($value, $field_name, $post) {
  global $meta_box_area_information;
  $my_fields = $meta_box_area_information['fields'];
  print_r($my_fields);
  foreach ($my_fields as $field) {
  print_r($field);
  if ($field_name == $field['id']) {
  $value = get_metadata('post', $post->ID, $field['id'], true);
  }
  }
  return $value;
  }

  function ca_custom_admin_head() {
  global $meta_box_area_information;
  $my_fields = $meta_box_area_information['fields'];
  foreach ($my_fields as $field) {
  // echo "add_filter('_wp_post_revision_field_" . $field['id'] . "' , 'ca_field', 10, 4);" . " \n";
  add_filter('_wp_post_revision_field_' . $field['id'], 'ca_field', 10, 4);
  }
  }

  add_action('admin_head', 'ca_custom_admin_head'); */

function ca_restore_revision($post_id, $revision_id) {
    $post = get_post($post_id);
    $revision = get_post($revision_id);
    if ($post->post_type = 'areas') {
        global $meta_box_area_information;
        $my_fields = $meta_box_area_information['fields'];
        foreach ($my_fields as $field) {
            $meta = get_metadata('post', $revision->ID, $field['id'], true);
            if (false !== $meta) {
                update_post_meta($post_id, 'ct_' . $field['id'], $meta);
            }
        }
    }
}

add_action('wp_restore_post_revision', 'ca_restore_revision', 10, 2);

$prefix_cabin_charter = '_cabin_charter_adatok_mbox_';
$meta_box_cabin_charter_information = array(
    'id' => 'cabin_charter-post-meta-box',
    'title' => 'Cabin Charter informations',
    'page' => 'cabin-charter',
    'context' => 'normal',
    'priority' => 'high',
    'fields' => array(
        /* array(
          'name' => 'Pre-registration fields',
          'desc' => 'Here are the fields for "Pre Registration" option',
          'type' => 'title',
          'id' => $prefix_cabin_charter . 'pre_registration_title'
          ), */ array(
            'name' => 'Pre-registration',
            'desc' => 'Check in for tours where you can only indicate the will to participate, if such a tour were to start (from date only month, price only from, no ship and cabin choosing)',
            'id' => $prefix_cabin_charter . 'pre_registration',
            'type' => 'checkbox',
        ),
        /* array(
          'name' => 'Normal tour fields',
          'desc' => 'Here are the normal tour registration/inquery fields',
          'type' => 'title',
          'id' => $prefix_cabin_charter . 'normal_registration_title'
          ), */
        array(
            'name' => 'Description:',
            'desc' => '',
            'id' => $prefix_cabin_charter . 'description_text',
            'type' => 'wp_editor',
            'std' => ''
        ),
        array(
            'name' => 'Date from',
            'desc' => 'Date of the charter from (by pergistration only the year and month will be shown) ',
            'id' => $prefix_cabin_charter . 'date_from',
            'type' => 'date',
            'std' => ''
        ), array(
            'name' => 'Date to',
            'desc' => 'Date of the charter to',
            'id' => $prefix_cabin_charter . 'date_to',
            'type' => 'date',
            'std' => ''
        ),
        array(
            'name' => 'Price from',
            'desc' => 'Only label text in &euro; (by pergistration only the from price will be shown)',
            'id' => $prefix_cabin_charter . 'price_from',
            'type' => 'text',
            'std' => ''
        ),
        array(
            'name' => 'Price to',
            'desc' => 'Only label text in &euro;',
            'id' => $prefix_cabin_charter . 'price_to',
            'type' => 'text',
            'std' => ''
        ),
        array(
            'name' => 'Discount',
            'desc' => 'Only % ',
            'id' => $prefix_cabin_charter . 'discount',
            'type' => 'text',
            'std' => ''
        ),
        array(
            'name' => 'General Information image:',
            'desc' => '',
            'id' => $prefix_cabin_charter . 'general_information_image',
            'type' => 'upload',
            'std' => ''
        ),
        array(
            'name' => 'General Information:',
            'desc' => '',
            'id' => $prefix_cabin_charter . 'general_information_text',
            'type' => 'wp_editor',
            'std' => ''
        ),
        array(
            'name' => 'What should I take with me? image:',
            'desc' => '',
            'id' => $prefix_cabin_charter . 'take_with_me_image',
            'type' => 'upload',
            'std' => ''
        ),
        array(
            'name' => 'What should I take with me?:',
            'desc' => '',
            'id' => $prefix_cabin_charter . 'take_with_me_text',
            'type' => 'wp_editor',
            'std' => ''
        ),
        array(
            'name' => 'Whats included image:',
            'desc' => '',
            'id' => $prefix_cabin_charter . 'whats_included_image',
            'type' => 'upload',
            'std' => ''
        ),
        array(
            'name' => 'Whats included:',
            'desc' => '',
            'id' => $prefix_cabin_charter . 'whats_included_text',
            'type' => 'wp_editor',
            'std' => ''
        ), array(
            'name' => 'Whats not included image:',
            'desc' => '',
            'id' => $prefix_cabin_charter . 'whats_not_included_image',
            'type' => 'upload',
            'std' => ''
        ), array(
            'name' => 'Whats not included:',
            'desc' => '',
            'id' => $prefix_cabin_charter . 'whats_not_included_text',
            'type' => 'wp_editor',
            'std' => ''
        ),
        array(
            'name' => 'Gallery from previous tours:',
            'desc' => '',
            'id' => $prefix_cabin_charter . 'previous_tours_gallery',
            'type' => 'wp_editor',
            'std' => ''
        ),
    )
);

add_action('admin_menu', 'cabin_charter_information_add_box');

// Add meta box
function cabin_charter_information_add_box() {
    global $meta_box_cabin_charter_information;

    add_meta_box($meta_box_cabin_charter_information['id'], $meta_box_cabin_charter_information['title'], 'cabin_charter_information_show_box', $meta_box_cabin_charter_information['page'], $meta_box_cabin_charter_information['context'], $meta_box_cabin_charter_information['priority']);
}

// Callback function to show fields in meta box
function cabin_charter_information_show_box() {
    global $meta_box_cabin_charter_information, $post;

    // Use nonce for verification
    echo '<input type="hidden" name="cabin_charter_information_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

    echo '<table class="form-table">';

    meta_box_fields($meta_box_cabin_charter_information['fields'], $post);

    echo '</table>';
}

add_action('save_post', 'cabin_charter_information_save_data');

// Save data from meta box
function cabin_charter_information_save_data($post_id) {
    global $meta_box_cabin_charter_information;

    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }

    //Don't update on Quick Edit
    if (defined('DOING_AJAX')) {
        return $post_id;
    }

    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return $post_id;
        }
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }

    meta_save_fields($meta_box_cabin_charter_information['fields'], $post_id);
}

// Partners
$meta_box_partners_information = array(
    'id' => 'partners-post-meta-box',
    'title' => 'Partners informations',
    'page' => 'partners',
    'context' => 'normal',
    'priority' => 'high',
    'fields' => array(
        array(
            'name' => 'Link:',
            'desc' => '',
            'id' => '_partner_link',
            'type' => 'text',
            'std' => ''
        )
    )
);

add_action('admin_menu', 'partners_information_add_box');

// Add meta box
function partners_information_add_box() {
    global $meta_box_partners_information;

    add_meta_box($meta_box_partners_information['id'], $meta_box_partners_information['title'], 'partners_information_show_box', $meta_box_partners_information['page'], $meta_box_partners_information['context'], $meta_box_partners_information['priority']);
}

// Callback function to show fields in meta box
function partners_information_show_box() {
    global $meta_box_partners_information, $post;

    // Use nonce for verification
    echo '<input type="hidden" name="partners_information_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

    echo '<table class="form-table">';

    meta_box_fields($meta_box_partners_information['fields'], $post);

    echo '</table>';
}

add_action('save_post', 'partners_information_save_data');

// Save data from meta box
function partners_information_save_data($post_id) {
    global $meta_box_partners_information;

    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }

    //Don't update on Quick Edit
    if (defined('DOING_AJAX')) {
        return $post_id;
    }

    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return $post_id;
        }
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }

    meta_save_fields($meta_box_partners_information['fields'], $post_id);
}

// Cabin charters day
$meta_box_cabin_charter_days_information = array(
    'id' => 'cabin_charter_days-post-meta-box',
    'title' => 'Cabin charter',
    'page' => 'cabin-charter_days',
    'context' => 'normal',
    'priority' => 'high',
    'fields' => array(
        array(
            'name' => 'Cabin charter:',
            'desc' => '',
            'id' => '_cabin_charter_id',
            'type' => 'cabincharter',
            'std' => ''
        )
    )
);

add_action('admin_menu', 'cabin_charter_days_information_add_box');

// Add meta box
function cabin_charter_days_information_add_box() {
    global $meta_box_cabin_charter_days_information;

    add_meta_box($meta_box_cabin_charter_days_information['id'], $meta_box_cabin_charter_days_information['title'], 'cabin_charter_days_information_show_box', $meta_box_cabin_charter_days_information['page'], $meta_box_cabin_charter_days_information['context'], $meta_box_cabin_charter_days_information['priority']);
}

// Callback function to show fields in meta box
function cabin_charter_days_information_show_box() {
    global $meta_box_cabin_charter_days_information, $post;

    // Use nonce for verification
    echo '<input type="hidden" name="cabin_charter_days_information_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

    echo '<table class="form-table">';

    meta_box_fields($meta_box_cabin_charter_days_information['fields'], $post);

    echo '</table>';
}

add_action('save_post', 'cabin_charter_days_information_save_data');

// Save data from meta box
function cabin_charter_days_information_save_data($post_id) {
    global $meta_box_cabin_charter_days_information;

    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }

    //Don't update on Quick Edit
    if (defined('DOING_AJAX')) {
        return $post_id;
    }

    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return $post_id;
        }
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }

    meta_save_fields($meta_box_cabin_charter_days_information['fields'], $post_id);
}

// Tour report  dayz
$meta_box_tour_report_days_information = array(
    'id' => 'cabin_charter_days-post-meta-box',
    'title' => 'Cabin charter',
    'page' => 'tour-report-days',
    'context' => 'normal',
    'priority' => 'high',
    'fields' => array(
        array(
            'name' => 'Túrabeszámoló:',
            'desc' => '',
            'id' => '_tour_report_id',
            'type' => 'tourreport',
            'std' => ''
        )
    )
);

add_action('admin_menu', 'tour_report_days_information_add_box');

// Add meta box
function tour_report_days_information_add_box() {
    global $meta_box_tour_report_days_information;

    add_meta_box($meta_box_tour_report_days_information['id'], $meta_box_tour_report_days_information['title'], 'tour_report_days_information_show_box', $meta_box_tour_report_days_information['page'], $meta_box_tour_report_days_information['context'], $meta_box_tour_report_days_information['priority']);
}

// Callback function to show fields in meta box
function tour_report_days_information_show_box() {
    global $meta_box_tour_report_days_information, $post;

    // Use nonce for verification
//    echo '<input type="hidden" name="cabin_charter_days_information_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

    echo '<table class="form-table">';

    meta_box_fields($meta_box_tour_report_days_information['fields'], $post);

    echo '</table>';
}

add_action('save_post', 'tour_report_days_information_save_data');

// Save data from meta box
function tour_report_days_information_save_data($post_id) {
    global $meta_box_tour_report_days_information;

    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }

    //Don't update on Quick Edit
    if (defined('DOING_AJAX')) {
        return $post_id;
    }

    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return $post_id;
        }
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }

    meta_save_fields($meta_box_tour_report_days_information['fields'], $post_id);
}

/*
  // Date discount percentage
  // Add term page
  function date_taxonomy_add_new_meta_field() {
  // this will add the custom meta field to the add new term page
  ?>
  <div class="form-field">
  <label for="discount_percentage">Price on date</label>
  <input type="text" name="date_price" id="date_price"  />
  <p class="description">Price of the cabincharter on this date in &euro;</p>
  </div>
  <div class="form-field">
  <label for="discount_percentage">Discount</label>
  <input type="text" name="discount_percentage" id="discount_percentage"  />
  <p class="description">% string of discount in price</p>
  </div>
  <?php
  }

  add_action('date_add_form_fields', 'date_taxonomy_add_new_meta_field', 10, 2);

  // Edit term page
  function date_taxonomy_edit_meta_fields($term, $taxonomy) {

  // put the term ID into a variable
  //$t_id = $term->term_id;
  // retrieve the existing value(s) for this meta field. This returns an array
  //$term_meta = get_option("taxonomy_$t_id");
  $date_price = get_term_meta($term->term_id, 'date_price', true);
  $discount_percentage = get_term_meta($term->term_id, 'discount_percentage', true);
  ?>
  <table class="form-table">
  <tbody>
  <tr class="form-field">
  <th scope="row" valign="top"><label for="date_price">Price on date</label></th>
  <td>
  <input type="text" value="<?php echo $date_price; ?>" id="date_price" name="date_price"/>
  <p class="description">Price of the cabincharter on this date in &euro;</p>
  </td>
  </tr>
  <tr class="form-field">
  <th scope="row" valign="top"><label for="discount_percentage">Discount</label></th>
  <td>
  <input type="text" value="<?php echo $discount_percentage; ?>" id="discount_percentage" name="discount_percentage"/>
  <p class="description">% string of discount in price</p>
  </td>
  </tr>
  </tbody>
  </table>
  <?php
  }

  add_action('date_edit_form_fields', 'date_taxonomy_edit_meta_fields', 10, 2);

  function save_taxonomy_custom_meta($term_id) {
  update_term_meta(
  $term_id,
  'date_price',
  sanitize_text_field($_POST['date_price'])
  );
  update_term_meta(
  $term_id,
  'discount_percentage',
  sanitize_text_field($_POST['discount_percentage'])
  );
  }

  add_action('edited_date', 'save_taxonomy_custom_meta');
  add_action('created_date', 'save_taxonomy_custom_meta');
 */
// Hajó
$prefix_ships = '_ships_adatok_mbox_';
$meta_box_ships = array(
    'id' => 'ships-post-meta-box',
    'title' => 'Ship datas',
    'page' => 'ships',
    'context' => 'normal',
    'priority' => 'high',
    'fields' => array(
        array(
            'name' => 'Cabin charter:',
            'desc' => 'Select the Cabin Charter to which this ship belongs',
            'id' => $prefix_ships . 'cabin_charter_id',
            'type' => 'cabincharter',
            'std' => ''
        ),
        array(
            'name' => 'Bookable',
            'desc' => 'Check, if this ship is currently bookable for this chrter.',
            'id' => $prefix_ships . 'bookable',
            'type' => 'checkbox',
            'std' => ''
        ),
        array(
            'name' => 'Ship type',
            'desc' => '',
            'id' => $prefix_ships . 'ship_type',
            'type' => 'text',
            'std' => ''
        ),
        array(
            'name' => 'Built Year',
            'desc' => '',
            'id' => $prefix_ships . 'built_year',
            'type' => 'text',
            'std' => ''
        ),
        array(
            'name' => 'Length',
            'desc' => '',
            'id' => $prefix_ships . 'length',
            'type' => 'text',
            'std' => ''
        ),
        array(
            'name' => 'Width',
            'desc' => '',
            'id' => $prefix_ships . 'width',
            'type' => 'text',
            'std' => ''
        ),
        array(
            'name' => 'Cabins',
            'desc' => 'number of cabins',
            'id' => $prefix_ships . 'cabins',
            'type' => 'text',
            'std' => ''
        ),
        array(
            'name' => 'WC',
            'desc' => '',
            'id' => $prefix_ships . 'wc',
            'type' => 'text',
            'std' => ''
        ), array(
            'name' => 'AC',
            'desc' => 'Airconditioner on the ship',
            'id' => $prefix_ships . 'ac',
            'type' => 'checkbox',
            'std' => ''
        ), array(
            'name' => 'Generator',
            'desc' => 'Generator onthe ship',
            'id' => $prefix_ships . 'generator',
            'type' => 'checkbox',
            'std' => ''
        ),
        array(
            'name' => 'Solar panel',
            'desc' => 'Solar panel on the ship',
            'id' => $prefix_ships . 'solar_panel',
            'type' => 'checkbox',
            'std' => ''
        ),
        array(
            'name' => 'Freshwater maker',
            'desc' => 'Freshwater maker on the ship',
            'id' => $prefix_ships . 'freshwater_maker',
            'type' => 'checkbox',
            'std' => ''
        ),
        array(
            'name' => 'Grill',
            'desc' => 'Grill on the ship',
            'id' => $prefix_ships . 'grill',
            'type' => 'checkbox',
            'std' => ''
        ), array(
            'name' => 'Whole ship price',
            'desc' => '',
            'id' => $prefix_ships . 'whole_ship_price',
            'type' => 'text',
            'std' => ''
        ), array(
            'name' => 'A cabin price',
            'desc' => '',
            'id' => $prefix_ships . 'a_cabin',
            'type' => 'text',
            'std' => ''
        ), array(
            'name' => 'B cabin price',
            'desc' => '',
            'id' => $prefix_ships . 'b_cabin',
            'type' => 'text',
            'std' => ''
        ), array(
            'name' => 'C cabin price',
            'desc' => '',
            'id' => $prefix_ships . 'c_cabin',
            'type' => 'text',
            'std' => ''
        ), array(
            'name' => 'D cabin price',
            'desc' => '',
            'id' => $prefix_ships . 'd_cabin',
            'type' => 'text',
            'std' => ''
        ), array(
            'name' => 'E cabin price',
            'desc' => '',
            'id' => $prefix_ships . 'e_cabin',
            'type' => 'text',
            'std' => ''
        ), array(
            'name' => 'F cabin price',
            'desc' => '',
            'id' => $prefix_ships . 'f_cabin',
            'type' => 'text',
            'std' => ''
        ),
    )
);

add_action('admin_menu', 'ships_add_box');

// Add meta box
function ships_add_box() {
    global $meta_box_ships;

    add_meta_box($meta_box_ships['id'], $meta_box_ships['title'], 'ships_show_box', $meta_box_ships['page'], $meta_box_ships['context'], $meta_box_ships['priority']);
}

// Callback function to show fields in meta box
function ships_show_box() {
    global $meta_box_ships, $post;

    // Use nonce for verification
    echo '<input type="hidden" name="ships_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

    echo '<table class="form-table">';

    ships_meta_box_fields($meta_box_ships['fields'], $post);

    echo '</table>';
}

add_action('save_post', 'ships_save_data');

// Save data from meta box
function ships_save_data($post_id) {
    global $meta_box_ships;

    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }
    //Don't update on Quick Edit
    if (defined('DOING_AJAX')) {
        return $post_id;
    }
    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return $post_id;
        }
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }

    ships_meta_save_fields($meta_box_ships['fields'], $post_id);
}

function ships_meta_box_fields($fields, $post) {
    foreach ($fields as $field) {
        // get current post meta data
        $meta = get_post_meta($post->ID, $field['id'], true);

        echo '<tr>';

        if ($field['name']) {
            echo '<th style="width:20%"><label for="',
            $field['id'],
            '">',
            $field['name'],
            '</label></th>',
            '<td>';
        } else {
            echo '<td colspan="2">';
        }
        switch ($field['type']) {
            case 'wp_editor':
                wp_editor($meta ? $meta : $field['std'], $field['id'], $field);
                break;
            case 'email':
            case 'number':
            case 'text':
                echo '<input type="', $field['type'], '"  name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />', '<br />', $field['desc'];
                break;
            case 'textarea':
                echo '<textarea name="', $field['id'], '" id="', $field['id'], '" cols="60" rows="4" style="width:97%">', $meta ? $meta : $field['std'], '</textarea>', '<br />', $field['desc'];
                break;
            case 'select':
                echo '<select name="', $field['id'], '" id="', $field['id'], '">';
                foreach ($field['options'] as $option) {
                    echo '<option', $meta == $option ? ' selected="selected"' : '', '>', $option, '</option>';
                }
                echo '</select>';
                break;
            case 'radio':
                foreach ($field['options'] as $option) {
                    echo '<input type="radio" name="', $field['id'], '" value="', $option['value'], '"', $meta == $option['value'] ? ' checked="checked"' : '', ' />', $option['name'];
                }
                break;
            case 'checkbox':
                echo '<input type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />', $field['desc'];
                break;
            case 'date':
                echo '<input type="date"   name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="28" style="max-width: 180px;"   />', '<br />', $field['desc'];
                break;
            case 'upload':
                add_filter('upload_mimes', 'restict_mime');
                if (true) {
                    ?>
                    <input name="<?php echo $field['id'] ?>" id="<?php echo $field['id']; ?>" value="<?php echo $meta ? $meta : $field['std']; ?>" class="<?php echo (!empty($field['class']) ? $field['class'] : 'large-text'); ?>" />
                    <input id="upload_image_button<?php echo $field['id'] ?>" type="button" class="button add_media" value="Upload" />
                    <?php ?>
                    <script>
                        jQuery(document).ready(function () {

                            var custom_uploader;

                            jQuery('#upload_image_button<?php echo $field['id'] ?>').click(function (e) {

                                e.preventDefault();

                                //If the uploader object has already been created, reopen the dialog
                                if (custom_uploader) {
                                    custom_uploader.open();
                                    return;
                                }

                                //Extend the wp.media object
                                custom_uploader = wp.media.frames.file_frame = wp.media({
                                    title: 'CSV feltöltése',
                                    button: {
                                        text: 'Feltölt'
                                    },
                                    multiple: false
                                });

                                //When a file is selected, grab the URL and set it as the text field's value
                                custom_uploader.on('select', function () {
                                    attachment = custom_uploader.state().get('selection').first().toJSON();

                                    // if (attachment.url.indexOf('.csv') !== -1) {
                                    jQuery('#<?php echo $field['id']; ?>').val(attachment.url);
                                    // } else {
                                    //     jQuery('#<?php echo $field['id']; ?>').val(" ");
                                    //     alert('Nem támogatott formátum!');
                                    //     return false;
                                    // }
                                });

                                //Open the uploader dialog
                                custom_uploader.open();
                            });
                        });
                    </script>
                    <?php
                }
                break;
            case 'cabincharter':
                ?>

                <select style="width:99%;" type="text" name="<?php echo $field['id']; ?>" id="<?php echo $field['id']; ?>" >
                    <option value="">-- Semmi -- </option>
                    <?php
                    $cabincharter_args = array(
                        'post_type' => 'cabin-charter',
                        'posts_per_page' => -1,
                        'post_status' => 'publish'
                    );
                    $cabincharter_query = new WP_Query($cabincharter_args);

                    if ($cabincharter_query->have_posts()) : while ($cabincharter_query->have_posts()) : $cabincharter_query->the_post();
                            ?>
                            <option value="<?php the_ID(); ?>" <?php
                            if ($meta == get_the_ID()) {
                                echo "selected";
                            }
                            ?> ><?php the_title(); ?>(<?php the_time('Y'); ?>)</option>
                                    <?php
                                endwhile;
                            endif;
                            wp_reset_query();
                            ?>
                </select>

                <?php
                echo '<br />', $field['desc'];
                break;
        }
        echo '<td>',
        '</tr>';
    }
}

function ships_meta_save_fields($fields, $post_id) {
    foreach ($fields as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];

        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);

            if (isset($field['callback']) && is_callable($field['callback'])) {
                call_user_func($field['callback'], $new, $post_id, $field['id']);
            }
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    }
}

// CabinCharter Bookings
$prefix_cabin_charter_bookings = '_cabin_charter_bookings_mbox_';
$homeurl = get_bloginfo('wpurl');
$meta_box_cabin_charter_bookings = array(
    'id' => 'cabin_charter_bookings-post-meta-box',
    'title' => __('Datas', 'friotyacht'),
    'page' => 'bookings',
    'context' => 'normal',
    'priority' => 'high',
    'fields' => array(
        array(
            'name' => 'Tour:',
            'desc' => '',
            'id' => $prefix_cabin_charter_bookings . 'tura_id',
            'type' => 'uticel',
            'std' => ''
        ),
        array(
            'name' => 'Phone:',
            'desc' => '',
            'id' => $prefix_cabin_charter_bookings . 'phone',
            'type' => 'text',
            'std' => ''
        ),
        array(
            'name' => 'Email:',
            'desc' => '',
            'id' => $prefix_cabin_charter_bookings . 'email',
            'type' => 'email',
            'std' => ''
        ),
    )
);

add_action('admin_menu', 'cabin_charter_bookings_add_box');

// Add meta box
function cabin_charter_bookings_add_box() {
    global $meta_box_cabin_charter_bookings;

    add_meta_box($meta_box_cabin_charter_bookings['id'], $meta_box_cabin_charter_bookings['title'], 'cabin_charter_bookings_show_box', $meta_box_cabin_charter_bookings['page'], $meta_box_cabin_charter_bookings['context'], $meta_box_cabin_charter_bookings['priority']);
}

// Callback function to show fields in meta box
function cabin_charter_bookings_show_box() {
    global $meta_box_cabin_charter_bookings, $post;

    // Use nonce for verification
    echo '<input type="hidden" name="cabin_charter_bookings_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

    echo '<table class="form-table">';

    foreach ($meta_box_cabin_charter_bookings['fields'] as $field) {
        // get current post meta data
        if (!get_post_meta($post->ID, $field['id'], true)) {
            continue;
        }
        $meta = get_post_meta($post->ID, $field['id'], true);

        if ($field['type'] == "checkbox") {
            echo '<tr><td colspan="2"><hr/></td></tr>';
        }
        echo '<tr>',
        '<th style="width:20%"><label for="', $field['id'], '">', $field['name'], '</label></th>',
        '<td>';
        switch ($field['type']) {
            case 'email':
            case 'number':
            case 'text':
                echo '<input type="', $field['type'], '"  name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />', '<br />', $field['desc'];
                break;
            case 'textarea':
                echo '<textarea name="', $field['id'], '" id="', $field['id'], '" cols="60" rows="4" style="width:97%">', $meta ? $meta : $field['std'], '</textarea>', '<br />', $field['desc'];
                break;
            case 'select':
                echo '<select name="', $field['id'], '" id="', $field['id'], '">';
                foreach ($field['options'] as $option) {
                    echo '<option', $meta == $option ? ' selected="selected"' : '', '>', $option, '</option>';
                }
                echo '</select>';
                break;
            case 'radio':
                foreach ($field['options'] as $option) {
                    echo '<input type="radio" name="', $field['id'], '" value="', $option['value'], '"', $meta == $option['value'] ? ' checked="checked"' : '', ' />', $option['name'];
                }
                break;
            case 'checkbox':
                echo '<input type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />', $field['desc'];
                ;
                break;
            case 'uticel':
                $tura = get_post_meta($post->ID, '_cabin_charter_bookings_mbox_tura_id', true);
                $tura_post_type_slug = get_post_type($tura);
                $tura_post_type = get_post_type_object($tura_post_type_slug);
                echo '<a href="' . get_permalink($meta) . '" target="_blank" ><span class="dashicons dashicons-external"></span> ' . $tura_post_type->labels->singular_name . ': ' . get_the_title($meta) . '</a>';
                echo '<input type="hidden"  name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '"  />';
                break;
            case 'cv':
                echo '<a href="' . wp_get_attachment_url($meta) . '" target="_blank" ><span class="dashicons dashicons-media-document"></span> ' . get_the_title($meta) . '</a>';
                echo '<input type="hidden"  name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '"  />';
                break;
        }
        echo '<td>',
        '</tr>';
    }

    echo '</table>';
}

add_action('save_post', 'cabin_charter_bookings_save_data');

// Save data from meta box
function cabin_charter_bookings_save_data($post_id) {
    global $meta_box_cabin_charter_bookings;

    // verify nonce
    if (!wp_verify_nonce($_POST['cabin_charter_bookings_meta_box_nonce'], basename(__FILE__))) {
        return $post_id;
    }

    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }

    //Don't update on Quick Edit
    if (defined('DOING_AJAX')) {
        return $post_id;
    }
    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return $post_id;
        }
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }

    foreach ($meta_box_cabin_charter_bookings['fields'] as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];

        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    }
}

add_action('cmb2_admin_init', 'friot_tour_report_days_metabox');

/**
 * Hook in and register a metabox for the admin comment edit page.
 */
function friot_tour_report_days_metabox() {

    $cmb_tour_reports_day = new_cmb2_box(array(
        'id' => '_mbox_tour_reports_day_metabox',
        'title' => 'Galéria',
        'object_types' => array('tour-report-days'),
    ));

    $cmb_tour_reports_day->add_field(array(
        'name' => 'Galéria képek',
        'desc' => '',
        'id' => '_mbox_tour_reports_day_gallery',
        'type' => 'file_list',
        'preview_size' => array(100, 100), // Default: array( 50, 50 )
        'query_args' => array('type' => 'image'), // Only images attachment
        // Optional, override default text strings
        'text' => array(
            'add_upload_files_text' => 'Képek kivállasztáa', // default: "Add or Upload Files"
            'remove_image_text' => 'Kép eltávolítása', // default: "Remove Image"
            'file_text' => 'Fájl', // default: "File:"
            'file_download_text' => 'Letöltés', // default: "Download"
            'remove_text' => 'Eltávolítás', // default: "Remove"
        ),
    ));
}

// Testimonial
$meta_box_testimonial_information = array(
    'id' => 'testimonial-post-meta-box',
    'title' => 'Cabin charter',
    'page' => 'testimonial',
    'context' => 'normal',
    'priority' => 'high',
    'fields' => array(
        array(
            'name' => 'Cabin charter:',
            'desc' => '',
            'id' => '_cabin_charter_id',
            'type' => 'cabincharter',
            'std' => ''
        )
    )
);

// add_action('admin_menu', 'testimonial_information_add_box');
// Add meta box
function testimonial_information_add_box() {
    global $meta_box_testimonial_information;

    add_meta_box($meta_box_testimonial_information['id'], $meta_box_testimonial_information['title'], 'testimonial_information_show_box', $meta_box_testimonial_information['page'], $meta_box_testimonial_information['context'], $meta_box_testimonial_information['priority']);
}

// Callback function to show fields in meta box
function testimonial_information_show_box() {
    global $meta_box_testimonial_information, $post;

    // Use nonce for verification
    echo '<input type="hidden" name="testimonial_information_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';

    echo '<table class="form-table">';

    meta_box_fields($meta_box_testimonial_information['fields'], $post);

    echo '</table>';
}

add_action('save_post', 'testimonial_information_save_data');

// Save data from meta box
function testimonial_information_save_data($post_id) {
    global $meta_box_testimonial_information;

    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }

    //Don't update on Quick Edit
    if (defined('DOING_AJAX')) {
        return $post_id;
    }

    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return $post_id;
        }
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }

    meta_save_fields($meta_box_testimonial_information['fields'], $post_id);
}
?>