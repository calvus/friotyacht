<?php

/*
  Plugin Name: Friot Yacht plugin
  Plugin URI: http://
  Description:
  Version: 1.0
  Author: Calv.us
  Author URI: https://calv.us
  Text Domain: friotyacht
 */



require_once plugin_dir_path(__FILE__) . 'custom_post_types.php';
require_once plugin_dir_path(__FILE__) . 'post_metabox.php';
require_once plugin_dir_path(__FILE__) . 'manage_column.php';
require_once plugin_dir_path(__FILE__) . 'iofunctions.php';
require_once plugin_dir_path(__FILE__) . 'friot_settings.php';
require_once plugin_dir_path(__FILE__) . 'method_crm.php';
require_once plugin_dir_path(__FILE__) . '/cmb2-page-select/cmb2_page_select.php';
?>
