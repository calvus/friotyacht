jQuery(document).ready(function () {

    jQuery('.accordion-collapse.collapse').on('hide.bs.collapse', function (e) {
        var $card = jQuery(this).closest('#mainAccordion');
        var $open = jQuery(jQuery(this).data('parent')).find('.collapse.show');

        var additionalOffset = 0;
        if ($card.prevAll().filter($open.closest('.accordion-item')).length !== 0)
        {
            additionalOffset = $open.height();
        }
        jQuery('html,body').animate({
            scrollTop: $card.offset().top - additionalOffset
        }, 0);
    });


    jQuery('.navbar-collapse').on('show.bs.collapse', function () {
        jQuery('body').addClass('onShowHamburgerMenu');
    });

    jQuery('.navbar-collapse').on('hide.bs.collapse', function () {
        jQuery('body').removeClass('onShowHamburgerMenu');
    });

    jQuery(window).resize(function () {
        jQuery('.search-iframe').attr('src', function (i, val) {
            return val;
        });
    });

    // --- Navigation
    jQuery('.nav-icon').click(function () {
        jQuery(this).toggleClass('open');
    });


    jQuery('.card-layout-3').owlCarousel({
        loop: false,
        margin: 20,
        autoplay: false,
        dots: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 2,
            },
            992: {
                items: 3,
                loop: false
            }
        }
    });

    jQuery('.card-layout-4').owlCarousel({
        loop: false,
        margin: 20,
        autoplay: false,
        dots: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 2,
            },
            992: {
                items: 4,
                loop: false
            }
        }
    });

    jQuery('.partners-carosuel').owlCarousel({
        loop: false,
        margin: 20,
        autoplay: false,
        dots: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 2,
            },
            992: {
                items: 4,
                loop: false
            }
        }
    });

    jQuery('.owl-2-col').owlCarousel({
        loop: true,
        margin: 00,
        autoplay: false,
        dots: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            }
        }
    });


    jQuery('.owl-3-col').owlCarousel({
        loop: true,
        margin: 00,
        autoplay: 10000,
        autoplayTimeout: 10000,
        autoplaySpeed: 2000,
        dots: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            }
        }
    });


    jQuery(window).on('hashchange', function () {
        console.log('hash változik');
        console.log('új hash:' + window.location.hash);
        if (window.location.hash === '#corporate_events') {
            jQuery("#owlskipper .owl-dots .owl-dot").last().trigger('click');
        }
        if (window.location.hash === '#skippered_charter') {
            jQuery("#owlskipper .owl-dots .owl-dot").first().trigger('click');
        }
    });



    // --- parallax bg
    // var background_image_parallax = function ($object, multiplier) {
    //   multiplier = typeof multiplier !== 'undefined' ? multiplier : 0.5;
    //   multiplier = 1 - multiplier;
    //   var $doc = jQuery(document);
    //   var $i = 0;

    //   jQuery("section.section").each(function () {
    //     var targeteClasslist = jQuery(this)[0].classList
    //     var objectClasslist = $object[0].classList

    //     if (targeteClasslist == objectClasslist) {
    //       return $i;
    //     }
    //     $i += jQuery(this).innerHeight();
    //   });
    //   $object.css({ "background-attatchment": "fixed" });

    //   jQuery(window).scroll(function () {
    //     var from_top = $doc.scrollTop();
    //     var bg_css = '100% ' + ((from_top - $i) * multiplier) + 'px';
    //     $object.css({ "background-position": bg_css });
    //   });
    // };

    // background_image_parallax(jQuery(".bg-parallax-1"), 0);
    // background_image_parallax(jQuery(".bg-parallax-2"), .45);





    // sticky navigation

    var lastScrollTop;
    var header = jQuery('#fixedHeader');
    var headerHeight = jQuery('#fixedHeader').innerHeight();
    var headerTopHeight = jQuery('.navbar-top').innerHeight();

    if (jQuery(window).width() >= 992) {

        jQuery('.page-content-wrapper').css({'padding-top': '104px'});

        window.addEventListener('scroll', function () {
            var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
            if (scrollTop > lastScrollTop) {
                if (scrollTop >= headerHeight / 2) {
                    jQuery('.navbar-top').hide();
                    header.css({'top': (-headerHeight - headerTopHeight) + 'px'});
                }
            } else {
                header.css({'top': '0'});
                if (scrollTop == 0) {
                    jQuery('.navbar-top').show();
                }
            }
            lastScrollTop = scrollTop;
        });
    }


    jQuery('.swiper').each(function (index) {
        if (jQuery('.swiper').length > 0) {

            console.dir(jQuery(this).attr('id'));
            if ((jQuery(this).attr('id') == 'swiper-top') || (jQuery(this).attr('id') == 'swiper-bottom')) {

                var swiper = new Swiper("#swiper-top", {
                    loop: true,
                    spaceBetween: 0,
                    slidesPerView: 4,
                    freeMode: true,
                    watchSlidesProgress: true,
                });
                var swiper2 = new Swiper("#swiper-bottom", {
                    loop: true,
                    spaceBetween: 0,
                    navigation: {
                        nextEl: ".swiper-button-next",
                        prevEl: ".swiper-button-prev",
                    },
                    thumbs: {
                        swiper: swiper,
                    },
                });
            } else if (jQuery(this).attr('id') == 'swiper-ship') {


                var swiper3 = new Swiper(jQuery(this)[0], {
                    loop: false,
                    loopAdditionalSlides: 0,
                    rewind: true,
                    spaceBetween: 0,
                    navigation: {
                        nextEl: ".swiper-ship-button-next",
                        prevEl: ".swiper-ship-button-prev",
                    },
                });
            }
        }
    })

    /*  if (jQuery('.swiper-ship').length > 0) {
     var swiper_ship_top = new Swiper("#swiper-ship-top", {
     loop: true,
     spaceBetween: 0,
     slidesPerView: 4,
     freeMode: true,
     watchSlidesProgress: true,
     });
     var swiper_ship_bottom = new Swiper("#swiper-ship-bottom", {
     loop: true,
     spaceBetween: 0,
     navigation: {
     nextEl: ".swiper-ship-button-next",
     prevEl: ".swiper-ship-button-prev",
     },
     thumbs: {
     swiper: swiper_ship_top,
     },
     });
     }*/









    jQuery('.gallery-item a').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
    });
    // jQuery('.swiper').each(function () { // the containers for all your galleries
    //     jQuery(this).magnificPopup({
    //         delegate: 'a.gallery-item', // the selector for gallery item
    //         type: 'image',
    //         gallery: {
    //             enabled: true
    //         }
    //     });
    // });
    // jQuery('.gallery').each(function () { // the containers for all your galleries
    //     jQuery(this).magnificPopup({
    //         delegate: '.gallery-item .gallery-icon a', // the selector for gallery item
    //         type: 'image',
    //         gallery: {
    //             enabled: true
    //         }
    //     });
    // });

    var cabinCharterInfoBlock = jQuery('.cbc-info');

    cabinCharterInfoBlock.each(function () {
        var textHeight = jQuery(this).innerHeight();
        var ssumHeight = textHeight - 16;
        var imgNextToText = jQuery(this).parents('div.cbc-content').find('img.cbc-arrow-img2');
        imgNextToText.css('height', ssumHeight + 'px')
    });
});


// --- Navigation
jQuery(window).resize(function () {
    toggleCollapse();

    var cabinCharterInfoBlock = jQuery('.cbc-info');

    cabinCharterInfoBlock.each(function () {
        var textHeight = jQuery(this).innerHeight();
        var ssumHeight = textHeight - 16;
        var imgNextToText = jQuery(this).parents('div.cbc-content').find('img.cbc-arrow-img2');
        imgNextToText.css('height', ssumHeight + 'px')
    });
});

// --- Screening collapsible toggle function
function toggleCollapse() {
    if (jQuery(window).width() > 767) {
        jQuery('.btn--collapse').removeClass('collapsed');
        jQuery('.btn--collapse').attr("data-bs-toggle", '');
        jQuery('.btn--collapse span').hide();
        jQuery('.item-list').removeClass('collapse');
    } else {
        jQuery('.btn--collapse').addClass('collapsed');
        jQuery('.btn--collapse').attr("data-bs-toggle", 'collapse');
        jQuery('.btn--collapse span').show();
        jQuery('.item-list').addClass('collapse');
    }
}


var errortext = '';
var noerror = true;
function adderror3(id, text) {
    jQuery("#" + id).addClass("alert");
    jQuery("#" + id)
            .next("label")
            .remove();
    jQuery("#" + id).after('<label class="alertbox alert">' + text + " </label>");
    //jQuery("#"+id).val(text);
    //errortext+="<p>"+text+"</p>";
    noerror = false;
}


function adderror4(id, text) {
    jQuery("#" + id).addClass("alert");
    jQuery("label[for='" + id + "']").addClass("alert");
    jQuery("#" + id)
            .parent()
            .append('<div class="validation-advice">' + text + " </div>");
    //jQuery("#"+id).val(text);
    //errortext+="<p>"+text+"</p>";
    noerror = false;
}
function adderrorshipselect(errorclass, text) {
    jQuery("." + errorclass).addClass("alert");
    //jQuery("#" + id).parent.next("label").remove();
    jQuery("." + errorclass).after('<label class="alertbox alert">' + text + " </label>");
    //jQuery("#"+id).val(text);
    //errortext+="<p>"+text+"</p>";
    noerror = false;
}
function hideerrors() {
    errortext = "";
    noerror = true;
    jQuery(".error").remove();
    jQuery(".validation-advice").remove();
    jQuery(".alert").removeClass("alert");
    jQuery("#alertext").html("");
    jQuery(".alertbox").html("");
    jQuery(".alertbox").hide();
}
function validateemail(email) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(email) == false) {
        return false;
    } else {
        return true;
    }
}

jQuery(document).ready(function () {

    // get selected date on cabin charter page
    var dropdownItem = jQuery('#available-ships-dropdown > .item');
    dropdownItem.on('click', function () {
        jQuery(this).addClass('selected');
        dropdownItem.not(jQuery(this)).removeClass('selected');
        jQuery("#available-cabins-display").click(function () {
            return false;
        });
        hideerrors();
        jQuery(".preloadaer.p-2.text-center").css('opacity', '1');
        jQuery(".preloadaer.p-2.text-center").css('visibility', 'visible');
        jQuery(".available-cabins").addClass("disabled");
        // jQuery(".available-cabins").prop("disabled", true);
        jQuery('#available-ships-display > .item').html(jQuery(this).html());
        jQuery('#available-ships-input').val(jQuery(this).data('value'));
        jQuery('#available-ships-input').data('ship_id', jQuery(this).data('ship_id'));
        jQuery("#available-cabins-dropdown").html('');
        jQuery("#available-cabins-display").html('<div class="item" data-value=""><p class="cabin">' + translations_request_forms['select_cabin'] + '</p></div>');
        get_cabins_avaible_on_ship(jQuery('#available-ships-input').val());
    });
    function get_cabins_avaible_on_ship(ship_id) {
        //  console.log(ship_id);
        jQuery.ajax({
            url: ajaxurl,
            data: {
                action: 'get_cabins_avaible_on_ship',
                ship_id: ship_id,
                currentLangCode: currentLangCode
            },
            success: function (msg) {
                //console.log(msg);
                // console.log(JSON.parse(msg));
                jQuery("#available-cabins-dropdown").html(JSON.parse(msg));
                var dropdowncabinsItem = jQuery('#available-cabins-dropdown > .item');
                jQuery("#available-cabins-display").click(function () {
                    return true;
                });
                jQuery(".available-cabins").css('cursor', 'auto');
                jQuery("#available-cabins-display").css('cursor', 'auto');
                jQuery("#available-cabins-display .dropdown-toggle").css('cursor', 'auto');
                jQuery("#available-cabins-display .dropdown-toggle .item").css('cursor', 'auto');
                //  jQuery(".available-cabins").prop("disabled", false);
                jQuery(".preloadaer.p-2.text-center").css('opacity', '0');
                jQuery(".preloadaer.p-2.text-center").css('visibility', 'hidden');
                jQuery(".available-cabins").removeClass("disabled");
                //  console.log(dropdowncabinsItem);
                dropdowncabinsItem.on('click', function () {
                    hideerrors();
                    //   console.log(jQuery(this).attr('data-value'));
                    jQuery(this).addClass('selected');
                    dropdowncabinsItem.not(jQuery(this)).removeClass('selected');
                    jQuery('#available-cabins-display > .item').html(jQuery(this).html());
                    jQuery('#available-cabins-input').val(jQuery(this).attr('data-value'));
                });
            }
        });
    }

    jQuery("a#turahajo_lefoglalas").on("click", function (e) {
        e.preventDefault();
        hideerrors();
        var available_ships_input = jQuery("#available-ships-input").val();
        var available_cabins_input = jQuery("#available-cabins-input").val();
        if (available_ships_input == "") {
            adderrorshipselect("available-ships", translations_request_forms['please_select_a_boat']);
        }

        if (available_cabins_input == "") {
            adderrorshipselect("available-cabins", translations_request_forms['please_select_a_cabin']);
        }


        if (noerror) {
            document.turahajo_foglalas_turavalasztas_form.submit();
        }

    });
    jQuery("a#turahajo_lefoglalas_veglegesites").on("click", function (e) {
        e.preventDefault();
        hideerrors();
        var checked = jQuery("#turahajo_aszf").prop("checked");
        var privacy_chk = jQuery("#privacy_chk").prop("checked");
        var booking_terms = jQuery("#booking_terms").prop("checked");
        var firstname = jQuery("#firstname").val();
        var lastname = jQuery("#lastname").val();
        var email_address = jQuery("#email_address").val();
        var phone = jQuery("#phone").val();
        var validemail = validateemail(email_address);
        if (!checked) {
            adderror4("turahajo_aszf", translations_request_forms['accept_terms']);
        }
        if (!privacy_chk) {
            adderror4("privacy_chk", translations_request_forms['accept_policy']);
        }
        if (!booking_terms) {
            adderror4("booking_terms", translations_request_forms['accept_conditions']);
        }

        if (firstname == "") {
            adderror3("firstname", translations_request_forms['enter_first_name']);
        }

        if (lastname == "") {
            adderror3("lastname", translations_request_forms['enter_last_name']);
        }

        if (email_address == "") {
            adderror3("email_address", translations_request_forms['enter_email']);
        } else {
            if (!validemail) {
                adderror3("email_address", translations_request_forms['error_email']);
            }
        }

        if (phone == "") {
            adderror3("phone", translations_request_forms['enter_phone']);
        }



        if (noerror) {
            document.turahajo_foglalas_form.submit();
        }

    });
    jQuery("a#turahajo_ajanlatkeres").on("click", function (e) {
        e.preventDefault();
        hideerrors();
        jQuery("#turahajo_foglalas_turavalasztas_form").attr("action", "" + translations_request_forms['request_form_id'] + "");
        var available_ships_input = jQuery("#available-ships-input").val();
        var available_cabins_input = jQuery("#available-cabins-input").val();
        var request_type = jQuery("#request_type").val('ajanlatkeres');
        if (available_ships_input == "") {
            adderrorshipselect("available-ships", translations_request_forms['please_select_a_boat']);
        }

        if (available_cabins_input == "") {
            adderrorshipselect("available-cabins", translations_request_forms['please_select_a_cabin']);
        }


        if (noerror) {
            document.turahajo_foglalas_turavalasztas_form.submit();
        }

    });
    jQuery("a#turahajo_ajanlatkeres_veglegesites").on("click", function (e) {
        e.preventDefault();
        hideerrors();
        var checked = jQuery("#turahajo_aszf").prop("checked");
        var privacy_chk = jQuery("#privacy_chk").prop("checked");
        var booking_terms = jQuery("#booking_terms").prop("checked");
        var firstname = jQuery("#firstname").val();
        var lastname = jQuery("#lastname").val();
        var email_address = jQuery("#email_address").val();
        var phone = jQuery("#phone").val();
        var validemail = validateemail(email_address);
        if (!checked) {
            adderror4("turahajo_aszf", translations_request_forms['accept_terms']);
        }
        if (!privacy_chk) {
            adderror4("privacy_chk", translations_request_forms['accept_policy']);
        }

        if (firstname == "") {
            adderror3("firstname", translations_request_forms['enter_first_name']);
        }

        if (lastname == "") {
            adderror3("lastname", translations_request_forms['enter_last_name']);
        }

        if (email_address == "") {
            adderror3("email_address", translations_request_forms['enter_email']);
        } else {
            if (!validemail) {
                adderror3("email_address", translations_request_forms['error_email']);
            }
        }

        if (phone == "") {
            adderror3("phone", translations_request_forms['enter_phone']);
        }



        if (noerror) {
            document.turahajo_ajanlatkeres_form.submit();
        }

    });
    jQuery("a#turahajo_eloregisztracio").on("click", function (e) {
        e.preventDefault();
        hideerrors();
        document.turahajo_eloregisztracio_turavalasztas_form.submit();
    });
    jQuery("a#turahajo_eloregisztracio_veglegesites").on("click", function (e) {
        e.preventDefault();
        hideerrors();
        var checked = jQuery("#turahajo_aszf").prop("checked");
        var privacy_chk = jQuery("#privacy_chk").prop("checked");
        var firstname = jQuery("#firstname").val();
        var lastname = jQuery("#lastname").val();
        var email_address = jQuery("#email_address").val();
        var phone = jQuery("#phone").val();
        var validemail = validateemail(email_address);
        if (!checked) {
            adderror4("turahajo_aszf", translations_request_forms['accept_terms']);
        }
        if (!privacy_chk) {
            adderror4("privacy_chk", translations_request_forms['accept_policy']);
        }

        if (firstname == "") {
            adderror3("firstname", translations_request_forms['enter_first_name']);
        }

        if (lastname == "") {
            adderror3("lastname", translations_request_forms['enter_last_name']);
        }

        if (email_address == "") {
            adderror3("email_address", translations_request_forms['enter_email']);
        } else {
            if (!validemail) {
                adderror3("email_address", translations_request_forms['error_email']);
            }
        }

        if (phone == "") {
            adderror3("phone", translations_request_forms['enter_phone']);
        }



        if (noerror) {
            document.turahajo_eloregisztracio_form.submit();
        }

    });
    jQuery("a#crewedcharterrequest_send").on("click", function (e) {
        e.preventDefault();
        hideerrors();
        var checked = jQuery("#turahajo_aszf").prop("checked");
        var privacy_chk = jQuery("#privacy_chk").prop("checked");
        var firstname = jQuery("#firstname").val();
        var lastname = jQuery("#lastname").val();
        var email_address = jQuery("#email_address").val();
        var phone = jQuery("#phone").val();
        var validemail = validateemail(email_address);
        var destination = jQuery("#destination").val();
        var ship_type = jQuery("#ship_type").val();
        var ship_age = jQuery("#ship_age").val();
        var number_of_persons = jQuery("#number_of_persons").val();
        var date_from = jQuery("#date_from").val();
        var date_to = jQuery("#date_to").val();
        if (!checked) {
            adderror4("turahajo_aszf", translations_request_forms['accept_terms']);
        }
        if (!privacy_chk) {
            adderror4("privacy_chk", translations_request_forms['accept_policy']);
        }

        if (firstname == "") {
            adderror3("firstname", translations_request_forms['enter_first_name']);
        }

        if (lastname == "") {
            adderror3("lastname", translations_request_forms['enter_last_name']);
        }

        if (email_address == "") {
            adderror3("email_address", translations_request_forms['enter_email']);
        } else {
            if (!validemail) {
                adderror3("email_address", translations_request_forms['error_email']);
            }
        }

        if (phone == "") {
            adderror3("phone", translations_request_forms['enter_phone']);
        }
        if (destination == "") {
            adderror3("destination", translations_request_forms['enter_destination']);
        }
        if (ship_type == "") {
            adderror3("ship_type", translations_request_forms['please_select_a_boat_type']);
        }
        if (ship_age == "") {
            adderror3("ship_age", translations_request_forms['please_select_a_boat_age']);
        }
        if (number_of_persons == "") {
            adderror3("number_of_persons", translations_request_forms['please_select_number_of_persons']);
        }
        if (date_from == "") {
            adderror3("date_from", translations_request_forms['please_select_date_from']);
        }
        if (date_to == "") {
            adderror3("date_to", translations_request_forms['please_select_date_to']);
        }




        if (noerror) {
            document.crewedcharterrequest_form.submit();
        }

    });
    jQuery("a#arearequest_send").on("click", function (e) {
        e.preventDefault();
        hideerrors();
        var checked = jQuery("#turahajo_aszf").prop("checked");
        var privacy_chk = jQuery("#privacy_chk").prop("checked");
        var firstname = jQuery("#firstname").val();
        var lastname = jQuery("#lastname").val();
        var email_address = jQuery("#email_address").val();
        var phone = jQuery("#phone").val();
        var validemail = validateemail(email_address);
        var ship_type = jQuery("#ship_type").val();
        var ship_age = jQuery("#ship_age").val();
        var number_of_persons = jQuery("#number_of_persons").val();
        var date_from = jQuery("#date_from").val();
        var date_to = jQuery("#date_to").val();
        if (!checked) {
            adderror4("turahajo_aszf", translations_request_forms['accept_terms']);
        }
        if (!privacy_chk) {
            adderror4("privacy_chk", translations_request_forms['accept_policy']);
        }

        if (firstname == "") {
            adderror3("firstname", translations_request_forms['enter_first_name']);
        }

        if (lastname == "") {
            adderror3("lastname", translations_request_forms['enter_last_name']);
        }

        if (email_address == "") {
            adderror3("email_address", translations_request_forms['enter_email']);
        } else {
            if (!validemail) {
                adderror3("email_address", translations_request_forms['error_email']);
            }
        }

        if (phone == "") {
            adderror3("phone", translations_request_forms['enter_phone']);
        }

        if (ship_type == "") {
            adderror3("ship_type", translations_request_forms['please_select_a_boat_type']);
        }
        if (ship_age == "") {
            adderror3("ship_age", translations_request_forms['please_select_a_boat_age']);
        }
        if (number_of_persons == "") {
            adderror3("number_of_persons", translations_request_forms['please_select_number_of_persons']);
        }
        if (date_from == "") {
            adderror3("date_from", translations_request_forms['please_select_date_from']);
        }
        if (date_to == "") {
            adderror3("date_to", translations_request_forms['please_select_date_to']);
        }




        if (noerror) {
            document.arearequest_form.submit();
        }

    });
    jQuery("a#generalrequest_send").on("click", function (e) {
        e.preventDefault();
        hideerrors();
        var checked = jQuery("#turahajo_aszf").prop("checked");
        var privacy_chk = jQuery("#privacy_chk").prop("checked");
        var firstname = jQuery("#firstname").val();
        var lastname = jQuery("#lastname").val();
        var email_address = jQuery("#email_address").val();
        var phone = jQuery("#phone").val();
        var validemail = validateemail(email_address);
        var destination = jQuery("#destination").val();
        var ship_type = jQuery("#ship_type").val();
        var budget = jQuery("#budget").val();
        var include_skipper = jQuery("#include_skipper").val();
        var date_from = jQuery("#date_from").val();
        var date_to = jQuery("#date_to").val();
        if (!checked) {
            adderror4("turahajo_aszf", translations_request_forms['accept_terms']);
        }
        if (!privacy_chk) {
            adderror4("privacy_chk", translations_request_forms['accept_policy']);
        }

        if (firstname == "") {
            adderror3("firstname", translations_request_forms['enter_first_name']);
        }

        if (lastname == "") {
            adderror3("lastname", translations_request_forms['enter_last_name']);
        }

        if (email_address == "") {
            adderror3("email_address", translations_request_forms['enter_email']);
        } else {
            if (!validemail) {
                adderror3("email_address", translations_request_forms['error_email']);
            }
        }

        if (phone == "") {
            adderror3("phone", translations_request_forms['enter_phone']);
        }
        if (destination == "") {
            adderror3("destination", translations_request_forms['enter_destination']);
        }
        if (ship_type == "") {
            adderror3("ship_type", translations_request_forms['please_select_a_boat_type']);
        }
        if (budget == "") {
            adderror3("budget", translations_request_forms['please_select_budget']);
        }

        if (include_skipper == "") {
            adderror3("include_skipper", translations_request_forms['please_select_skipper']);
        }
        if (date_from == "") {
            adderror3("date_from", translations_request_forms['please_select_date_from']);
        }
        if (date_to == "") {
            adderror3("date_to", translations_request_forms['please_select_date_to']);
        }




        if (noerror) {
            document.generalrequest_form.submit();
        }

    });
});


window.cookieconsent.initialise({
    "palette": {
        "popup": {
            "background": "#082b3f"
        },
        "button": {
            "background": "#e84250",
            "text": translations_cookieconsent['got_it']
        }
    },
    "content": {
        "message": translations_cookieconsent['cookie_text'],
        "link": translations_cookieconsent['learn_more'],
        "href": translations_cookieconsent['privacy_policy_link'],
        "dismiss": translations_cookieconsent['got_it']
    }
});

jQuery(document).ready(function () {
    grecaptcha.ready(function () {
        grecaptcha.execute('6Lda_DIjAAAAAFP5mmU5KNgViVl5Ch9pex0z-x_U', {action: 'request_send_form'}).then(function (token) {
            jQuery('#googletoken').val(token);
        });
    });
});


jQuery.event.special.touchstart = {
    setup: function (_, ns, handle) {
        this.addEventListener("touchstart", handle, {passive: !ns.includes("noPreventDefault")});
    }
};
jQuery.event.special.touchmove = {
    setup: function (_, ns, handle) {
        this.addEventListener("touchmove", handle, {passive: !ns.includes("noPreventDefault")});
    }
};
jQuery.event.special.wheel = {
    setup: function (_, ns, handle) {
        this.addEventListener("wheel", handle, {passive: true});
    }
};
jQuery.event.special.mousewheel = {
    setup: function (_, ns, handle) {
        this.addEventListener("mousewheel", handle, {passive: true});
    }
};

