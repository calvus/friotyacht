/**
 * A simple Gulp 4 Starter Kit for modern web development.
 *
 * @package @jr-cologne/create-gulp-starter-kit
 * @author JR Cologne <kontakt@jr-cologne.de>
 * @copyright 2019 JR Cologne
 * @license https://github.com/jr-cologne/gulp-starter-kit/blob/master/LICENSE MIT
 * @version v0.10.10-beta
 * @link https://github.com/jr-cologne/gulp-starter-kit GitHub Repository
 * @link https://www.npmjs.com/package/@jr-cologne/create-gulp-starter-kit npm package site
 *
 * ________________________________________________________________________________
 *
 * gulpfile.js
 *
 * The gulp configuration file.
 *
 */

const gulp                      = require('gulp'),
      del                       = require('del'),
      sourcemaps                = require('gulp-sourcemaps'),
      plumber                   = require('gulp-plumber'),
      sass                      = require('gulp-sass'),
      autoprefixer              = require('gulp-autoprefixer'),
      minifyCss                 = require('gulp-clean-css'),
      babel                     = require('gulp-babel'),
      webpack                   = require('webpack-stream'),
      uglify                    = require('gulp-uglify'),
      concat                    = require('gulp-concat'),
      browserSync               = require('browser-sync').create(),

      src_folder                = './src/',
      src_assets_folder         = src_folder + 'assets/',
      dist_folder               = '../includes/',
      dist_dev_folder           = './dist/';

gulp.task('clear', () =>
    del([dist_dev_folder + '**', '!.gitignore'], {force: true}),
    del([dist_folder + '**', '!.gitignore'], {force: true})
);

gulp.task('html', () => {
return gulp.src([ src_folder + '**/*.html' ], {
    base: src_folder,
    since: gulp.lastRun('html')
})
    .pipe(gulp.dest(dist_dev_folder))
    .pipe(browserSync.stream());
});

gulp.task('sass', () => {
  return gulp.src([
    src_assets_folder + 'scss/*.scss'
  ])
    .pipe(plumber())
    .pipe(sass())
    .pipe(gulp.dest(dist_folder + 'css'))
    .pipe(gulp.dest(dist_dev_folder + 'css'))
    .pipe(browserSync.stream());
});

gulp.task('sass-build', () => {
  return gulp.src([
    src_assets_folder + 'scss/*.scss'
  ])
    .pipe(sourcemaps.init())
      .pipe(plumber())
      .pipe(sass())
      .pipe(autoprefixer())
      .pipe(minifyCss())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(dist_folder + 'css'))
    .pipe(gulp.dest(dist_dev_folder + 'css'))
    .pipe(browserSync.stream());
});

gulp.task('css-build', () => {
  return gulp.src([
    src_assets_folder + 'css/*.css'
  ])
    .pipe(sourcemaps.init())
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest(dist_folder + 'css'))
      .pipe(gulp.dest(dist_dev_folder + 'css'))
      .pipe(browserSync.stream());
});

gulp.task('js', () => {
  return gulp.src([ src_assets_folder + 'js/scripts/*.js' ], { since: gulp.lastRun('js') })
    .pipe(plumber())
    .pipe(webpack({
      mode: 'production'
    }))
    .pipe(sourcemaps.init())
      .pipe(babel({
        presets: [ '@babel/env' ]
      }))
      .pipe(concat('scripts.min.js'))
      .pipe(uglify())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(dist_folder + 'js'))
    .pipe(gulp.dest(dist_dev_folder + 'js'))
    .pipe(browserSync.stream());
});

gulp.task('fonts', function() {
  return gulp.src([ src_assets_folder + 'fonts/*' ], { since: gulp.lastRun('fonts') })
    .pipe(gulp.dest(dist_folder + 'fonts'))
    .pipe(gulp.dest(dist_dev_folder + 'fonts'))
});

gulp.task('fix-js', () => {
  return gulp.src([ src_assets_folder + 'js/*.+(js|map)' ], { since: gulp.lastRun('fix-js') })
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(dist_folder + 'js'))
    .pipe(gulp.dest(dist_dev_folder + 'js'))
    .pipe(browserSync.stream());
});

gulp.task('images', () => {
  return gulp.src([ src_assets_folder + 'images/**/*.+(png|jpg|jpeg|gif|svg|ico|pdf)' ], { since: gulp.lastRun('images') })
    .pipe(plumber())
    .pipe(gulp.dest(dist_folder + 'images'))
    .pipe(gulp.dest(dist_dev_folder + 'images'))
    .pipe(browserSync.stream());
});

gulp.task('serve', () => {
  return browserSync.init({
    server: {
      baseDir: [ 'dist' ]
    },
    port: 3000,
    open: false
  });
});

gulp.task('watch', () => {
  const watchImages = [
    src_assets_folder + 'images/**/*.+(png|jpg|jpeg|gif|svg|ico)'
  ];

  const watch = [
    src_folder + '*.html',
    src_assets_folder + 'scss/**/*.scss',
    src_assets_folder + 'fonts/**/*.{eot|ttf|woff|woff2}',
    src_assets_folder + 'js/**/*.js'
  ];

  gulp.watch(watch, gulp.series('dev')).on('change', browserSync.reload);
  gulp.watch(watchImages, gulp.series('images')).on('change', browserSync.reload);
});

gulp.task('build', gulp.series('clear', 'sass-build', 'css-build', 'html', 'js', 'fix-js', 'images', 'fonts'));
gulp.task('dev', gulp.series('sass-build', 'css-build', 'html', 'js', 'fix-js', 'images', 'fonts'));
gulp.task('default', gulp.series('dev', gulp.parallel('serve', 'watch')));
