include .env
export

compose=docker-compose -f $(DOCKER_COMPOSE_FILE)
export compose

.PHONY: start
start: stop build up  ## recreate dependencies and spin up again

.PHONY: restart
restart: down pull start ## clean current environment and start

## Docker compose commands
.PHONY: erase
erase: ## stop and delete containers, clean volumes.
	$(compose) stop
	$(compose) rm -v -f

.PHONY: eraseall
eraseall: erase erasedb erasesitebuild## erase containers, database, sitebuild dists

.PHONY: build
build: ## build environment
	$(compose) build

.PHONY: pull
pull: ## pull fresh docker images
	$(compose) pull

.PHONY: up
up: ## spin up environment
	$(compose) up -d

.PHONY: down
down: ## stop and delete containers, clean volumes.
	$(compose) down -v --rmi local --remove-orphans

.PHONY: stop
stop: ## stop environment
	$(compose) stop


## Containers
.PHONY: sh
sh: ## gets inside a container, use 's' variable to select a service. make s=php sh
	$(compose) exec $(s) sh -l

.PHONY: psh
psh: ## gets inside php container
	$(compose) exec php sh -l

.PHONY: sbsh
sbsh: ## gets inside sitebuild container, where way we can access npm and gulp
	$(compose) exec sitebuild sh -l

.PHONY: erasesitebuild
erasesitebuild: ## delete the sitebuild prod built files
	rm -R includes
	rm -R sitebuild/dist

## Wordpress commands
.PHONY: sr
sr: ## Execute wp-cli search and replace
	$(compose) run php sh -lc 'wp search-replace "${VIRTUAL_HOST}" "http://localhost:${HTTP_PORT}" --allow-root'

## Utility commands
.PHONY: logs
logs: ## look for 's' service logs, make s=php logs
	$(compose) logs -f $(s)

.PHONY: help
help: ## Display this help message
	@cat $(MAKEFILE_LIST) | grep -e "^[a-zA-Z_\-]*: *.*## *" | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
